package com.novinteh.mdl

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule


open class ScopeViewModelTest {


    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    private val scope = CoroutineScope(Dispatchers.IO)
    private val firstList = MutableLiveData<List<Int>>()
    private val secondList = MutableLiveData<List<Int>>()


    @Test
    fun combineWithAsyncText() {
        val result = firstList.combineWithAsync(secondList) { first, second ->
            first.associateWith { second }
        }

        result.observeForever {
            println("Result submitted: $it")
        }

        val firstJob = scope.launch(start = CoroutineStart.LAZY) {
            firstList.postValue(List(100) { it })
        }

        val secondJob = scope.launch(start = CoroutineStart.LAZY) {
            secondList.postValue(List(100) { it - 10 })
        }

        firstList.observeForever {
            println("First list submitted")
        }

        secondList.observeForever {
            println("Second list submitted")
        }

        firstJob.start()
        secondJob.start()

        runBlocking {
            firstJob.join()
            secondJob.join()
            delay(5000)
        }
    }

    private inline fun <T, K, R> LiveData<T>.combineWithAsync(
        other: LiveData<K>,
        crossinline map: (T, K) -> R?
    ): LiveData<R> {
        val mediator = MediatorLiveData<R>()

        mediator.addSource(this) { value ->
            //println("Mediator this source changed")
            other.value?.also { otherValue ->
                scope.launch {
                    val result = map(value, otherValue)
                    mediator.postValue(result)
                }
            }
        }

        mediator.addSource(other) { otherValue ->
            //println("Mediator other source changed")
            value?.also { value ->
                scope.launch {
                    val result = map(value, otherValue)
                    mediator.postValue(result)
                }
            }
        }

        return mediator
    }

}