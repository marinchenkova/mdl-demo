package com.novinteh.mdl

import com.novinteh.mdl.data.repo.AsyncRepository
import com.novinteh.mdl.data.Result
import kotlinx.coroutines.*
import org.junit.Test
import kotlin.random.Random
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import junit.framework.Assert.assertEquals
import org.junit.Rule
import org.junit.rules.TestRule



/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class AsyncRepositoryTest {
/*
    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    private val scope = CoroutineScope(Dispatchers.IO)
    private val repo = object : AsyncRepository() {

        override val LOG = true
        override val LOG_TAG = "TestRepo"

        private val dbMock = mutableListOf<Entity>()
        val items: List<Entity> get() = dbMock


        override suspend fun contains(id: String): Boolean {
            val entity = dbMock.find { entity -> entity.id == id }
            randomDelayMs(50, 100)
            return entity != null
        }

        override suspend fun upsert(id: String, data: Result.Success.Data) {
            randomDelayMs(100, 200)
            dbMock.add(toEntity(id, data))
        }

        private fun toEntity(date: String, data: Result.Success.Data): Entity = data.reader.use { reader ->
            Entity(date, reader.readText())
        }

        override suspend fun download(id: String): Result {
            randomDelayMs(500, 1000)
            return Result.Success.Data("ExampleData".reader())
        }

        private suspend fun randomDelayMs(from: Long, to: Long) {
            delay(Random.nextLong(from, to))
        }
    }

    private data class Entity(val id: String, val data: String)

    private fun IntRange.toListOfStrings() = map { "$it" }


    @Test fun `loadIfAbsent loads only absent ids`() {
        val ids: MutableList<String> = (0..5).toListOfStrings().toMutableList()

        val loadJob = scope.launch {
            repo.loadIfAbsent(ids)
        }

        runBlocking {
            loadJob.join()
            assertEquals(ids, repo.items.map { it.id })
        }

        ids += (6..7).toListOfStrings()
        val repeatLoadJob = scope.launch {
            repo.loadIfAbsent(ids)
        }

        runBlocking {
            repeatLoadJob.join()
            assertEquals(ids, repo.items.map { it.id })
        }
    }

    @Test fun `load state test`() {
        //TODO repo load state test
        val ids: MutableList<String> = (0..5).toListOfStrings().toMutableList()
        var lastState: Result = Result.Success.Empty

        repo.state.observeForever { state ->
            println("state changed to $state")
            lastState = state
        }

        val loadJob = scope.launch {
            repo.load(ids)
        }

        println("load was launched, last state is $lastState")

        runBlocking {
            loadJob.join()
            println("load job joined, last state is $lastState")
            delay(5000)
            println("5 s passed, last state is $lastState")
        }

    }
*/

}
