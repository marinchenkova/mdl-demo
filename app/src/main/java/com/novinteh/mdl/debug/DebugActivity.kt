package com.novinteh.mdl.debug

import android.os.Bundle
import com.novinteh.mdl.R
import com.novinteh.mdl.main.WorkActivity


class DebugActivity : WorkActivity() {

    override val LOG = true
    override val LOG_TAG = "DebugActivity"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_debug)
        initToolbar("Debug")
    }

}