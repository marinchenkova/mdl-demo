package com.novinteh.mdl.regimes.dayPlans.details.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.novinteh.mdl.R
import com.novinteh.mdl.entity.data.common.Regime
import com.novinteh.mdl.regimes.dayPlans.model.mapping.TehsectPlanStub
import com.novinteh.mdl.view.appear
import com.novinteh.mdl.view.dpToPixels
import com.novinteh.mdl.view.recycler.adapter.ItemAdapter
import com.novinteh.mdl.view.recycler.viewholder.BindViewHolder
import com.novinteh.mdl.view.span
import kotlinx.android.synthetic.main.item_tehsect_plan_details.view.*
import org.jetbrains.anko.backgroundResource
import org.jetbrains.anko.displayMetrics


private const val TIMEBAR_MARGIN_DISPLAY = 30
private const val TIMEBAR_MARGIN_ROOT = 22


class TehsectPlanAdapter : ItemAdapter<TehsectPlanStub, TehsectPlanAdapter.ViewHolder>(
    TehsectPlanStub.DiffCallback
) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_tehsect_plan_details, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position - 1)?.apply { holder.prevRegime = regime }
        super.onBindViewHolder(holder, position)
    }


    class ViewHolder(view: View) : BindViewHolder<TehsectPlanStub>(view) {

        private val root = itemView.root
        private val timebar = itemView.timebar

        private val time = itemView.time

        private val recyclerNps = itemView.recycler_nps

        private val regime = itemView.regime
        private val q = itemView.q
        private val npsParamsTable = itemView.nps_params_table_layout
        private val note = itemView.note

        private val npsRowAdapter = NpsRowAdapter()
        var prevRegime = Regime("")

        private val timeBarMarginDisplay = itemView.context.dpToPixels(TIMEBAR_MARGIN_DISPLAY).toInt()
        private val timeBarMarginRoot = itemView.context.dpToPixels(TIMEBAR_MARGIN_ROOT).toInt()
        private val timeBarWidth = itemView.context.displayMetrics.widthPixels - timeBarMarginDisplay * 2


        init {
            recyclerNps.layoutManager = LinearLayoutManager(itemView.context, LinearLayoutManager.VERTICAL, false)
            recyclerNps.itemAnimator = null
            recyclerNps.adapter = npsRowAdapter
        }

        override fun content(item: TehsectPlanStub) {
            time.text = itemView.context.getString(R.string.str_dash_str, item.beginHm.formatHm(), item.endHm.formatHm())
            q.text = item.regimeQt.toString()
            regime.text =
                if (adapterPosition == 0) item.regime.name
                else item.regime.span(prevRegime, ContextCompat.getColor(itemView.context, R.color.colorPrimary))

            val noteText = item.note?.trim()
            note.text = noteText
            note.appear(noteText != null)

            npsRowAdapter.submitList(item.npsList)
            npsParamsTable.appear(item.npsList.isNotEmpty())

            makeTimeBar(item)
        }

        private fun makeTimeBar(item: TehsectPlanStub) {
            val beginPercent = item.beginHm.percentOfDay()
            val percent = item.endHm.percentOfDay() - beginPercent

            val width = (timeBarWidth * percent).toInt()
            val start = (timeBarWidth * beginPercent).toInt() + timeBarMarginRoot

            timebar.layoutParams.width = width
            timebar.backgroundResource =
                    if (item.isStub) R.color.grey_light
                    else R.color.yellow_dark

            ConstraintSet().apply {
                clone(root)
                connect(timebar.id, ConstraintSet.START, root.id, ConstraintSet.START, start)
                applyTo(root)
            }
        }

    }

}