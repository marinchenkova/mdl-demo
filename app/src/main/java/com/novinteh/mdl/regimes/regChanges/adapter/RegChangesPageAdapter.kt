package com.novinteh.mdl.regimes.regChanges.adapter

import android.view.View
import com.novinteh.mdl.entity.util.DateUtils
import com.novinteh.mdl.paged.viewpager.adapter.PageAdapter
import com.novinteh.mdl.paged.viewpager.viewholder.PageViewHolder
import com.novinteh.mdl.regimes.regChanges.model.RegChangeListItem


class RegChangesPageAdapter : PageAdapter<RegChangeListItem, RegChangesPageAdapter.ViewHolder>() {

    override val LOG: Boolean get() = true
    override val LOG_TAG: String get() = "RegChangesPageAdapter"


    override fun onCreateViewHolder(view: View): ViewHolder {
        return ViewHolder(view)
    }


    class ViewHolder(view: View): PageViewHolder<RegChangeListItem>(view) {

        override val subAdapter = RegChangeAdapter()

    }

}