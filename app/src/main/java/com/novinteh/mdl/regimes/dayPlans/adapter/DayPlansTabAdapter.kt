package com.novinteh.mdl.regimes.dayPlans.adapter

import android.view.View
import com.novinteh.mdl.paged.model.Tab
import com.novinteh.mdl.paged.viewpager.adapter.TabAdapter
import com.novinteh.mdl.paged.viewpager.viewholder.TabViewHolder


class DayPlansTabAdapter : TabAdapter<Tab, TabViewHolder<Tab>>(Tab.DiffCallback) {

    override val LOG = false
    override val LOG_TAG = "DayPlansTabAdapter"


    override fun createViewHolder(view: View): TabViewHolder<Tab> {
        return TabViewHolder(view)
    }

}