package com.novinteh.mdl.regimes.dayPlans

import android.view.Menu
import androidx.lifecycle.Observer
import com.google.gson.Gson
import com.novinteh.mdl.R
import com.novinteh.mdl.paged.activity.PageActivity
import com.novinteh.mdl.paged.filter.changeFilter
import com.novinteh.mdl.paged.model.Tab
import com.novinteh.mdl.regimes.common.model.FilterTitle
import com.novinteh.mdl.regimes.dayPlans.adapter.DayPlansPageAdapter
import com.novinteh.mdl.regimes.dayPlans.adapter.DayPlansTabAdapter
import com.novinteh.mdl.regimes.dayPlans.details.DayPlanDetailsActivity
import com.novinteh.mdl.regimes.dayPlans.details.adapter.DayPlanDetailsAdapter
import com.novinteh.mdl.regimes.dayPlans.filter.DayPlansFilterActivity
import com.novinteh.mdl.regimes.dayPlans.model.DayPlanFilter
import com.novinteh.mdl.regimes.dayPlans.model.DayPlanListItem
import com.novinteh.mdl.regimes.regChanges.details.RegChangeDetailsActivity
import com.novinteh.mdl.util.RefreshRate
import com.novinteh.mdl.util.getAomnId
import com.novinteh.mdl.util.getProdType
import com.novinteh.mdl.util.iconRes
import org.jetbrains.anko.startActivity


class DayPlansActivity : PageActivity<DayPlanListItem, Tab, DayPlanFilter, DayPlansViewModel>() {

    override val LOG = true
    override val LOG_TAG = "DayPlansActivity"

    override val titleRes = R.string.activity_day_plan_title
    override val refreshRate = RefreshRate(hours = 0, minutes = 2, seconds = 0)

    override val pageAdapter = DayPlansPageAdapter()
    override val tabAdapter = DayPlansTabAdapter()

    override val viewModelClass = DayPlansViewModel::class.java
    override val filterActivityClass = DayPlansFilterActivity::class.java

    override var filter = DayPlanFilter()


    override fun getFilterFromJson(json: String?): DayPlanFilter? {
        return Gson().fromJson(json, DayPlanFilter::class.java)
    }

    override fun buildEmptyTab(id: String): Tab {
        return Tab(id)
    }

    override fun viewDetails(initialPosition: Int, date: String) {
        viewModel.getDetails(date, filter) { details ->
            startActivity<DayPlanDetailsActivity>(
                DATE to date,
                DETAILS_LIST_INIT_POSITION to initialPosition,
                DETAILS_LIST to Gson().toJson(details)
            )
        }
    }

    override fun onInitFilter() {
        changeFilter {
            prodType = getProdType()
            aomnId = getAomnId()
        }

        viewModel.filterTitle.observe(this, Observer { title ->
            when (title) {
                is FilterTitle.Text -> setSubtitle(title.text)
                is FilterTitle.All -> setSubtitle(getString(R.string.shift_filter_direction_all))
                is FilterTitle.NotSet -> setSubtitle(getString(R.string.not_set))
            }
        })

        viewModel.aomns.observe(this, Observer { list ->
            list?.firstOrNull()?.also { aomn ->
                changeFilter { setAomnIdIfAbsent(aomn.id) }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        return super.onCreateOptionsMenu(menu).also {
            menu.findItem(R.id.menu_prod_type).icon = getDrawable(filter.prodType.iconRes())
        }
    }

}
