package com.novinteh.mdl.regimes.regChanges.filter.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.novinteh.mdl.R
import com.novinteh.mdl.entity.data.common.ProdType
import com.novinteh.mdl.paged.filter.Filterable
import com.novinteh.mdl.util.iconRes
import com.novinteh.mdl.util.stringRes
import com.novinteh.mdl.view.recycler.adapter.RadioAdapter
import com.novinteh.mdl.view.recycler.viewholder.CheckViewHolder
import com.novinteh.mdl.view.recycler.viewholder.ClickViewHolder
import kotlinx.android.synthetic.main.item_radio_prod_type.view.*
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.textResource


class ProdTypeAdapter: RadioAdapter<ProdType, ProdTypeAdapter.ViewHolder>(DiffCallback),
    Filterable<ProdType> {

    override var filter = ProdType.OIL


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_radio_prod_type, parent, false)

        return ViewHolder(view).also { holder ->
            holder.init(clickListener)
        }
    }

    override fun onFilterChanged(value: ProdType) {
        checkFilterPosition()
    }

    override fun submitList(list: List<ProdType>?) {
        super.submitList(list)
        checkFilterPosition()
    }

    private fun checkFilterPosition() {
        val position = positionOf(filter)
        if (position != RecyclerView.NO_POSITION) switch(position)
    }


    class ViewHolder(view: View) : CheckViewHolder<ProdType>(view) {

        private val button = itemView.button
        private val icon = itemView.icon
        private val title = itemView.title


        override fun content(item: ProdType) {
            title.textResource = item.stringRes()
            icon.imageResource = item.iconRes()
        }

        override fun check() {
            button.isChecked = true
        }

        override fun uncheck() {
            button.isChecked = false
        }

    }


    companion object {

        private val DiffCallback = object : DiffUtil.ItemCallback<ProdType>() {
            override fun areItemsTheSame(oldItem: ProdType, newItem: ProdType): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: ProdType, newItem: ProdType): Boolean {
                return oldItem == newItem
            }
        }

    }

}