package com.novinteh.mdl.regimes.regChanges.persistence.converters

import androidx.room.TypeConverter
import com.google.gson.reflect.TypeToken
import com.novinteh.mdl.entity.JsonParser

class DirectionConverters {

    private val parser = JsonParser()


    @TypeConverter
    fun aomnIdsToString(aomnIds: List<Int>): String = parser.toJson(aomnIds)

    @TypeConverter
    fun aomnIdsFromString(str: String): List<Int> = parser
        .listFromJson(str, object : TypeToken<List<Int>>(){})

}