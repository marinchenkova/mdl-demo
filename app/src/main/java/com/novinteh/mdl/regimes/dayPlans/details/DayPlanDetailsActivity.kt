package com.novinteh.mdl.regimes.dayPlans.details

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.novinteh.mdl.R
import com.novinteh.mdl.entity.data.tehsectPlans.TehsectPlan
import com.novinteh.mdl.paged.activity.DetailsActivity
import com.novinteh.mdl.regimes.dayPlans.details.adapter.DayPlanDetailsAdapter
import com.novinteh.mdl.regimes.dayPlans.model.DayPlanItem
import com.novinteh.mdl.regimes.dayPlans.model.TehsectPlanItem
import com.novinteh.mdl.regimes.regChanges.details.adapter.RegChangeDetailsAdapter
import com.novinteh.mdl.regimes.regChanges.model.RegChangeItem


class DayPlanDetailsActivity : DetailsActivity<DayPlanItem>() {

    override val adapter = DayPlanDetailsAdapter()
    override val titleRes = R.string.activity_day_plan_details_title


    override fun getDetailsFromJson(json: String?): List<DayPlanItem>? {
        return Gson().fromJson(json, object : TypeToken<List<DayPlanItem>>(){}.type)
    }

}
