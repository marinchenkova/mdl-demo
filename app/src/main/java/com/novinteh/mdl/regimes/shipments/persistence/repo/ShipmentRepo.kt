package com.novinteh.mdl.regimes.shipments.persistence.repo

import androidx.lifecycle.LiveData
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.novinteh.mdl.data.repo.AsyncRepository
import com.novinteh.mdl.regimes.regChanges.model.RegChangeItem
import com.novinteh.mdl.regimes.regChanges.model.RegChangeListItem
import com.novinteh.mdl.entity.data.common.ProdType
import com.novinteh.mdl.entity.data.regchanges.RegChange
import com.novinteh.mdl.paged.filter.RangeSearch
import com.novinteh.mdl.regimes.regChanges.model.RegChangeFilter
import com.novinteh.mdl.regimes.regChanges.model.toItem
import com.novinteh.mdl.regimes.regChanges.persistence.dao.RegChangeDao
import com.novinteh.mdl.rest.RequestService
import com.novinteh.mdl.data.Result
import com.novinteh.mdl.entity.data.shipments.Shipment
import com.novinteh.mdl.regimes.common.persistence.dao.StateDao
import com.novinteh.mdl.regimes.shipments.model.ShipmentFilter
import com.novinteh.mdl.regimes.shipments.model.ShipmentItem
import com.novinteh.mdl.regimes.shipments.model.ShipmentListItem
import com.novinteh.mdl.regimes.shipments.model.toItem
import com.novinteh.mdl.regimes.shipments.persistence.dao.ShipmentDao

class ShipmentRepo(stateDao: StateDao, private val dao: ShipmentDao) : AsyncRepository(stateDao) {

    override val LOG = true
    override val LOG_TAG = "ShipmentRepo"

    override val stateId = "state.shipments"

    private val gson = GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()


    fun listItems(search: RangeSearch<ShipmentFilter>): LiveData<List<ShipmentListItem>> {
        return dao.listItems(
            search.fromDate,
            search.toDate,
            search.filter.prodType,
            search.filter.portId
        )
    }

    fun details(date: String, prodType: ProdType, portId: Int): List<ShipmentItem> {
        return dao.items(date, prodType, portId)
    }

    override suspend fun download(id: String): Result {
        return RequestService.getShipments(id)
    }

    override suspend fun upsert(id: String, data: Result.Success.Data) {
        val list = toList(id, data)
        dao.insert(list)
    }

    override suspend fun contains(id: String): Boolean {
        val count = dao.count(id)
        return count > 0
    }

    private fun toList(date: String, data: Result.Success.Data): List<ShipmentItem> = data.responseBody.charStream().use { reader -> gson
        .fromJson<List<Shipment>>(reader, object : TypeToken<List<Shipment>>() {}.type)
        ?.map { it.toItem(date) }
        ?: emptyList()
    }

}