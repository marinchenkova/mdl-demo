package com.novinteh.mdl.regimes.shipments.persistence.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.novinteh.mdl.regimes.common.model.AomnItem
import com.novinteh.mdl.regimes.shipments.model.PortItem


@Dao
interface PortDao {

    @Query("SELECT * from ports ORDER BY id")
    fun items(): LiveData<List<PortItem>>

    @Query("SELECT count(id) from ports")
    fun count(): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(list: List<PortItem>)

    @Query("DELETE FROM ports")
    suspend fun deleteAll()

}