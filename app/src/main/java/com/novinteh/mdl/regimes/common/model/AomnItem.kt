package com.novinteh.mdl.regimes.common.model

import androidx.recyclerview.widget.DiffUtil
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.novinteh.mdl.entity.data.common.ProdType
import com.novinteh.mdl.entity.data.structure.Aomn


@Entity(tableName = "aomns")
data class AomnItem(
    @PrimaryKey val id: Int,
    val rowN: Int,
    val name: String?
) {


    companion object {

        val DiffCallback = object : DiffUtil.ItemCallback<AomnItem>() {
            override fun areItemsTheSame(oldItem: AomnItem, newItem: AomnItem): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: AomnItem, newItem: AomnItem): Boolean {
                return oldItem == newItem
            }
        }

    }

}

fun Aomn.toItem() = AomnItem(id, rowN, name)