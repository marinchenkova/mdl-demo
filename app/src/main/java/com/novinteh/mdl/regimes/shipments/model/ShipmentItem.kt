package com.novinteh.mdl.regimes.shipments.model

import androidx.recyclerview.widget.DiffUtil
import androidx.room.Embedded
import androidx.room.Entity
import com.novinteh.mdl.entity.data.common.ProdType
import com.novinteh.mdl.entity.data.shipments.Operation
import com.novinteh.mdl.entity.data.shipments.Shipment
import com.novinteh.mdl.entity.data.shipments.ShipmentState
import com.novinteh.mdl.entity.data.structure.Port

@Entity(tableName = "shipments", primaryKeys = ["id", "date"])
data class ShipmentItem(
    val id: Int,
    val date: String,
    val rowN: Int,

    @Embedded(prefix = "port_")
    val port: Port,
    val tankerName: String,

    val prodType: ProdType,
    val cargo: String,

    val loadBeginStr: String,
    val loadEndStr: String,

    val timeWaitStr: String,
    val timeRaidStr: String,

    val moorBeginStr: String,
    val moorEndStr: String,

    @Embedded(prefix = "position_")
    val position: Shipment.Position,

    val plan: Float?,
    val fact: Float?,
    val sera: Float?,
    val pl20: Float?,

    val berthNum: String?,
    val state: ShipmentState,

    val timesheet: List<Operation>
) {

    companion object {

        val DiffCallback = object : DiffUtil.ItemCallback<ShipmentItem>() {
            override fun areItemsTheSame(oldItem: ShipmentItem, newItem: ShipmentItem): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: ShipmentItem, newItem: ShipmentItem): Boolean {
                return oldItem == newItem
            }
        }

    }

}


fun Shipment.toItem(date: String) = ShipmentItem(
    id, date, rowN,
    port, tankerName,
    prodType, cargo,
    loadBeginStr, loadEndStr,
    timeWaitStr, timeRaidStr,
    moorBeginStr, moorEndStr,
    position,
    plan, fact, sera, pl20,
    berthNum,
    state,
    timesheet
)