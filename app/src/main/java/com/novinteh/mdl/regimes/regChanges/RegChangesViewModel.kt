package com.novinteh.mdl.regimes.regChanges

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.novinteh.mdl.data.MdlDatabase
import com.novinteh.mdl.paged.model.Page
import com.novinteh.mdl.paged.viewmodel.PageViewModel
import com.novinteh.mdl.regimes.common.model.LoadStateInfo
import com.novinteh.mdl.util.Timestamp
import com.novinteh.mdl.regimes.common.persistence.repo.AomnRepo
import com.novinteh.mdl.regimes.common.model.FilterTitle
import com.novinteh.mdl.regimes.regChanges.model.RegChangeFilter
import com.novinteh.mdl.regimes.regChanges.model.*
import com.novinteh.mdl.regimes.regChanges.persistence.repo.DirectionRepo
import com.novinteh.mdl.regimes.regChanges.persistence.repo.RegChangeRepo
import com.novinteh.mdl.regimes.regChanges.persistence.repo.UserShiftRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class RegChangesViewModel(app: Application) : PageViewModel<RegChangeListItem, RegChangeTab, RegChangeFilter>(app) {

    override val LOG: Boolean get() = true
    override val LOG_TAG: String = "RegChangesVM"

    val currentDate = MutableLiveData<String>()
    val today = MutableLiveData<String>()

    override val pages: LiveData<List<Page<RegChangeListItem>>>
    override val tabs: LiveData<List<RegChangeTab>>
    override val state: LiveData<LoadStateInfo>

    override val repo: RegChangeRepo
    private val directionRepo: DirectionRepo
    private val aomnRepo: AomnRepo
    private val userShiftRepo: UserShiftRepo

    val userShifts: LiveData<List<UserShiftWithAomns>>
    val filterTitle: LiveData<FilterTitle>


    init {
        val db = MdlDatabase.getInstance(app)
        val stateDao = db.stateDao()

        repo = RegChangeRepo(stateDao, db.regChangeDao())
        directionRepo = DirectionRepo(stateDao, db.directionDao())
        userShiftRepo = UserShiftRepo(stateDao, db.userShiftDao())
        aomnRepo = AomnRepo(stateDao, db.aomnDao())

        launch { aomnRepo.loadIfAbsent() }

        val dirsLiveData = Transformations.switchMap(currentDate) { date ->
            directionRepo.items(date)
        }

        val dirsWithAomnsLiveData = dirsLiveData
            .combineWith(aomnRepo.items()) { dirs, aomns ->
                dirs.map { dir -> DirWithAomns(
                    dir.id,
                    dir.name,
                    aomns.filter { aomn -> aomn.id in dir.aomnIds }
                )}
            }

        filterTitle = search.combineWith(dirsWithAomnsLiveData) { search, dirsWithAomns ->
            buildSubtitle(search.filter, dirsWithAomns)
        }

        state = repo.state

        val userShiftItems = Transformations.switchMap(today) { date ->
            launch { userShiftRepo.loadIfAbsent(date, Timestamp.now()) }
            userShiftRepo.items(date)
        }

        userShifts = userShiftItems.combineWith(dirsLiveData) { userShifts, dirs ->
            userShifts.map { shift ->
                val aomnIds = dirs
                    .find { dir -> dir.id == shift.directionId }
                    ?.aomnIds
                    ?: emptyList()

                UserShiftWithAomns(shift.directionId, aomnIds)
            }
        }

        val listItems = Transformations.switchMap(search) { search ->
            repo.listItems(search)
        }

        pages = search.combineWith(listItems) { search, items ->
            search.currentDates.map { date ->
                Page(date, items.filter { it.date == date })
            }
        }

        tabs = pages.mapAsync { regChangePages ->
            regChangePages.map { page -> RegChangeTab.from(page) }
        }
    }

    fun getDetails(date: String, filter: RegChangeFilter, onReceive: (List<RegChangeItem>) -> Unit) {
        launch {
            val details = repo.details(date, filter.prodType, filter.aomnIds)
            withContext(Dispatchers.Main) { onReceive(details) }
        }
    }

    private fun buildSubtitle(filter: RegChangeFilter, dirs: List<DirWithAomns>): FilterTitle {
        if (filter.dirIsAll) return FilterTitle.All

        val dir = dirs.find { dir -> dir.id == filter.dirId }
        val aomnIds = filter.aomnIds


        return when {
            aomnIds.isEmpty() -> {
                FilterTitle.NotSet
            }

            aomnIds.size == 1 -> {
                val firstId = aomnIds.first()
                val aomn = dir?.aomns?.find { aomn -> aomn.id == firstId }
                FilterTitle.Text(aomn?.name ?: "")
            }

            else -> {
                FilterTitle.Text(dir?.name ?: "")
            }
        }
    }

    override fun refresh(dates: List<String>, timestamp: Timestamp) {
        launch {
            repo.load(dates, timestamp)
            directionRepo.load(dates, timestamp)
        }

        launch { aomnRepo.loadIfAbsent() }

        today.value?.also { today ->
            launch { userShiftRepo.loadIfAbsent(today, Timestamp.now()) }
        }
    }

    override fun loadIfAbsent(dates: List<String>, timestamp: Timestamp) {
        launch {
            repo.loadIfAbsent(dates, timestamp)
            directionRepo.loadIfAbsent(dates, timestamp)
        }
    }

}