package com.novinteh.mdl.regimes.shipments.model

interface ShipmentCargo {

    fun getCargo(): String

}

data class CargoSection(private val cargo: String) : ShipmentCargo {

    override fun getCargo(): String {
        return cargo
    }

}