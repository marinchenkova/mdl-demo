package com.novinteh.mdl.regimes.shipments.persistence.repo

import androidx.lifecycle.LiveData
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.novinteh.mdl.entity.data.structure.Aomn
import com.novinteh.mdl.regimes.common.model.AomnItem
import com.novinteh.mdl.regimes.common.persistence.dao.AomnDao
import com.novinteh.mdl.rest.RequestService
import com.novinteh.mdl.data.Result
import com.novinteh.mdl.data.repo.Repository
import com.novinteh.mdl.data.repo.SingleIdRepository
import com.novinteh.mdl.entity.data.structure.Port
import com.novinteh.mdl.regimes.common.model.toItem
import com.novinteh.mdl.regimes.common.persistence.dao.StateDao
import com.novinteh.mdl.regimes.shipments.model.PortItem
import com.novinteh.mdl.regimes.shipments.model.toItem
import com.novinteh.mdl.regimes.shipments.persistence.dao.PortDao

class PortRepo(stateDao: StateDao, private val dao: PortDao) : SingleIdRepository(stateDao) {

    override val LOG = true
    override val LOG_TAG = "PortRepo"

    override val singleId = "ports"
    override val stateId = "state.ports"

    private val gson: Gson = GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()


    fun items(): LiveData<List<PortItem>> = dao.items()

    override suspend fun download(id: String): Result {
        return RequestService.getPorts()
    }

    override suspend fun contains(id: String): Boolean {
        val count = dao.count()
        return count > 0
    }

    override suspend fun upsert(id: String, data: Result.Success.Data) {
        val list = toList(data)
        dao.insert(list)
    }

    private fun toList(data: Result.Success.Data): List<PortItem> = data.responseBody.charStream().use { reader -> gson
        .fromJson<List<Port>>(reader, object : TypeToken<List<Port>>() {}.type)
        ?.map { it.toItem() }
        ?: emptyList()
    }

}