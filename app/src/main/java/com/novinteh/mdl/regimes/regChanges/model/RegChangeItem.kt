package com.novinteh.mdl.regimes.regChanges.model

import androidx.recyclerview.widget.DiffUtil
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.novinteh.mdl.entity.data.common.ProdType
import com.novinteh.mdl.entity.data.common.Regime
import com.novinteh.mdl.entity.data.regchanges.QKind
import com.novinteh.mdl.entity.data.regchanges.RegChange
import com.novinteh.mdl.entity.data.regchanges.RegChangeState
import com.novinteh.mdl.entity.data.regchanges.Switch

@Entity(tableName = "reg_changes", primaryKeys = ["id", "date"])
data class RegChangeItem(
    val id: String,
    val date: String,
    val rowN: Int,
    val aomnId: Int,
    val aomnName: String?,
    val prodType: ProdType,
    val tehsectName: String,
    val planDate: String,
    val factDate: String,
    val fromReg: Regime,
    val toReg: Regime,
    val qKind: QKind,
    val planQPrev: Int?,
    val planQCurr: Int?,
    val factQPrev: Int?,
    val factQCurr: Int?,
    val isPlanNextDay: Boolean,
    val isFactNextDay: Boolean,
    val state: RegChangeState,
    val switches: List<Switch>
) {

    companion object {

        val DiffCallback = object : DiffUtil.ItemCallback<RegChangeItem>() {
            override fun areItemsTheSame(oldItem: RegChangeItem, newItem: RegChangeItem): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: RegChangeItem, newItem: RegChangeItem): Boolean {
                return oldItem == newItem
            }
        }

    }

}


fun RegChange.toItem(date: String) = RegChangeItem(
    id, date, rowN,
    aomnId, aomnName, prodType, tehsectName ?: "",
    planDateStr, factDateStr,
    fromReg, toReg,
    qKind, planQPrev, planQCurr, factQPrev, factQCurr,
    isPlanNextDay, isFactNextDay,
    state,
    switches
)