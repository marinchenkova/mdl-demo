package com.novinteh.mdl.regimes.shipments.filter

import android.app.Application
import androidx.lifecycle.LiveData
import com.novinteh.mdl.data.MdlDatabase
import com.novinteh.mdl.paged.viewmodel.FilterViewModel
import com.novinteh.mdl.paged.viewmodel.ScopeViewModel
import com.novinteh.mdl.regimes.common.model.AomnItem
import com.novinteh.mdl.regimes.common.model.LoadStateInfo
import com.novinteh.mdl.regimes.common.persistence.repo.AomnRepo
import com.novinteh.mdl.regimes.shipments.model.PortItem
import com.novinteh.mdl.regimes.shipments.persistence.repo.PortRepo
import kotlinx.coroutines.launch

class ShipmentFilterViewModel(app: Application) : FilterViewModel(app) {

    override val LOG = true
    override val LOG_TAG = "ShipmentFilterVM"

    override val state: LiveData<LoadStateInfo>
    val ports: LiveData<List<PortItem>>

    private val portRepo: PortRepo


    init {
        val db = MdlDatabase.getInstance(app)
        val stateDao = db.stateDao()

        portRepo = PortRepo(stateDao, db.portDao())
        launch { portRepo.loadIfAbsent() }

        state = portRepo.state
        ports = portRepo.items()
    }

    override fun refresh() {
        launch { portRepo.loadIfAbsent() }
    }

}