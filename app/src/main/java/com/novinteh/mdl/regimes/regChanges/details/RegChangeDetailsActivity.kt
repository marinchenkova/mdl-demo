package com.novinteh.mdl.regimes.regChanges.details

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.novinteh.mdl.R
import com.novinteh.mdl.paged.activity.DetailsActivity
import com.novinteh.mdl.regimes.regChanges.details.adapter.RegChangeDetailsAdapter
import com.novinteh.mdl.regimes.regChanges.model.RegChangeItem


class RegChangeDetailsActivity : DetailsActivity<RegChangeItem>() {

    override val adapter = RegChangeDetailsAdapter()
    override val titleRes = R.string.activity_reg_change_details_title


    override fun getDetailsFromJson(json: String?): List<RegChangeItem>? {
        return Gson().fromJson(json, object : TypeToken<List<RegChangeItem>>(){}.type)
    }

}
