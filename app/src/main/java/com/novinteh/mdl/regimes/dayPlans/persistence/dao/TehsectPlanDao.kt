package com.novinteh.mdl.regimes.dayPlans.persistence.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.novinteh.mdl.regimes.regChanges.model.RegChangeItem
import com.novinteh.mdl.regimes.regChanges.model.RegChangeListItem
import com.novinteh.mdl.entity.data.common.ProdType
import com.novinteh.mdl.regimes.dayPlans.model.TehsectPlanItem
import com.novinteh.mdl.regimes.dayPlans.model.TehsectPlanListItem


@Dao
interface TehsectPlanDao {

    @Query("""
        SELECT * from tehsectplanlistitem 
        WHERE (date between :fromDate AND :toDate) 
          AND (prodType = :prodType) 
          AND (aomnId = :aomnId) 
        ORDER BY DATE
        """)
    fun listItems(fromDate: String, toDate: String, prodType: ProdType, aomnId: Int): LiveData<List<TehsectPlanListItem>>

    @Query("""
        SELECT * from tehsect_plans 
        WHERE date = :date 
          AND prodType = :prodType 
          AND aomnId = :aomnId 
        ORDER BY rowN
    """)
    fun items(date: String, prodType: ProdType, aomnId: Int): List<TehsectPlanItem>

    @Query("SELECT count(id) from tehsect_plans WHERE date = :date")
    fun count(date: String): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(list: List<TehsectPlanItem>)

    @Query("DELETE FROM tehsect_plans")
    suspend fun deleteAll()

}