package com.novinteh.mdl.regimes.dayPlans.details.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.novinteh.mdl.R
import com.novinteh.mdl.entity.data.tehsectPlans.Nps
import com.novinteh.mdl.view.recycler.adapter.ItemAdapter
import com.novinteh.mdl.view.recycler.viewholder.BindViewHolder
import kotlinx.android.synthetic.main.item_tehsect_plan_details_nps_row.view.*


class NpsRowAdapter : ItemAdapter<Nps, NpsRowAdapter.NpsRowViewHolder>(DiffCallback) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): NpsRowViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_tehsect_plan_details_nps_row, parent, false)

        return NpsRowViewHolder(view)
    }


    class NpsRowViewHolder(view: View) : BindViewHolder<Nps>(view) {

        private val npsName = itemView.nps_name
        private val onList = itemView.on_list
        private val pIn = itemView.p_in
        private val pOut = itemView.p_out


        override fun content(item: Nps) {
            npsName.text = item.npsName
            onList.text = item.agrs
            pIn.text = item.pIn?.toString()
            pOut.text = item.pOut?.toString()
        }

    }


    companion object {

        private val DiffCallback = object : DiffUtil.ItemCallback<Nps>() {
            override fun areItemsTheSame(oldItem: Nps, newItem: Nps): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Nps, newItem: Nps): Boolean {
                return oldItem == newItem
            }
        }

    }

}