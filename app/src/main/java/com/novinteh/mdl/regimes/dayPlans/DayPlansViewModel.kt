package com.novinteh.mdl.regimes.dayPlans

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.novinteh.mdl.data.MdlDatabase
import com.novinteh.mdl.paged.model.Page
import com.novinteh.mdl.paged.model.Tab
import com.novinteh.mdl.paged.viewmodel.PageViewModel
import com.novinteh.mdl.regimes.common.model.AomnItem
import com.novinteh.mdl.regimes.common.model.FilterTitle
import com.novinteh.mdl.regimes.common.model.LoadStateInfo
import com.novinteh.mdl.util.Timestamp
import com.novinteh.mdl.regimes.common.persistence.repo.AomnRepo
import com.novinteh.mdl.regimes.dayPlans.model.DayPlanFilter
import com.novinteh.mdl.regimes.dayPlans.model.DayPlanItem
import com.novinteh.mdl.regimes.dayPlans.model.DayPlanListItem
import com.novinteh.mdl.regimes.dayPlans.model.mapping.TehsectPlanListStub
import com.novinteh.mdl.regimes.dayPlans.model.mapping.TehsectPlanStub
import com.novinteh.mdl.regimes.dayPlans.model.mapping.fillVoids
import com.novinteh.mdl.regimes.dayPlans.model.mapping.filterSimilar
import com.novinteh.mdl.regimes.dayPlans.persistence.repo.TehsectPlanRepo
import com.novinteh.mdl.regimes.regChanges.model.RegChangeFilter
import com.novinteh.mdl.regimes.regChanges.model.RegChangeItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class DayPlansViewModel(app: Application) : PageViewModel<DayPlanListItem, Tab, DayPlanFilter>(app) {

    override val LOG: Boolean get() = true
    override val LOG_TAG: String = "DayPlansViewModel"

    override val repo: TehsectPlanRepo
    private val aomnRepo: AomnRepo

    override val pages: LiveData<List<Page<DayPlanListItem>>>
    override val tabs: LiveData<List<Tab>>
    override val state: LiveData<LoadStateInfo>

    val filterTitle: LiveData<FilterTitle>
    val aomns: LiveData<List<AomnItem>>


    init {
        val db = MdlDatabase.getInstance(app)
        val stateDao = db.stateDao()

        repo = TehsectPlanRepo(stateDao, db.tehsectPlanDao())
        aomnRepo = AomnRepo(stateDao, db.aomnDao())

        launch { aomnRepo.loadIfAbsent() }

        aomns = aomnRepo.items()

        state = repo.state

        tabs = Transformations.map(search) { search ->
            search.currentDates.map { Tab(it) }
        }

        filterTitle = search.combineWith(aomns) { search, aomns ->
            buildSubtitle(search.filter, aomns)
        }

        val listItems = Transformations.switchMap(search) { search ->
            repo.listItems(search)
        }

        pages = search.combineWith(listItems) { search, tehsectPlans ->
            search.currentDates.map { date ->
                val dayPlans = tehsectPlans
                    .filter { it.date == date }
                    .groupBy { it.tehsectName }
                    .map { entry ->
                        DayPlanListItem(
                            entry.key,
                            entry.value
                                .map { plan -> plan.toStub() }
                                .fillVoids(onEmpty = TehsectPlanListStub.Empty)
                                .filterSimilar()
                        )
                    }

                Page(date, dayPlans)
            }
        }
    }

    private fun buildSubtitle(filter: DayPlanFilter, aomns: List<AomnItem>): FilterTitle {
        if (filter.aomnIdAbsent || aomns.isEmpty()) return FilterTitle.NotSet

        val aomn = aomns.find { it.id == filter.aomnId }
        return FilterTitle.Text(aomn?.name ?: "")
    }

    fun getDetails(date: String, filter: DayPlanFilter, onReceive: (List<DayPlanItem>) -> Unit) {
        launch {
            val items = repo.details(date, filter.prodType, filter.aomnId)
            val aomnName = items.firstOrNull()?.aomnName ?: ""
            val details = items
                .groupBy { it.tehsectName ?: "" }
                .map { entry ->
                    DayPlanItem(
                        aomnName,
                        entry.key,
                        entry.value
                            .map { plan -> plan.toStub() }
                            .fillVoids(onEmpty = TehsectPlanStub.Empty)
                    )
                }

            withContext(Dispatchers.Main) { onReceive(details) }
        }
    }

    override fun refresh(dates: List<String>, timestamp: Timestamp) {
        launch {
            repo.load(dates, timestamp)
            aomnRepo.loadIfAbsent()
        }
    }

    override fun loadIfAbsent(dates: List<String>, timestamp: Timestamp) {
        launch {
            repo.loadIfAbsent(dates, timestamp)
        }
    }

}