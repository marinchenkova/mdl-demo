package com.novinteh.mdl.regimes.regChanges.filter

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.novinteh.mdl.R
import com.novinteh.mdl.data.Result
import com.novinteh.mdl.entity.data.common.ProdType
import com.novinteh.mdl.paged.activity.FilterActivity
import com.novinteh.mdl.paged.filter.changeFilter
import com.novinteh.mdl.paged.filter.setFilter
import com.novinteh.mdl.regimes.regChanges.filter.adapter.DirectionAdapter
import com.novinteh.mdl.regimes.regChanges.filter.adapter.ProdTypeAdapter
import com.novinteh.mdl.regimes.regChanges.model.DirWithAomns
import com.novinteh.mdl.regimes.regChanges.model.RegChangeFilter
import com.novinteh.mdl.util.getToday
import com.novinteh.mdl.util.pickErrorMessage
import com.novinteh.mdl.util.setProdType
import com.novinteh.mdl.view.show
import kotlinx.android.synthetic.main.activity_filter.*
import org.jetbrains.anko.textResource


class RegChangesFilterActivity : FilterActivity<RegChangeFilter, RegChangesFilterViewModel>() {

    override val LOG = true
    override val LOG_TAG = "RegChangesFilterActivity"

    override var filter = RegChangeFilter()

    private val prodTypeAdapter = ProdTypeAdapter()
    private val directionAdapter = DirectionAdapter()

    override val viewModelClass = RegChangesFilterViewModel::class.java


    override fun onDateSet(date: String) {
        initViewModel(date)
        initFilter()
        initProdTypes()
        initDirections()
    }

    private fun initViewModel(date: String) {
        viewModel.currentDate.value = date
        viewModel.today.value = getToday()
    }

    private fun initFilter() {
        viewModel.userShifts.observe(this, Observer { list ->
            list?.firstOrNull()?.directionId?.also {
                changeFilter { setUserShiftDirId(it) }
            }
        })
    }

    override fun getFilterFromJson(json: String?): RegChangeFilter? {
        return Gson().fromJson(json, RegChangeFilter::class.java)
    }

    override fun saveFilter(): RegChangeFilter? {
        val prodType = prodTypeAdapter.checkedItem
        val dirId = directionAdapter.checkedItem?.id
        val aomnIds = directionAdapter.checkedAomns.map { it.id }

        if (prodType == null || dirId == null) return null

        setProdType(prodType)

        with(filter) {
            this.prodType = prodType
            setUserDir(dirId, aomnIds)
        }

        return filter
    }

    override fun onFilterChanged(value: RegChangeFilter) {
        log("onFilterChanged to $value")
        prodTypeAdapter.setFilter(value.prodType)
        directionAdapter.setFilter(value)
    }

    private fun initProdTypes() {
        prodTypeAdapter.submitList(listOf(ProdType.OIL, ProdType.NP))

        recycler_prod_types.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recycler_prod_types.itemAnimator = null
        recycler_prod_types.adapter = prodTypeAdapter
    }

    private fun initDirections() {
        var submitted = false
        viewModel.dirs.observe(this, Observer { list ->
            if (submitted) return@Observer

            val aomns = list.orEmpty().flatMap { it.aomns }
            if (aomns.isEmpty()) return@Observer

            submitted = true
            val dirAll = DirWithAomns(
                RegChangeFilter.DIR_ALL,
                getString(R.string.shift_filter_direction_all),
                aomns
            )

            directionAdapter.submitList(listOf(dirAll) + list)
        })

        direction_header.textResource = R.string.shift_filter_direction_header

        recycler_directions.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recycler_directions.itemAnimator = null
        recycler_directions.adapter = directionAdapter
    }

    override fun isContentLoaded(): Boolean {
        return directionAdapter.itemCount > 0
    }

}
