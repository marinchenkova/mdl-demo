package com.novinteh.mdl.regimes.shipments.filter

import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.novinteh.mdl.R
import com.novinteh.mdl.paged.activity.FilterActivity
import com.novinteh.mdl.entity.data.common.ProdType
import com.novinteh.mdl.paged.filter.setFilter
import com.novinteh.mdl.regimes.regChanges.filter.adapter.ProdTypeAdapter
import com.novinteh.mdl.regimes.shipments.filter.adapter.PortAdapter
import com.novinteh.mdl.regimes.shipments.model.ShipmentFilter
//import com.novinteh.mdl.regimes.shipments.filter.adapter.PortAdapter
import com.novinteh.mdl.util.setPortId
import com.novinteh.mdl.util.setProdType
import kotlinx.android.synthetic.main.activity_filter.*
import org.jetbrains.anko.textResource


class ShipmentFilterActivity : FilterActivity<ShipmentFilter, ShipmentFilterViewModel>() {

    override val LOG = true
    override val LOG_TAG = "ShipmentFilterActivity"

    override var filter = ShipmentFilter()

    private val prodTypeAdapter = ProdTypeAdapter()
    private val portAdapter = PortAdapter()

    override val viewModelClass = ShipmentFilterViewModel::class.java


    override fun onDateSet(date: String) {
        initProdTypes()
        initPorts()
    }

    override fun saveFilter(): ShipmentFilter? {
        val prodType = prodTypeAdapter.checkedItem
        val portId = portAdapter.checkedItem?.id

        if (prodType == null || portId == null) return null

        setProdType(prodType)
        setPortId(portId)

        with(filter) {
            this.prodType = prodType
            this.portId = portId
        }

        return filter
    }

    override fun getFilterFromJson(json: String?): ShipmentFilter? {
        return Gson().fromJson(json, ShipmentFilter::class.java)
    }

    override fun onFilterChanged(value: ShipmentFilter) {
        prodTypeAdapter.setFilter(value.prodType)
        portAdapter.setFilter(value.portId)
    }

    private fun initPorts() {
        viewModel.ports.observe(this, Observer { list ->
            if (!list.isNullOrEmpty()) portAdapter.submitList(list)
        })

        direction_header.textResource = R.string.shift_filter_port_header
        recycler_directions.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recycler_directions.itemAnimator = null
        recycler_directions.adapter = portAdapter
    }

    private fun initProdTypes() {
        prodTypeAdapter.submitList(listOf(ProdType.OIL, ProdType.NP))

        recycler_prod_types.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recycler_prod_types.itemAnimator = null
        recycler_prod_types.adapter = prodTypeAdapter
    }

    override fun isContentLoaded(): Boolean {
        return portAdapter.itemCount > 0
    }

}
