package com.novinteh.mdl.regimes.shipments.adapter

import android.view.View
import com.novinteh.mdl.paged.viewpager.adapter.PageAdapter
import com.novinteh.mdl.paged.viewpager.viewholder.PageViewHolder
import com.novinteh.mdl.regimes.shipments.model.ShipmentListItem


class ShipmentsPageAdapter : PageAdapter<ShipmentListItem, ShipmentsPageAdapter.ViewHolder>() {

    override val LOG = true
    override val LOG_TAG = "DayPlansPageAdapter"


    override fun onCreateViewHolder(view: View): ViewHolder {
        return ViewHolder(view)
    }


    class ViewHolder(view: View): PageViewHolder<ShipmentListItem>(view) {

        override val subAdapter = ShipmentAdapter()

    }

}