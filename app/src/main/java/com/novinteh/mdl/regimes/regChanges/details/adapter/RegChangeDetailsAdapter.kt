package com.novinteh.mdl.regimes.regChanges.details.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.getColor
import androidx.recyclerview.widget.LinearLayoutManager
import com.novinteh.mdl.R
import com.novinteh.mdl.entity.data.regchanges.RegChangeState
import com.novinteh.mdl.regimes.regChanges.model.RegChangeItem
import com.novinteh.mdl.util.backgroundRes
import com.novinteh.mdl.util.imageRes
import com.novinteh.mdl.util.stringRes
import com.novinteh.mdl.view.appear
import com.novinteh.mdl.view.recycler.adapter.ItemAdapter
import com.novinteh.mdl.view.recycler.viewholder.BindViewHolder
import com.novinteh.mdl.view.show
import com.novinteh.mdl.view.span
import kotlinx.android.synthetic.main.item_reg_change_details.view.*
import org.jetbrains.anko.textResource


class RegChangeDetailsAdapter : ItemAdapter<RegChangeItem, RegChangeDetailsAdapter.ViewHolder>(
    RegChangeItem.DiffCallback
) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_reg_change_details, parent, false)

        return ViewHolder(view)
    }


    class ViewHolder(view: View) : BindViewHolder<RegChangeItem>(view) {

        private val back = itemView.details_top_background
        private val aomnName = itemView.details_aomn_name
        private val tehsectName = itemView.details_tehsect_name

        private val fromReg = itemView.details_from_reg
        private val toReg = itemView.activity_reg_change_details_to_reg

        private val plan = itemView.details_plan
        private val planHeader = itemView.details_plan_header
        private val ctrl = itemView.details_ctrl
        private val planDay = itemView.plan_day
        private val ctrlDay = itemView.details_ctrl_day

        private val qPlan = itemView.details_q_plan
        private val qPlanHeader = itemView.details_q_plan_header
        private val qFact = itemView.details_q_fact

        private val state = itemView.details_state
        private val stateImage = itemView.details_state_image

        private val switchesHeader = itemView.details_switches_header
        private val recyclerSwitches = itemView.recycler_switches
        private val adapter = SwitchAdapter()


        init {
            recyclerSwitches.layoutManager = LinearLayoutManager(itemView.context, LinearLayoutManager.VERTICAL, false)
            recyclerSwitches.adapter = adapter
        }

        override fun content(item: RegChangeItem) {
            back.setBackgroundResource(item.state.backgroundRes())

            aomnName.text = item.aomnName
            tehsectName.text = item.tehsectName

            fromReg.text = item.fromReg.name
            toReg.text = item.toReg.span(item.fromReg, getColor(itemView.context, R.color.text_light))

            plan.text = item.planDate
            ctrl.text = item.factDate

            planDay.show(item.isPlanNextDay)
            ctrlDay.show(item.isFactNextDay)

            if (item.state == RegChangeState.NOT_SCHEDULED) {
                planHeader.appear(false)
                plan.appear(false)
                qPlanHeader.appear(false)
                qPlan.appear(false)
            }

            val qPlanText = "${item.planQPrev} -> ${item.planQCurr}"
            val qFactText = "${item.factQPrev} -> ${item.factQCurr}"
            qPlan.text = qPlanText
            qFact.text = qFactText

            state.textResource = item.state.stringRes()
            stateImage.setImageResource(item.state.imageRes())

            switchesHeader.appear(item.switches.isNotEmpty())
            adapter.submitList(item.switches, item.fromReg)
        }

    }


}