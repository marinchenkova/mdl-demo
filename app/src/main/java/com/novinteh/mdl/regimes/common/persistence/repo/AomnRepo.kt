package com.novinteh.mdl.regimes.common.persistence.repo

import androidx.lifecycle.LiveData
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.novinteh.mdl.entity.data.structure.Aomn
import com.novinteh.mdl.regimes.common.model.AomnItem
import com.novinteh.mdl.regimes.common.persistence.dao.AomnDao
import com.novinteh.mdl.rest.RequestService
import com.novinteh.mdl.data.Result
import com.novinteh.mdl.data.repo.Repository
import com.novinteh.mdl.data.repo.SingleIdRepository
import com.novinteh.mdl.regimes.common.model.toItem
import com.novinteh.mdl.regimes.common.persistence.dao.StateDao

class AomnRepo(stateDao: StateDao, private val dao: AomnDao) : SingleIdRepository(stateDao) {

    override val LOG = true
    override val LOG_TAG = "AomnRepo"

    override val singleId = "aomns"
    override val stateId = "state.aomns"

    private val gson: Gson = GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()


    fun items(): LiveData<List<AomnItem>> = dao.items()

    override suspend fun download(id: String): Result {
        return RequestService.getAomns()
    }

    override suspend fun contains(id: String): Boolean {
        val count = dao.count()
        //log("contains: $count entries")
        return count > 0
    }

    override suspend fun upsert(id: String, data: Result.Success.Data) {
        val list = toList(data)
        //log("upsert list $list")
        dao.insert(list)
    }

    private fun toList(data: Result.Success.Data): List<AomnItem> = data.responseBody.charStream().use { reader -> gson
        .fromJson<List<Aomn>>(reader, object : TypeToken<List<Aomn>>() {}.type)
        ?.map { it.toItem() }
        ?: emptyList()
    }

}