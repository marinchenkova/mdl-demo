package com.novinteh.mdl.regimes.shipments.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.novinteh.mdl.R
import com.novinteh.mdl.entity.data.shipments.Operation
import com.novinteh.mdl.entity.data.shipments.Shipment
import com.novinteh.mdl.entity.util.DateUtils
import com.novinteh.mdl.view.recycler.adapter.ItemAdapter
import com.novinteh.mdl.view.recycler.viewholder.BindViewHolder
import kotlinx.android.synthetic.main.item_shipment_op.view.*


class OperationsAdapter : ItemAdapter<Operation, OperationsAdapter.OperationViewHolder>(DiffCallback) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): OperationViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_shipment_op, parent, false)

        return OperationViewHolder(view)
    }


    class OperationViewHolder(view: View) : BindViewHolder<Operation>(view) {
        
        private val op = itemView.op
        
        private val opBeginD = itemView.op_begin_d
        private val opBeginHm = itemView.op_begin_hm
        
        private val opEndD = itemView.op_end_d
        private val opEndHm = itemView.op_end_hm
        
        
        override fun content(item: Operation) {
            op.text = item.type

            val opBeginDate = DateUtils.parseYmdHm(item.beginDateStr)
            val opBeginDStr = DateUtils.formatD(opBeginDate)
            val opBeginHmStr = DateUtils.formatHm(opBeginDate)
            opBeginD.text = opBeginDStr
            opBeginHm.text = opBeginHmStr

            val opEndDate = DateUtils.parseYmdHm(item.endDateStr)
            val opEndDStr = DateUtils.formatD(opEndDate)
            val opEndHmStr = DateUtils.formatHm(opEndDate)
            opEndD.text = opEndDStr
            opEndHm.text = opEndHmStr
        }

    }


    companion object {

        private val DiffCallback = object : DiffUtil.ItemCallback<Operation>() {
            override fun areItemsTheSame(oldItem: Operation, newItem: Operation): Boolean {
                return oldItem.beginDateStr == newItem.beginDateStr &&
                        oldItem.endDateStr == newItem.endDateStr
            }

            override fun areContentsTheSame(oldItem: Operation, newItem: Operation): Boolean {
                return oldItem == newItem
            }
        }

    }

}