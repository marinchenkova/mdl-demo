package com.novinteh.mdl.regimes.dayPlans.model

import androidx.room.Entity
import com.novinteh.mdl.entity.data.common.ProdType
import com.novinteh.mdl.entity.data.common.Regime
import com.novinteh.mdl.entity.data.tehsectPlans.Nps
import com.novinteh.mdl.entity.data.tehsectPlans.TehsectPlan
import com.novinteh.mdl.entity.util.DateUtils
import com.novinteh.mdl.regimes.dayPlans.model.mapping.TehsectPlanListStub
import com.novinteh.mdl.regimes.dayPlans.model.mapping.TehsectPlanStub
import com.novinteh.mdl.util.Hm

@Entity(tableName = "tehsect_plans", primaryKeys = ["id", "date"])
data class TehsectPlanItem(
    val id: Int,
    val date: String,
    val rowN: Int,
    val aomnId: Int,
    val aomnName: String?,
    val tehsectName: String?,
    val prodType: ProdType,
    val beginStr: String,
    val endStr: String,
    val regimeQm3: Int,
    val regimeQt: Int,
    val regime: Regime,
    val npsList: List<Nps>,
    val note: String?
) {

    fun toStub(): TehsectPlanStub {
        val beginDate = DateUtils.parseYmdHm(beginStr)
        val endDate = DateUtils.parseYmdHm(endStr)
        val nextDay = DateUtils.diffDays(beginDate, endDate) > 0

        return TehsectPlanStub(
            Hm.fromDate(beginDate, false),
            Hm.fromDate(endDate, nextDay),
            regimeQt,
            regime,
            note,
            npsList,
            isStub = false
        )
    }

}


fun TehsectPlan.toItem(ymd: String) = TehsectPlanItem(
    id, ymd, rowN,
    aomnId, aomnName, tehsectName,
    prodType,
    beginDateStr, endDateStr,
    regimeQm3, regimeQt, regime,
    npsList,
    note
)