package com.novinteh.mdl.regimes.regChanges.persistence.repo

import androidx.lifecycle.LiveData
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.novinteh.mdl.data.repo.AsyncRepository
import com.novinteh.mdl.regimes.regChanges.model.DirectionItem
import com.novinteh.mdl.entity.data.structure.Direction
import com.novinteh.mdl.regimes.regChanges.persistence.dao.DirectionDao
import com.novinteh.mdl.rest.RequestService
import com.novinteh.mdl.data.Result
import com.novinteh.mdl.regimes.common.persistence.dao.StateDao
import com.novinteh.mdl.regimes.regChanges.model.toItem


class DirectionRepo(stateDao: StateDao, private val dao: DirectionDao): AsyncRepository(stateDao) {

    override val LOG = false
    override val LOG_TAG = "DirectionRepo"

    override val stateId = "state.directions"

    private val gson = GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()


    fun items(date: String): LiveData<List<DirectionItem>> {
        return dao.items(date)
    }

    override suspend fun download(id: String): Result {
        return RequestService.getDirections(id)
    }

    override suspend fun upsert(id: String, data: Result.Success.Data) {
        dao.insert(toList(id, data))
    }

    override suspend fun contains(id: String): Boolean {
        return dao.count(id) > 0
    }

    private fun toList(date: String, data: Result.Success.Data): List<DirectionItem> = data.responseBody.charStream().use { reader -> gson
        .fromJson<List<Direction>>(reader, object : TypeToken<List<Direction>>(){}.type)
        ?.map { it.toItem(date) }
        ?: emptyList()
    }

}