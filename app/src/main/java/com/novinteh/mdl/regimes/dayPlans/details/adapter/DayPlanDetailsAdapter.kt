package com.novinteh.mdl.regimes.dayPlans.details.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.novinteh.mdl.R
import com.novinteh.mdl.regimes.dayPlans.model.DayPlanItem
import com.novinteh.mdl.view.recycler.adapter.ItemAdapter
import com.novinteh.mdl.view.recycler.viewholder.BindViewHolder
import kotlinx.android.synthetic.main.item_day_plan_details.view.*


class DayPlanDetailsAdapter : ItemAdapter<DayPlanItem, DayPlanDetailsAdapter.DetailsViewHolder>(
    DayPlanItem.DiffCallback
) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DetailsViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_day_plan_details, parent, false)

        return DetailsViewHolder(view)
    }


    class DetailsViewHolder(view: View) : BindViewHolder<DayPlanItem>(view) {

        private val aomnName = itemView.aomn_name
        private val tehsectName = itemView.tehsect_name
        private val recyclerPlans = itemView.recycler_plans

        private val adapter = TehsectPlanAdapter()


        init {
            recyclerPlans.layoutManager = LinearLayoutManager(itemView.context, LinearLayoutManager.VERTICAL, false)
            recyclerPlans.adapter = adapter
            recyclerPlans.itemAnimator = null
        }

        override fun content(item: DayPlanItem) {
            aomnName.text = item.aomnName
            tehsectName.text = item.tehsectName
            adapter.submitList(item.tehsectPlans)
        }

    }


}