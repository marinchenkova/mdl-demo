package com.novinteh.mdl.regimes.dayPlans.model

import androidx.recyclerview.widget.DiffUtil
import com.novinteh.mdl.regimes.dayPlans.model.mapping.TehsectPlanListStub

data class DayPlanListItem(
    val tehsectName: String,
    val tehsectPlans: List<TehsectPlanListStub>
) {

    companion object {

        val DiffCallback = object : DiffUtil.ItemCallback<DayPlanListItem>() {
            override fun areItemsTheSame(
                oldItem: DayPlanListItem,
                newItem: DayPlanListItem
            ): Boolean {
                return oldItem.tehsectName == newItem.tehsectName
            }

            override fun areContentsTheSame(
                oldItem: DayPlanListItem,
                newItem: DayPlanListItem
            ): Boolean {
                return oldItem == newItem
            }
        }

    }

}