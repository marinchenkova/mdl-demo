package com.novinteh.mdl.regimes.regChanges.model

import androidx.recyclerview.widget.DiffUtil
import com.novinteh.mdl.entity.data.regchanges.RegChangeState
import com.novinteh.mdl.paged.model.Page
import com.novinteh.mdl.paged.model.Tab

data class RegChangeTab(override val id: String, val hasProblems: Boolean = true) : Tab(id) {

    companion object {

        val DiffCallback = object : DiffUtil.ItemCallback<RegChangeTab>() {
            override fun areItemsTheSame(oldItem: RegChangeTab, newItem: RegChangeTab): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: RegChangeTab, newItem: RegChangeTab): Boolean {
                return oldItem == newItem
            }
        }


        fun from(regChangePage: Page<RegChangeListItem>) = RegChangeTab(
            regChangePage.id,
            regChangePage.list.any { item ->
                item.state == RegChangeState.NOT_SCHEDULED || item.state == RegChangeState.FAILED
            }
        )

    }

}