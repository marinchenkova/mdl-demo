package com.novinteh.mdl.regimes.regChanges.model

data class UserShiftWithAomns(val dirId: String, val aomnIds: List<Int>)