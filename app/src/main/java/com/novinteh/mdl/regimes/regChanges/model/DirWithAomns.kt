package com.novinteh.mdl.regimes.regChanges.model

import androidx.recyclerview.widget.DiffUtil
import com.novinteh.mdl.regimes.common.model.AomnItem

data class DirWithAomns(
    val id: String,
    val name: String,
    val aomns: List<AomnItem>
) {

    companion object {

        val DiffCallback = object : DiffUtil.ItemCallback<DirWithAomns>() {
            override fun areItemsTheSame(oldItem: DirWithAomns, newItem: DirWithAomns): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: DirWithAomns, newItem: DirWithAomns): Boolean {
                return oldItem == newItem
            }
        }

    }

}