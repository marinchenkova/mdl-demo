package com.novinteh.mdl.regimes.dayPlans.filter


import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.novinteh.mdl.R
import com.novinteh.mdl.entity.data.common.ProdType
import com.novinteh.mdl.paged.activity.FilterActivity
import com.novinteh.mdl.paged.filter.setFilter
import com.novinteh.mdl.regimes.dayPlans.filter.adapter.AomnAdapter
import com.novinteh.mdl.regimes.dayPlans.model.DayPlanFilter
import com.novinteh.mdl.regimes.regChanges.filter.adapter.ProdTypeAdapter
import com.novinteh.mdl.util.setAomnId
import com.novinteh.mdl.util.setProdType
import kotlinx.android.synthetic.main.activity_filter.*
import org.jetbrains.anko.textResource


class DayPlansFilterActivity : FilterActivity<DayPlanFilter, DayPlanFilterViewModel>() {

    override val LOG = true
    override val LOG_TAG = "DayPlansFilterActivity"

    override var filter = DayPlanFilter()

    private val prodTypeAdapter = ProdTypeAdapter()
    private val aomnAdapter = AomnAdapter()

    override val viewModelClass = DayPlanFilterViewModel::class.java


    override fun onDateSet(date: String) {
        initProdTypes()
        initAomns()
    }

    override fun saveFilter(): DayPlanFilter? {
        val prodType = prodTypeAdapter.checkedItem
        val aomnId = aomnAdapter.checkedItem?.id

        if (prodType == null || aomnId == null) return null

        setProdType(prodType)
        setAomnId(aomnId)

        with(filter) {
            this.prodType = prodType
            this.aomnId = aomnId
        }

        return filter
    }

    override fun getFilterFromJson(json: String?): DayPlanFilter? {
        return Gson().fromJson(json, DayPlanFilter::class.java)
    }

    override fun onFilterChanged(value: DayPlanFilter) {
        prodTypeAdapter.setFilter(value.prodType)
        aomnAdapter.setFilter(value.aomnId)
    }

    private fun initAomns() {
        viewModel.aomns.observe(this, Observer { list ->
            if (!list.isNullOrEmpty()) aomnAdapter.submitList(list)
        })

        direction_header.textResource = R.string.shift_filter_aomn_header
        recycler_directions.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recycler_directions.itemAnimator = null
        recycler_directions.adapter = aomnAdapter
    }

    private fun initProdTypes() {
        prodTypeAdapter.submitList(listOf(ProdType.OIL, ProdType.NP))

        recycler_prod_types.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recycler_prod_types.itemAnimator = null
        recycler_prod_types.adapter = prodTypeAdapter
    }

    override fun isContentLoaded(): Boolean {
        return aomnAdapter.itemCount > 0
    }

}
