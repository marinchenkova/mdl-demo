package com.novinteh.mdl.regimes.common.persistence.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.novinteh.mdl.regimes.common.model.LoadStateInfo


@Dao
interface StateDao {

    @Query("SELECT * from load_states WHERE stateId = :stateId LIMIT 1")
    fun getState(stateId: String): LiveData<LoadStateInfo>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(loadStateInfo: LoadStateInfo)

    @Query("DELETE FROM load_states")
    suspend fun deleteAll()

}