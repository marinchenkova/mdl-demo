package com.novinteh.mdl.regimes.shipments.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.novinteh.mdl.R
import com.novinteh.mdl.paged.model.Tab
import com.novinteh.mdl.paged.viewpager.adapter.TabAdapter
import com.novinteh.mdl.paged.viewpager.viewholder.TabViewHolder


class ShipmentsTabAdapter : TabAdapter<Tab, TabViewHolder<Tab>>(Tab.DiffCallback) {

    override val LOG = false
    override val LOG_TAG = "ShipmentsTabAdapter"


    override fun createViewHolder(view: View): TabViewHolder<Tab> {
        return TabViewHolder(view)
    }

}