package com.novinteh.mdl.regimes.dayPlans.persistence.converters

import androidx.room.TypeConverter
import com.google.gson.reflect.TypeToken
import com.novinteh.mdl.entity.JsonParser
import com.novinteh.mdl.entity.data.tehsectPlans.Nps
import com.novinteh.mdl.entity.util.DateUtils

class DayPlanConverters {

    private val parser = JsonParser()


    @TypeConverter
    fun npsListToString(npsList: List<Nps>): String = parser.toJson(npsList)

    @TypeConverter
    fun npsListFromString(str: String): List<Nps> = parser
        .listFromJson(str, object : TypeToken<List<Nps>>(){})

}