package com.novinteh.mdl.regimes.common.converters

import androidx.room.TypeConverter
import com.novinteh.mdl.data.Result
import com.novinteh.mdl.entity.JsonParser
import com.novinteh.mdl.entity.data.common.ProdType
import com.novinteh.mdl.entity.data.common.Regime
import com.novinteh.mdl.util.Timestamp

class CommonConverters {

    private val parser = JsonParser()


    @TypeConverter
    fun resultToString(result: Result): String = when (result) {
        is Result.Success -> "result.success"
        is Result.Loading -> "result.loading"
        is Result.Error.Login -> "result.error.login.${result.msg.getOrSetEmptyWord()}"
        is Result.Error.Server -> "result.error.server.${result.msg.getOrSetEmptyWord()}"
        is Result.Error.Client -> "result.error.client.${result.msg.getOrSetEmptyWord()}"
    }

    private fun String.getOrSetEmptyWord() = if (isEmpty()) "empty" else this

    private fun String.getMsgOrEmpty() = if (this == "empty") "" else this

    @TypeConverter
    fun resultFromString(str: String): Result = when (str) {
        "result.success" -> Result.Success.Empty
        "result.loading" -> Result.Loading
        else -> {
            val msg = str.split(".").last().getMsgOrEmpty()
            when {
                str.startsWith("result.error.login.") -> Result.Error.Login(msg)
                str.startsWith("result.error.server.") -> Result.Error.Server(msg)
                else -> Result.Error.Client(msg)
            }
        }
    }

    @TypeConverter
    fun timestampToString(timestamp: Timestamp): String = timestamp.ymdHms

    @TypeConverter
    fun timestampFromString(str: String): Timestamp =
        Timestamp(str)

    @TypeConverter
    fun prodTypeToString(prodType: ProdType): String = prodType.name

    @TypeConverter
    fun prodTypeFromString(str: String): ProdType = ProdType.fromString(str)

    @TypeConverter
    fun regimeToString(regime: Regime): String = parser.toJson(regime)

    @TypeConverter
    fun regimeFromString(str: String): Regime = parser
        .fromJson(str, Regime::class.java)
        ?: Regime(null)

}