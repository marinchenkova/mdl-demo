package com.novinteh.mdl.regimes.regChanges.persistence.repo

import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.novinteh.mdl.entity.data.structure.UserShift
import com.novinteh.mdl.regimes.regChanges.model.UserShiftItem
import com.novinteh.mdl.regimes.regChanges.model.toItem
import com.novinteh.mdl.regimes.regChanges.persistence.dao.UserShiftDao
import com.novinteh.mdl.rest.RequestService
import com.novinteh.mdl.data.Result
import com.novinteh.mdl.data.repo.Repository
import com.novinteh.mdl.regimes.common.persistence.dao.StateDao

class UserShiftRepo(stateDao: StateDao, private val dao: UserShiftDao) : Repository(stateDao) {

    override val LOG = false
    override val LOG_TAG = "UserShiftRepo"

    override val stateId = "state.user_shifts"

    private val gson = GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()


    fun items(date: String) = dao.items(date)

    override suspend fun download(id: String): Result {
        return RequestService.getUserShifts(id)
    }

    override suspend fun contains(id: String): Boolean {
        return dao.count(id) > 0
    }

    override suspend fun upsert(id: String, data: Result.Success.Data) {
        dao.insert(toList(id, data))
    }

    private fun toList(date: String, data: Result.Success.Data): List<UserShiftItem> = data.responseBody.charStream().use { reader -> gson
        .fromJson<List<UserShift>>(reader, object : TypeToken<List<UserShift>>() {}.type)
        ?.map { userShift -> userShift.toItem(date) }
        ?: emptyList()
    }

}