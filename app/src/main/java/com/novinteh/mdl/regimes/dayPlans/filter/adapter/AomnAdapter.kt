package com.novinteh.mdl.regimes.dayPlans.filter.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Checkable
import androidx.recyclerview.widget.RecyclerView
import com.novinteh.mdl.R
import com.novinteh.mdl.entity.data.common.ProdType
import com.novinteh.mdl.paged.filter.Filterable
import com.novinteh.mdl.entity.data.structure.Aomn
import com.novinteh.mdl.regimes.common.model.AomnItem
import com.novinteh.mdl.view.recycler.adapter.RadioAdapter
import com.novinteh.mdl.view.recycler.util.OnCheckListener
import com.novinteh.mdl.view.recycler.viewholder.CheckViewHolder
import com.novinteh.mdl.view.recycler.viewholder.ClickViewHolder
import kotlinx.android.synthetic.main.item_radio_aomn.view.*


class AomnAdapter: RadioAdapter<AomnItem, AomnAdapter.ViewHolder>(AomnItem.DiffCallback),
    Filterable<Int> {

    override var filter = -1


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_radio_aomn, parent, false)

        return ViewHolder(view).also { holder ->
            holder.init(clickListener)
        }
    }

    override fun onFilterChanged(value: Int) {
        checkFilterPosition()
    }

    override fun submitList(list: List<AomnItem>?) {
        super.submitList(list)
        checkFilterPosition()
    }

    private fun checkFilterPosition() {
        val aomn = currentList.find { it.id == filter } ?: return
        val position = positionOf(aomn)
        if (position != RecyclerView.NO_POSITION) switch(position)
    }


    class ViewHolder(view: View) : CheckViewHolder<AomnItem>(view) {

        private val button= itemView.button
        private val title = itemView.title


        override fun content(item: AomnItem) {
            title.text = item.name
        }

        override fun check() {
            button.isChecked = true
        }

        override fun uncheck() {
            button.isChecked = false
        }

    }

}