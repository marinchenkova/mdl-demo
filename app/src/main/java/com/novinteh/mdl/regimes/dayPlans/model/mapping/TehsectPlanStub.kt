package com.novinteh.mdl.regimes.dayPlans.model.mapping

import androidx.recyclerview.widget.DiffUtil
import com.novinteh.mdl.entity.data.common.Regime
import com.novinteh.mdl.entity.data.tehsectPlans.Nps
import com.novinteh.mdl.util.Hm
import kotlin.math.abs

data class TehsectPlanStub(
    override val beginHm: Hm,
    override val endHm: Hm,
    override val regimeQt: Int,
    override val regime: Regime,
    val note: String?,
    val npsList: List<Nps>,
    override val isStub: Boolean
) : ITehsectPlan<TehsectPlanStub> {

    override fun clone(
        beginHm: Hm,
        endHm: Hm,
        regimeQt: Int,
        regime: Regime,
        isStub: Boolean
    ): TehsectPlanStub {
        return TehsectPlanStub(beginHm, endHm, regimeQt, regime, null, emptyList(), isStub)
    }


    companion object {

        val DiffCallback = object : DiffUtil.ItemCallback<TehsectPlanStub>() {
            override fun areItemsTheSame(
                oldItem: TehsectPlanStub,
                newItem: TehsectPlanStub
            ): Boolean {
                return oldItem.beginHm == newItem.beginHm && oldItem.endHm == newItem.endHm
            }

            override fun areContentsTheSame(
                oldItem: TehsectPlanStub,
                newItem: TehsectPlanStub
            ): Boolean {
                return oldItem == newItem
            }

        }


        val Empty = TehsectPlanStub(
            Hm.Hm0000,
            Hm.Hm2400,
            regimeQt = 0,
            regime = Regime(),
            note = null,
            npsList = emptyList(),
            isStub = true
        )

    }

}