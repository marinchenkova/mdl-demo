package com.novinteh.mdl.regimes.regChanges.adapter

import android.view.View
import com.novinteh.mdl.R
import com.novinteh.mdl.paged.viewpager.adapter.TabAdapter
import com.novinteh.mdl.paged.viewpager.viewholder.TabViewHolder
import com.novinteh.mdl.regimes.regChanges.model.RegChangeTab
import org.jetbrains.anko.textColorResource


class RegChangesTabAdapter :
    TabAdapter<RegChangeTab, RegChangesTabAdapter.ViewHolder>(RegChangeTab.DiffCallback) {

    override val LOG = false
    override val LOG_TAG = "RegChangesTabAdapter"


    override fun createViewHolder(view: View): ViewHolder {
        return ViewHolder(view)
    }


    class ViewHolder(view: View) : TabViewHolder<RegChangeTab>(view) {

        private var hasProblems = true
        private var isSelected = false


        override fun content(item: RegChangeTab) {
            super.content(item)
            hasProblems = item.hasProblems
            defineTextColor()
        }

        private fun defineTextColor() {
            date.textColorResource = when {
                isSelected && hasProblems -> R.color.red95
                isSelected && !hasProblems -> R.color.white95
                !isSelected && hasProblems -> R.color.red60
                else -> R.color.white60
            }
        }

        override fun highlight() {
            isSelected = true
            defineTextColor()
        }

        override fun unhighlight() {
            isSelected = false
            defineTextColor()
        }

    }

}