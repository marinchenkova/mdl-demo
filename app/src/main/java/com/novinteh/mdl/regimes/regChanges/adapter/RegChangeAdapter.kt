package com.novinteh.mdl.regimes.regChanges.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.novinteh.mdl.R
import com.novinteh.mdl.regimes.regChanges.model.RegChangeListItem
import com.novinteh.mdl.util.imageRes
import com.novinteh.mdl.util.qKindImageRes
import com.novinteh.mdl.util.sideBarRes
import com.novinteh.mdl.util.stringRes
import com.novinteh.mdl.view.recycler.adapter.ClickAdapter
import com.novinteh.mdl.view.recycler.viewholder.ClickViewHolder
import com.novinteh.mdl.view.regimeArray
import com.novinteh.mdl.view.show
import kotlinx.android.synthetic.main.item_reg_change.view.*
import org.jetbrains.anko.backgroundResource
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.textResource


class RegChangeAdapter : ClickAdapter<RegChangeListItem, RegChangeAdapter.RegChangeViewHolder>(
    RegChangeListItem.DiffCallback
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RegChangeViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_reg_change, parent, false)

        return RegChangeViewHolder(view).also { holder ->
            holder.init(clickListener)
        }
    }

    override fun getItemId(position: Int): Long {
        return getItem(position)?.rowN?.toLong() ?: 0
    }


    class RegChangeViewHolder(view: View) : ClickViewHolder<RegChangeListItem>(view) {

        private val tehsect = itemView.tehsect
        private val regimeBefore = itemView.regime_before
        private val regimeAfter = itemView.regime_after
        private val plan = itemView.plan
        private val ctrl = itemView.ctrl
        private val planDay = itemView.plan_next_day
        private val ctrlDay = itemView.ctrl_next_day
        private val qKind = itemView.q_kind_image
        private val sideBar = itemView.side_bar
        private val stateImage = itemView.state_image
        private val stateText = itemView.state_text


        override fun content(item: RegChangeListItem) {
            tehsect.text = item.tehsectName

            item.regimeArray(4, ContextCompat.getColor(itemView.context, R.color.colorPrimary))
                .also { arr ->
                    regimeBefore.text = arr[0]
                    regimeAfter.text = arr[1]
                }

            plan.text = item.planDate
            ctrl.text = item.factDate

            planDay.show(item.isPlanNextDay)
            ctrlDay.show(item.isFactNextDay)

            qKind.imageResource = item.qKindImageRes()

            sideBar.backgroundResource = item.state.sideBarRes()

            stateImage.imageResource = item.state.imageRes()
            stateText.textResource = item.state.stringRes()
        }

    }

}
