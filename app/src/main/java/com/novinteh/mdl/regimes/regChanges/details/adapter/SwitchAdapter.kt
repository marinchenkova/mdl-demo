package com.novinteh.mdl.regimes.regChanges.details.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import com.novinteh.mdl.R
import com.novinteh.mdl.entity.data.common.Regime
import com.novinteh.mdl.entity.data.regchanges.Switch
import com.novinteh.mdl.regimes.regChanges.model.DirWithAomns
import com.novinteh.mdl.util.drawableRes
import com.novinteh.mdl.util.stringRes
import com.novinteh.mdl.view.appear
import com.novinteh.mdl.view.recycler.adapter.ItemAdapter
import com.novinteh.mdl.view.recycler.viewholder.BindViewHolder
import com.novinteh.mdl.view.span
import kotlinx.android.synthetic.main.item_switch.view.*
import org.jetbrains.anko.textResource


class SwitchAdapter : ItemAdapter<Switch, SwitchAdapter.SwitchViewHolder>(DiffCallback) {

    private var fromReg = Regime()


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SwitchViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_switch, parent, false)

        return SwitchViewHolder(
            view
        )
    }

    override fun onBindViewHolder(holder: SwitchViewHolder, position: Int) {
        holder.fromReg = getItem(position - 1)?.regAfterSwitch ?: fromReg
        super.onBindViewHolder(holder, position)
    }

    fun submitList(new: List<Switch>, fromReg: Regime) {
        this.fromReg = fromReg
        submitList(new)
    }


    class SwitchViewHolder(view: View) : BindViewHolder<Switch>(view) {

        private val time = itemView.data
        private val name = itemView.name
        private val numShort = itemView.num_short
        private val numLong = itemView.num_long
        private val regAfterShort = itemView.reg_after_short
        private val regAfterLong = itemView.reg_after_long
        private val type = itemView.type

        var fromReg = Regime("")


        override fun content(item: Switch) {
            time.text = item.dateStr
            name.text = item.npsName

            val numText = itemView.context.getString(item.agrType.stringRes(), item.agrNum)
            numShort.text = numText
            numLong.text = numText

            val regAfterText = item.regAfterSwitch.span(
                fromReg,
                ContextCompat.getColor(itemView.context, R.color.colorPrimary)
            )
            regAfterShort.text = regAfterText
            regAfterLong.text = regAfterText

            val regLong = item.regAfterSwitch.name.length > 6
            numShort.appear(!regLong)
            numLong.appear(regLong)
            regAfterShort.appear(!regLong)
            regAfterLong.appear(regLong)

            type.setCompoundDrawablesWithIntrinsicBounds(
                null,
                null,
                itemView.context.getDrawable(item.type.drawableRes()),
                null
            )
            type.textResource = item.type.stringRes()
        }

    }


    companion object {

        private val DiffCallback = object : DiffUtil.ItemCallback<Switch>() {
            override fun areItemsTheSame(oldItem: Switch, newItem: Switch): Boolean {
                return oldItem.npsId == newItem.npsId
            }

            override fun areContentsTheSame(oldItem: Switch, newItem: Switch): Boolean {
                return oldItem == newItem
            }
        }

    }

}