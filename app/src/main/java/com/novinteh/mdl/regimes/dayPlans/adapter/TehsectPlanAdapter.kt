package com.novinteh.mdl.regimes.dayPlans.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.novinteh.mdl.R
import com.novinteh.mdl.entity.data.common.Regime
import com.novinteh.mdl.regimes.dayPlans.model.mapping.TehsectPlanListStub
import com.novinteh.mdl.view.dpToPixels
import com.novinteh.mdl.view.recycler.adapter.ItemAdapter
import com.novinteh.mdl.view.recycler.viewholder.BindViewHolder
import com.novinteh.mdl.view.span
import kotlinx.android.synthetic.main.item_tehsect_plan.view.*
import org.jetbrains.anko.backgroundColorResource
import org.jetbrains.anko.displayMetrics


private const val MARGIN = 8


class TehsectPlanAdapter : ItemAdapter<TehsectPlanListStub, TehsectPlanAdapter.ViewHolder>(
    TehsectPlanListStub.DiffCallback
) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_tehsect_plan, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position - 1)?.also { prev -> holder.prevRegime = prev.regime }
        super.onBindViewHolder(holder, position)
    }


    class ViewHolder(view: View): BindViewHolder<TehsectPlanListStub>(view) {

        private val viewWidth = initViewWidth()
        private val root = itemView.root
        private val regime = itemView.regime
        private val q = itemView.q
        private val time = itemView.time

        var prevRegime = Regime("")


        override fun content(item: TehsectPlanListStub) {
            initItemWidth(item)

            root.backgroundColorResource = when {
                item.isStub -> R.color.grey_light
                adapterPosition % 2 == 0 -> R.color.yellow_light
                else -> R.color.yellow_dark
            }

            regime.text = if (adapterPosition == 0 || prevRegime.isEmpty()) item.regime.name
            else item.regime.span(prevRegime, ContextCompat.getColor(itemView.context, R.color.colorPrimary))

            q.text = itemView.context.getString(R.string.activity_day_plan_q, item.regimeQt)
            time.text = itemView.context.getString(R.string.str_dash_str, item.beginHm.hStr, item.endHm.hStr)
        }

        private fun initItemWidth(item: TehsectPlanListStub) {
            val percent = item.endHm.percentOfDay() - item.beginHm.percentOfDay()
            itemView.layoutParams.width = (viewWidth * percent).toInt()
        }

        private fun initViewWidth() =
            itemView.context.displayMetrics.widthPixels - itemView.context.dpToPixels(MARGIN) * 2

    }

}