package com.novinteh.mdl.regimes.shipments.model

import androidx.recyclerview.widget.DiffUtil
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.novinteh.mdl.entity.data.common.ProdType
import com.novinteh.mdl.entity.data.structure.Aomn
import com.novinteh.mdl.entity.data.structure.Port


@Entity(tableName = "ports")
data class PortItem(
    @PrimaryKey val id: Int,
    val name: String?
) {

    companion object {

        val DiffCallback = object : DiffUtil.ItemCallback<PortItem>() {
            override fun areItemsTheSame(oldItem: PortItem, newItem: PortItem): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: PortItem, newItem: PortItem): Boolean {
                return oldItem == newItem
            }
        }

    }

}

fun Port.toItem() = PortItem(id, name)