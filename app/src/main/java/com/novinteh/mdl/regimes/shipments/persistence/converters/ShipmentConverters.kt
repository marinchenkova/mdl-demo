package com.novinteh.mdl.regimes.shipments.persistence.converters

import androidx.room.TypeConverter
import com.google.gson.reflect.TypeToken
import com.novinteh.mdl.entity.JsonParser
import com.novinteh.mdl.entity.data.regchanges.QKind
import com.novinteh.mdl.entity.data.shipments.Operation
import com.novinteh.mdl.entity.data.shipments.ShipmentState

class ShipmentConverters {

    private val parser = JsonParser()


    @TypeConverter
    fun timesheetToString(timesheet: List<Operation>): String = parser.toJson(timesheet)

    @TypeConverter
    fun timesheetFromString(str: String): List<Operation> = parser
        .listFromJson(str, object : TypeToken<List<Operation>>(){})

    @TypeConverter
    fun shipmentStateToString(state: ShipmentState): String = state.name

    @TypeConverter
    fun shipmentStateFromString(str: String): ShipmentState = ShipmentState.fromString(str)

}