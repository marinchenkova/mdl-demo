package com.novinteh.mdl.regimes.regChanges.persistence.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.novinteh.mdl.regimes.regChanges.model.DirectionItem
import com.novinteh.mdl.regimes.regChanges.model.UserShiftItem


@Dao
interface UserShiftDao {

    @Query("SELECT * from user_shifts WHERE date = :date")
    fun items(date: String): LiveData<List<UserShiftItem>>

    @Query("SELECT count(id) from user_shifts WHERE date = :date")
    fun count(date: String): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(list: List<UserShiftItem>)

    @Query("DELETE FROM user_shifts")
    suspend fun deleteAll()

}