package com.novinteh.mdl.regimes.dayPlans.model

import androidx.recyclerview.widget.DiffUtil
import com.novinteh.mdl.regimes.dayPlans.model.mapping.TehsectPlanStub

data class DayPlanItem(
    val aomnName: String,
    val tehsectName: String,
    val tehsectPlans: List<TehsectPlanStub>
) {

    companion object {

        val DiffCallback = object : DiffUtil.ItemCallback<DayPlanItem>() {
            override fun areItemsTheSame(oldItem: DayPlanItem, newItem: DayPlanItem): Boolean {
                return oldItem.tehsectName == newItem.tehsectName
            }

            override fun areContentsTheSame(oldItem: DayPlanItem, newItem: DayPlanItem): Boolean {
                return oldItem == newItem
            }
        }

    }

}