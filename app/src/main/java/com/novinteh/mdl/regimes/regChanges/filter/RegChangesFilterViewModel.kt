package com.novinteh.mdl.regimes.regChanges.filter

import android.app.Application
import androidx.lifecycle.*
import com.novinteh.mdl.data.MdlDatabase
import com.novinteh.mdl.paged.viewmodel.FilterViewModel
import com.novinteh.mdl.regimes.common.model.LoadStateInfo
import com.novinteh.mdl.util.Timestamp
import com.novinteh.mdl.regimes.common.persistence.repo.AomnRepo
import com.novinteh.mdl.regimes.regChanges.model.DirWithAomns
import com.novinteh.mdl.regimes.regChanges.model.UserShiftItem
import com.novinteh.mdl.regimes.regChanges.persistence.repo.DirectionRepo
import com.novinteh.mdl.regimes.regChanges.persistence.repo.UserShiftRepo
import kotlinx.coroutines.launch


class RegChangesFilterViewModel(app: Application) : FilterViewModel(app) {

    override val LOG = true
    override val LOG_TAG = "RegChangesFilterViewModel"

    val currentDate = MutableLiveData<String>()
    val today = MutableLiveData<String>()

    val dirs: LiveData<List<DirWithAomns>>
    val userShifts: LiveData<List<UserShiftItem>>
    override val state: LiveData<LoadStateInfo>

    private val directionRepo: DirectionRepo
    private val userShiftRepo: UserShiftRepo
    private val aomnRepo: AomnRepo


    init {
        val db = MdlDatabase.getInstance(app)
        val stateDao = db.stateDao()

        directionRepo = DirectionRepo(stateDao, db.directionDao())
        userShiftRepo = UserShiftRepo(stateDao, db.userShiftDao())
        aomnRepo = AomnRepo(stateDao, db.aomnDao())

        launch { aomnRepo.loadIfAbsent() }

        state = directionRepo.state

        dirs = Transformations
            .switchMap(currentDate) { date ->
                launch { directionRepo.loadIfAbsent(date, Timestamp.now()) }
                directionRepo.items(date)
            }
            .combineWith(aomnRepo.items()) { dirs, aomns ->
                dirs.map { dir -> DirWithAomns(
                    dir.id,
                    dir.name,
                    aomns.filter { aomn -> aomn.id in dir.aomnIds }
                )}
            }

        userShifts = Transformations.switchMap(today) { today ->
            launch { userShiftRepo.loadIfAbsent(today, Timestamp.now()) }
            userShiftRepo.items(today)
        }
    }

    override fun refresh() {
        launch { aomnRepo.loadIfAbsent() }

        currentDate.value?.also { date ->
            launch { directionRepo.loadIfAbsent(date, Timestamp.now()) }
        }

        today.value?.also { today ->
            launch { userShiftRepo.loadIfAbsent(today, Timestamp.now()) }
        }
    }

}