package com.novinteh.mdl.regimes.dayPlans.adapter

import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintSet
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.novinteh.mdl.R
import com.novinteh.mdl.entity.util.DateUtils
import com.novinteh.mdl.regimes.dayPlans.model.DayPlanListItem
import com.novinteh.mdl.regimes.dayPlans.model.TehsectPlanListItem
import com.novinteh.mdl.view.recycler.adapter.ClickAdapter
import com.novinteh.mdl.view.recycler.viewholder.ClickViewHolder
import kotlinx.android.synthetic.main.item_day_plan.view.*
import kotlinx.android.synthetic.main.item_tehsect_plan.view.*
import org.jetbrains.anko.displayMetrics
import java.util.*
import kotlin.math.roundToInt


class DayPlanAdapter : ClickAdapter<DayPlanListItem, DayPlanAdapter.ViewHolder>(DayPlanListItem.DiffCallback) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_day_plan, parent, false)

        return ViewHolder(view).also { holder ->
            holder.init(clickListener)
        }
    }

    override fun getItemId(position: Int): Long {
        return getItem(position)?.tehsectName?.hashCode()?.toLong() ?: 0
    }


    class ViewHolder(view: View) : ClickViewHolder<DayPlanListItem>(view) {

        private val tehsectHeader = itemView.tehsect_header
        private val recycler = itemView.recycler_plan_items
        private val adapter = TehsectPlanAdapter()


        init {
            recycler.layoutManager = LinearLayoutManager(itemView.context, LinearLayoutManager.HORIZONTAL, false)
            recycler.itemAnimator = null
            recycler.setHasFixedSize(true)
            recycler.adapter = adapter
        }

        override fun content(item: DayPlanListItem) {
            tehsectHeader.text = item.tehsectName
            adapter.submitList(item.tehsectPlans)
        }

    }

}

