package com.novinteh.mdl.regimes.common.model

sealed class FilterTitle {

    data class Text(val text: String) : FilterTitle()

    object All : FilterTitle()

    object NotSet : FilterTitle()

}