package com.novinteh.mdl.regimes.regChanges.filter.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.novinteh.mdl.R
import com.novinteh.mdl.regimes.common.model.AomnItem
import com.novinteh.mdl.view.recycler.adapter.CheckBoxAdapter
import com.novinteh.mdl.view.recycler.viewholder.CheckViewHolder
import com.novinteh.mdl.view.recycler.viewholder.ClickViewHolder
import kotlinx.android.synthetic.main.item_checkbox_aomn.view.*


class AomnAdapter: CheckBoxAdapter<AomnItem, AomnAdapter.ViewHolder>(AomnItem.DiffCallback) {

    private var isFrozen = false


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_checkbox_aomn, parent, false)

        return ViewHolder(view).also { holder ->
            holder.init(clickListener)
        }
    }

    fun notifyChecks() {
        checkListener.get()?.onChecksUpdated(checkedItems)
    }

    fun checkIds(ids: List<Int>) {
        if (currentList.map { it.id }.toSet() == ids.toSet()) return

        isFrozen = false
        uncheckAll()
        currentList.forEachIndexed { position, aomn ->
            if (aomn.id in ids) check(position)
        }
    }

    fun freezeAll() {
        isFrozen = true
        checkAll()
    }

    override fun switch(position: Int) {
        if (isFrozen) {
            isFrozen = false
            uncheckAll()
            check(position)
        }
        else super.switch(position)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.checkFreeze()
    }

    private fun ViewHolder.checkFreeze() {
        if (isFrozen) freeze()
    }

    class ViewHolder(view: View) : CheckViewHolder<AomnItem>(view) {

        private val button = itemView.button
        private val title = itemView.title


        override fun content(item: AomnItem) {
            title.text = item.name
        }

        override fun check() {
            button.isChecked = true
            alpha(1f)
        }

        override fun uncheck() {
            button.isChecked = false
            alpha(1f)
        }

        fun freeze() {
            button.isChecked = true
            alpha(0.5f)
        }

        private fun alpha(value: Float) {
            button.alpha = value
            title.alpha = value
        }

    }

}