package com.novinteh.mdl.regimes.regChanges.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.novinteh.mdl.entity.data.structure.UserShift


@Entity(tableName = "user_shifts")
data class UserShiftItem(
    val date: String,
    val fio: String,
    val isRegular: Boolean,
    val isGeneral: Boolean,
    val directionId: String
) {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

}

fun UserShift.toItem(date: String) = UserShiftItem(
    date,
    id,
    isRegular,
    isGeneral,
    directionId
)