package com.novinteh.mdl.regimes.regChanges.model

import com.novinteh.mdl.entity.data.common.ProdType
import com.novinteh.mdl.util.replaceAll


class RegChangeFilter(var prodType: ProdType = ProdType.OIL, dirId: String = DIR_ALL) {

    var userShiftPriority = true
        private set

    private val checkedAomnIds = mutableListOf<Int>()
    val aomnIds: List<Int> get() = checkedAomnIds

    val dirIsAll get() = dirId == DIR_ALL
    var dirId: String = dirId
        private set

    fun setUserDir(id: String, aomnIds: List<Int>) {
        if (id == dirId && aomnIds.toSet() == checkedAomnIds.toSet()) return

        userShiftPriority = false
        dirId = id
        setAomnIds(aomnIds)
    }

    private fun setAomnIds(new: Collection<Int>) {
        checkedAomnIds.replaceAll(new)
    }

    fun setUserShiftDirId(id: String) {
        if (userShiftPriority) dirId = id
    }

    fun setUserShift(shift: UserShiftWithAomns) {
        if (userShiftPriority) {
            dirId = shift.dirId
            setAomnIds(shift.aomnIds)
        }
    }

    override fun toString() = "RegChangeFilter($prodType, dir $dirId, usp $userShiftPriority, aomns $checkedAomnIds)"

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is RegChangeFilter) return false
        if (!super.equals(other)) return false

        if (prodType != other.prodType) return false
        if (dirId != other.dirId) return false
        if (checkedAomnIds != other.checkedAomnIds) return false
        if (userShiftPriority != other.userShiftPriority) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + prodType.hashCode()
        result = 31 * result + dirId.hashCode()
        result = 31 * result + checkedAomnIds.hashCode()
        result = 31 * result + userShiftPriority.hashCode()
        return result
    }


    companion object {

        const val DIR_ALL = "ALL"

    }

}