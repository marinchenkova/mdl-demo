package com.novinteh.mdl.regimes.shipments.details

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.novinteh.mdl.R
import com.novinteh.mdl.paged.activity.DetailsActivity
import com.novinteh.mdl.regimes.shipments.model.ShipmentItem


class ShipmentDetailsActivity : DetailsActivity<ShipmentItem>() {

    override val adapter = ShipmentDetailsAdapter()
    override val titleRes = R.string.activity_shipment_details_title


    override fun getDetailsFromJson(json: String?): List<ShipmentItem>? {
        return Gson().fromJson(json, object : TypeToken<List<ShipmentItem>>(){}.type)
    }

}