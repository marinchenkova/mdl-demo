package com.novinteh.mdl.regimes.regChanges.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.novinteh.mdl.entity.data.structure.Direction


@Entity(tableName = "directions", primaryKeys = ["id", "date"])
data class DirectionItem(
    val id: String,
    val date: String,
    val rowN: Int,
    val name: String,
    val aomnIds: List<Int>
)

fun Direction.toItem(date: String) = DirectionItem(
    id,
    date,
    rowN,
    name ?: "",
    aomnIds
)