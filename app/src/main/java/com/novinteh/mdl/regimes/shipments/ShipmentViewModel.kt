package com.novinteh.mdl.regimes.shipments

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.novinteh.mdl.data.MdlDatabase
import com.novinteh.mdl.paged.model.Page
import com.novinteh.mdl.paged.model.Tab
import com.novinteh.mdl.paged.viewmodel.PageViewModel
import com.novinteh.mdl.regimes.common.model.FilterTitle
import com.novinteh.mdl.regimes.common.model.LoadStateInfo
import com.novinteh.mdl.regimes.shipments.model.PortItem
import com.novinteh.mdl.regimes.shipments.model.ShipmentFilter
import com.novinteh.mdl.regimes.shipments.model.ShipmentItem
import com.novinteh.mdl.regimes.shipments.model.ShipmentListItem
import com.novinteh.mdl.regimes.shipments.persistence.repo.PortRepo
import com.novinteh.mdl.regimes.shipments.persistence.repo.ShipmentRepo
import com.novinteh.mdl.util.Timestamp
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class ShipmentViewModel(app: Application) : PageViewModel<ShipmentListItem, Tab, ShipmentFilter>(app) {

    override val LOG: Boolean get() = true
    override val LOG_TAG: String = "ShipmentViewModel"

    override val repo: ShipmentRepo
    private val portRepo: PortRepo

    override val pages: LiveData<List<Page<ShipmentListItem>>>
    override val tabs: LiveData<List<Tab>>
    override val state: LiveData<LoadStateInfo>

    val filterTitle: LiveData<FilterTitle>
    val ports: LiveData<List<PortItem>>


    init {
        val db = MdlDatabase.getInstance(app)
        val stateDao = db.stateDao()

        repo = ShipmentRepo(stateDao, db.shipmentDao())
        portRepo = PortRepo(stateDao, db.portDao())

        launch { portRepo.loadIfAbsent() }

        ports = portRepo.items()

        state = repo.state

        tabs = Transformations.map(search) { search ->
            search.currentDates.map { Tab(it) }
        }

        filterTitle = search.combineWith(ports) { search, ports ->
            buildSubtitle(search.filter, ports)
        }

        val listItems = Transformations.switchMap(search) { search ->
            repo.listItems(search)
        }

        pages = search.combineWith(listItems) { search, shipments ->
            search.currentDates.map { date ->
                Page(date, shipments.filter { it.date == date })
            }
        }
    }

    private fun buildSubtitle(filter: ShipmentFilter, ports: List<PortItem>): FilterTitle {
        if (filter.portIdAbsent || ports.isEmpty()) return FilterTitle.NotSet

        val port = ports.find { it.id == filter.portId }
        return FilterTitle.Text(port?.name ?: "")
    }

    fun getDetails(date: String, filter: ShipmentFilter, onReceive: (List<ShipmentItem>) -> Unit) {
        launch {
            val items = repo.details(date, filter.prodType, filter.portId)
            withContext(Dispatchers.Main) { onReceive(items) }
        }
    }

    override fun refresh(dates: List<String>, timestamp: Timestamp) {
        launch {
            repo.load(dates, timestamp)
            portRepo.loadIfAbsent()
        }
    }

    override fun loadIfAbsent(dates: List<String>, timestamp: Timestamp) {
        launch {
            repo.loadIfAbsent(dates, timestamp)
        }
    }

}