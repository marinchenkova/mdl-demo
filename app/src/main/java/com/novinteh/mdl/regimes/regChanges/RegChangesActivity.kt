package com.novinteh.mdl.regimes.regChanges

import android.view.Menu
import androidx.lifecycle.Observer
import com.google.gson.Gson
import com.novinteh.mdl.R
import com.novinteh.mdl.paged.activity.PageActivity
import com.novinteh.mdl.paged.filter.changeFilter
import com.novinteh.mdl.regimes.common.model.FilterTitle
import com.novinteh.mdl.regimes.regChanges.adapter.RegChangesPageAdapter
import com.novinteh.mdl.regimes.regChanges.adapter.RegChangesTabAdapter
import com.novinteh.mdl.regimes.regChanges.details.RegChangeDetailsActivity
import com.novinteh.mdl.regimes.regChanges.model.RegChangeFilter
import com.novinteh.mdl.regimes.regChanges.filter.RegChangesFilterActivity
import com.novinteh.mdl.regimes.regChanges.model.RegChangeListItem
import com.novinteh.mdl.regimes.regChanges.model.RegChangeTab
import com.novinteh.mdl.util.*
import org.jetbrains.anko.startActivity


class RegChangesActivity :
    PageActivity<RegChangeListItem, RegChangeTab, RegChangeFilter, RegChangesViewModel>() {

    override val LOG: Boolean get() = true
    override val LOG_TAG: String get() = "RegChangesActivity"

    override val titleRes = R.string.activity_reg_changes_title
    override val refreshRate = RefreshRate(hours = 0, minutes = 2, seconds = 0)

    override val pageAdapter = RegChangesPageAdapter()
    override var tabAdapter = RegChangesTabAdapter()

    override val viewModelClass = RegChangesViewModel::class.java
    override val filterActivityClass = RegChangesFilterActivity::class.java

    override var filter = RegChangeFilter()


    override fun buildEmptyTab(id: String): RegChangeTab {
        return RegChangeTab(id)
    }

    override fun onInitViewModel(date: String) {
        viewModel.currentDate.value = date
        viewModel.today.value = getToday()
    }

    override fun onInitFilter() {
        changeFilter { prodType = getProdType() }
        initShift()
    }

    override fun getFilterFromJson(json: String?): RegChangeFilter? {
        return Gson().fromJson(json, RegChangeFilter::class.java)
    }

    private fun initShift() {
        viewModel.userShifts.observe(this, Observer { list ->
            list?.firstOrNull()?.also { shift ->
                changeFilter { setUserShift(shift) }
            }
        })

        viewModel.filterTitle.observe(this, Observer { title ->
            when (title) {
                is FilterTitle.Text -> setSubtitle(title.text)
                is FilterTitle.All -> setSubtitle(getString(R.string.shift_filter_direction_all))
                is FilterTitle.NotSet -> setSubtitle(getString(R.string.not_set))
            }
        })
    }

    override fun viewDetails(initialPosition: Int, date: String) {
        viewModel.getDetails(date, filter) { details ->
            startActivity<RegChangeDetailsActivity>(
                DATE to date,
                DETAILS_LIST_INIT_POSITION to initialPosition,
                DETAILS_LIST to Gson().toJson(details)
            )
        }
    }

    override fun onDateChanged(date: String) {
        super.onDateChanged(date)
        viewModel.currentDate.value = date
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        return super.onCreateOptionsMenu(menu).also {
            menu.findItem(R.id.menu_prod_type).icon = getDrawable(filter.prodType.iconRes())
        }
    }

}
