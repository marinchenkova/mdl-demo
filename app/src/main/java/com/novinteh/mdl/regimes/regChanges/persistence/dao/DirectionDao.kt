package com.novinteh.mdl.regimes.regChanges.persistence.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.novinteh.mdl.regimes.regChanges.model.DirectionItem


@Dao
interface DirectionDao {

    @Query("SELECT * from directions WHERE date = :date ORDER BY rowN")
    fun items(date: String): LiveData<List<DirectionItem>>

    @Query("SELECT count(id) from directions WHERE date = :date")
    fun count(date: String): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(list: List<DirectionItem>)

    @Query("DELETE FROM directions")
    suspend fun deleteAll()

}