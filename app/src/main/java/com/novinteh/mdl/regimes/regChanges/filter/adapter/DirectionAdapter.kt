package com.novinteh.mdl.regimes.regChanges.filter.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.novinteh.mdl.R
import com.novinteh.mdl.paged.filter.Filterable
import com.novinteh.mdl.regimes.common.model.AomnItem
import com.novinteh.mdl.regimes.regChanges.model.RegChangeFilter
import com.novinteh.mdl.regimes.regChanges.model.DirWithAomns
import com.novinteh.mdl.util.replaceAll
import com.novinteh.mdl.view.DEF_ANIM_DURATION
import com.novinteh.mdl.view.appear
import com.novinteh.mdl.view.recycler.adapter.RadioAdapter
import com.novinteh.mdl.view.recycler.util.OnCheckListener
import com.novinteh.mdl.view.recycler.viewholder.CheckViewHolder
import kotlinx.android.synthetic.main.item_radio_direction.view.*


class DirectionAdapter: RadioAdapter<DirWithAomns, DirectionAdapter.ViewHolder>(DirWithAomns.DiffCallback),
    Filterable<RegChangeFilter> {

    val checkedAomns: List<AomnItem> get() = checkedList
    private val checkedList = mutableListOf<AomnItem>()
    override var filter = RegChangeFilter()

    private val aomnCheckListener= object : OnCheckListener<AomnItem> {
        override fun onChecksUpdated(position: Int, checked: List<AomnItem>) {
            if (isChecked(position)) checkedList.replaceAll(checked)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_radio_direction, parent, false)

        return ViewHolder(view, aomnCheckListener).also { holder ->
            holder.init(clickListener)
        }
    }

    override fun submitList(list: List<DirWithAomns>?) {
        super.submitList(list)
        checkFilterPosition()
    }

    override fun onFilterChanged(value: RegChangeFilter) {
        checkFilterPosition()
    }

    private fun checkFilterPosition() {
        val dir = currentList.find { it.id == filter.dirId } ?: return

        val position = positionOf(dir)
        if (position == RecyclerView.NO_POSITION) return

        switch(position)
        notifyItemRangeChanged(0, itemCount)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        if (getItem(position)?.id == filter.dirId && !filter.userShiftPriority) {
            holder.checkAomns(filter.aomnIds)
        }
    }

    class ViewHolder(view: View, private val aomnCheckListener: OnCheckListener<AomnItem>) :
        CheckViewHolder<DirWithAomns>(view) {

        private val button = itemView.button
        private val title = itemView.title
        private val recycler = itemView.recycler_aomns
        private val adapter = AomnAdapter()

        private val expander = itemView.expander
        private val line = itemView.line


        init {
            adapter.setOnCheckListener(object : OnCheckListener<AomnItem> {
                override fun onChecksUpdated(checked: List<AomnItem>) {
                    aomnCheckListener.onChecksUpdated(adapterPosition, checked)
                }
            })
            recycler.layoutManager = LinearLayoutManager(itemView.context, LinearLayoutManager.VERTICAL, false)
            recycler.itemAnimator = null
            recycler.adapter = adapter
        }

        override fun content(item: DirWithAomns) {
            title.text = item.name
            adapter.submitList(item.aomns)
            adapter.freezeAll()
        }

        fun checkAomns(ids: List<Int>) {
            adapter.checkIds(ids)
        }

        override fun check() {
            button.isChecked = true
            expand()
            adapter.notifyChecks()
        }

        override fun uncheck() {
            button.isChecked = false
            collapse()
        }

        private fun expand() {
            rotateExpander(180f)
            //animateAlpha(1f)
            setRecyclerAppeared(true)
        }

        private fun collapse() {
            rotateExpander(0f)
            //animateAlpha(0f)
            setRecyclerAppeared(false)
        }

        private fun rotateExpander(angle: Float) {
            expander.animate().rotation(angle).setDuration(DEF_ANIM_DURATION).start()
        }

        private fun animateAlpha(value: Float) {
            recycler.animate().alpha(value).setDuration(DEF_ANIM_DURATION * 3).start()
            line.animate().alpha(value).setDuration(DEF_ANIM_DURATION * 3).start()
        }

        private fun setRecyclerAppeared(enable: Boolean) {
            recycler.appear(enable)
            line.appear(enable)
        }

    }

}