package com.novinteh.mdl.regimes.dayPlans.model.mapping

import androidx.recyclerview.widget.DiffUtil
import com.novinteh.mdl.entity.data.common.Regime
import com.novinteh.mdl.util.Hm
import kotlin.math.abs

data class TehsectPlanListStub(
    override val beginHm: Hm,
    override val endHm: Hm,
    override val regimeQt: Int,
    override val regime: Regime,
    override val isStub: Boolean
) : ITehsectPlan<TehsectPlanListStub> {

    override fun clone(
        beginHm: Hm,
        endHm: Hm,
        regimeQt: Int,
        regime: Regime,
        isStub: Boolean
    ): TehsectPlanListStub {
        return TehsectPlanListStub(beginHm, endHm, regimeQt, regime, isStub)
    }


    companion object {

        val DiffCallback = object : DiffUtil.ItemCallback<TehsectPlanListStub>() {
            override fun areItemsTheSame(
                oldItem: TehsectPlanListStub,
                newItem: TehsectPlanListStub
            ): Boolean {
                return oldItem.beginHm == newItem.beginHm && oldItem.endHm == newItem.endHm
            }

            override fun areContentsTheSame(
                oldItem: TehsectPlanListStub,
                newItem: TehsectPlanListStub
            ): Boolean {
                return oldItem == newItem
            }

        }


        val Empty = TehsectPlanListStub(
            Hm.Hm0000,
            Hm.Hm2400,
            0,
            Regime(),
            isStub = true
        )

    }

}