package com.novinteh.mdl.regimes.regChanges.model

import androidx.recyclerview.widget.DiffUtil
import androidx.room.DatabaseView
import com.novinteh.mdl.entity.data.common.ProdType
import com.novinteh.mdl.entity.data.common.Regime
import com.novinteh.mdl.entity.data.regchanges.QKind
import com.novinteh.mdl.entity.data.regchanges.RegChangeState


@DatabaseView("""
    SELECT rowN,
           date, 
           aomnId, 
           prodType, 
           tehsectName, 
           fromReg, 
           toReg, 
           qKind, 
           planDate, 
           factDate, 
           isPlanNextDay, 
           isFactNextDay, 
           state
    from reg_changes
    ORDER BY rowN
""")
data class RegChangeListItem(
    val rowN: Int,
    val date: String,
    val aomnId: Int,
    val prodType: ProdType,
    val tehsectName: String,
    val fromReg: Regime,
    val toReg: Regime,
    val qKind: QKind,
    val planDate: String,
    val factDate: String,
    val isPlanNextDay: Boolean,
    val isFactNextDay: Boolean,
    val state: RegChangeState
) {

    companion object {

        val DiffCallback = object : DiffUtil.ItemCallback<RegChangeListItem>() {
            override fun areItemsTheSame(
                oldItem: RegChangeListItem,
                newItem: RegChangeListItem
            ): Boolean {
                return oldItem.rowN == newItem.rowN
            }

            override fun areContentsTheSame(
                oldItem: RegChangeListItem,
                newItem: RegChangeListItem
            ): Boolean {
                return oldItem == newItem
            }
        }

    }

}