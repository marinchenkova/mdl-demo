package com.novinteh.mdl.regimes.dayPlans.filter

import android.app.Application
import androidx.lifecycle.LiveData
import com.novinteh.mdl.data.MdlDatabase
import com.novinteh.mdl.paged.viewmodel.FilterViewModel
import com.novinteh.mdl.regimes.common.model.AomnItem
import com.novinteh.mdl.regimes.common.model.LoadStateInfo
import com.novinteh.mdl.regimes.common.persistence.repo.AomnRepo
import kotlinx.coroutines.launch


class DayPlanFilterViewModel(app: Application) : FilterViewModel(app) {

    override val LOG = true
    override val LOG_TAG = "DayPlansFilterViewModel"

    override val state: LiveData<LoadStateInfo>
    val aomns: LiveData<List<AomnItem>>

    private val aomnRepo: AomnRepo


    init {
        val db = MdlDatabase.getInstance(app)
        val stateDao = db.stateDao()

        aomnRepo = AomnRepo(stateDao, db.aomnDao())
        launch { aomnRepo.loadIfAbsent() }

        state = aomnRepo.state
        aomns = aomnRepo.items()
    }

    override fun refresh() {
        launch { aomnRepo.loadIfAbsent() }
    }

}