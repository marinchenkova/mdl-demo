package com.novinteh.mdl.regimes.shipments

import android.view.Menu
import androidx.lifecycle.Observer
import com.google.gson.Gson
import com.novinteh.mdl.R
import com.novinteh.mdl.paged.activity.PageActivity
import com.novinteh.mdl.paged.filter.changeFilter
import com.novinteh.mdl.paged.model.Tab
import com.novinteh.mdl.regimes.common.model.FilterTitle
import com.novinteh.mdl.regimes.shipments.adapter.ShipmentsPageAdapter
import com.novinteh.mdl.regimes.shipments.adapter.ShipmentsTabAdapter
import com.novinteh.mdl.regimes.shipments.details.ShipmentDetailsActivity
import com.novinteh.mdl.regimes.shipments.filter.ShipmentFilterActivity
import com.novinteh.mdl.regimes.shipments.model.ShipmentFilter
import com.novinteh.mdl.regimes.shipments.model.ShipmentListItem
import com.novinteh.mdl.util.RefreshRate
import com.novinteh.mdl.util.getPortId
import com.novinteh.mdl.util.getProdType
import com.novinteh.mdl.util.iconRes
import org.jetbrains.anko.startActivity


class  ShipmentActivity : PageActivity<ShipmentListItem, Tab, ShipmentFilter, ShipmentViewModel>() {

    override val LOG = true
    override val LOG_TAG = "ShipmentActivity"

    override val titleRes = R.string.activity_shipment_title
    override val refreshRate = RefreshRate(hours = 0, minutes = 2, seconds = 0)

    override val pageAdapter = ShipmentsPageAdapter()
    override val tabAdapter = ShipmentsTabAdapter()

    override val viewModelClass = ShipmentViewModel::class.java
    override val filterActivityClass = ShipmentFilterActivity::class.java

    override var filter = ShipmentFilter()


    override fun getFilterFromJson(json: String?): ShipmentFilter? {
        return Gson().fromJson(json, ShipmentFilter::class.java)
    }

    override fun buildEmptyTab(id: String): Tab {
        return Tab(id)
    }

    override fun viewDetails(initialPosition: Int, date: String) {
        log("view details")
        viewModel.getDetails(date, filter) { details ->
            startActivity<ShipmentDetailsActivity>(
                DATE to date,
                DETAILS_LIST_INIT_POSITION to initialPosition,
                DETAILS_LIST to Gson().toJson(details)
            )
        }
    }

    override fun onInitFilter() {
        changeFilter {
            prodType = getProdType()
            portId = getPortId()
        }

        viewModel.filterTitle.observe(this, Observer { title ->
            when (title) {
                is FilterTitle.Text -> setSubtitle(title.text)
                is FilterTitle.All -> setSubtitle(getString(R.string.shift_filter_ports_all))
                is FilterTitle.NotSet -> setSubtitle(getString(R.string.not_set))
            }
        })

        viewModel.ports.observe(this, Observer { list ->
            list?.firstOrNull()?.also { port ->
                changeFilter { setPortIdIfAbsent(port.id) }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        return super.onCreateOptionsMenu(menu).also {
            menu.findItem(R.id.menu_prod_type).icon = getDrawable(filter.prodType.iconRes())
        }
    }

}
