package com.novinteh.mdl.regimes.dayPlans.model

import com.novinteh.mdl.entity.data.common.ProdType

data class DayPlanFilter(var prodType: ProdType = ProdType.OIL, var aomnId: Int = -1) {

    val aomnIdAbsent get() = aomnId == -1


    fun setAomnIdIfAbsent(id: Int?) {
        if (aomnIdAbsent && id != null) aomnId = id
    }

}