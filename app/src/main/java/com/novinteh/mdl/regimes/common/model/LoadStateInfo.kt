package com.novinteh.mdl.regimes.common.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.novinteh.mdl.data.Result
import com.novinteh.mdl.util.Timestamp

@Entity(tableName = "load_states")
data class LoadStateInfo(
    @PrimaryKey val stateId: String,
    val timestamp: Timestamp,
    val lastResult: Result
)