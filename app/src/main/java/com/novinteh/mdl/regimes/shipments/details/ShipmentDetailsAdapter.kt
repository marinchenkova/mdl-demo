package com.novinteh.mdl.regimes.shipments.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.novinteh.mdl.R
import com.novinteh.mdl.entity.data.shipments.Shipment
import com.novinteh.mdl.entity.util.DateUtils
import com.novinteh.mdl.regimes.shipments.model.ShipmentItem
import com.novinteh.mdl.util.stringRes
import com.novinteh.mdl.view.recycler.adapter.ItemAdapter
import com.novinteh.mdl.view.recycler.viewholder.BindViewHolder
import kotlinx.android.synthetic.main.item_shipment_details.view.*
import org.jetbrains.anko.textResource


class ShipmentDetailsAdapter : ItemAdapter<ShipmentItem, ShipmentDetailsAdapter.DetailsViewHolder>(
    ShipmentItem.DiffCallback
) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DetailsViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_shipment_details, parent, false)

        return DetailsViewHolder(view)
    }


    class DetailsViewHolder(view: View) : BindViewHolder<ShipmentItem>(view) {
        
        private val tanker = itemView.tanker
        private val posNo = itemView.pos_no
        private val owner = itemView.owner
        private val state = itemView.state
        private val port = itemView.port
        private val berth = itemView.berth
        
        private val waitDm = itemView.wait_dm
        private val waitHm = itemView.wait_hm

        private val moorBeginDm = itemView.moor_begin_dm
        private val moorBeginHm = itemView.moor_begin_hm
        private val moorEndDm = itemView.moor_end_dm
        private val moorEndHm = itemView.moor_end_hm

        private val loadBeginDm = itemView.load_begin_dm
        private val loadBeginHm = itemView.load_begin_hm
        private val loadEndDm = itemView.load_end_dm
        private val loadEndHm = itemView.load_end_hm

        private val nettoPos = itemView.netto_pos
        private val nettoFact = itemView.netto_fact
        
        private val recyclerOps = itemView.recycler_ops
        private val adapter = OperationsAdapter()


        init {
            recyclerOps.layoutManager = LinearLayoutManager(itemView.context, LinearLayoutManager.VERTICAL, false)
            recyclerOps.adapter = adapter
        }

        override fun content(item: ShipmentItem) {
            tanker.text = item.tankerName
            posNo.text = item.position.no
            owner.text = item.position.groName
            state.textResource = item.state.stringRes()
            port.text = item.port.name
            berth.text = item.berthNum
            
            val waitDate = DateUtils.parseYmdHm(item.timeWaitStr)
            val waitDmStr = DateUtils.formatDm(waitDate)
            val waitHmStr = DateUtils.formatHm(waitDate)
            waitDm.text = waitDmStr
            waitHm.text = waitHmStr

            val moorBeginDate = DateUtils.parseYmdHm(item.moorBeginStr)
            val moorBeginDmStr = DateUtils.formatDm(moorBeginDate)
            val moorBeginHmStr = DateUtils.formatHm(moorBeginDate)
            moorBeginDm.text = moorBeginDmStr
            moorBeginHm.text = moorBeginHmStr

            val moorEndDate = DateUtils.parseYmdHm(item.moorEndStr)
            val moorEndDmStr = DateUtils.formatDm(moorEndDate)
            val moorEndHmStr = DateUtils.formatHm(moorEndDate)
            moorEndDm.text = moorEndDmStr
            moorEndHm.text = moorEndHmStr

            val loadBeginDate = DateUtils.parseYmdHm(item.loadBeginStr)
            val loadBeginDmStr = DateUtils.formatDm(loadBeginDate)
            val loadBeginHmStr = DateUtils.formatHm(loadBeginDate)
            loadBeginDm.text = loadBeginDmStr
            loadBeginHm.text = loadBeginHmStr

            val loadEndDate = DateUtils.parseYmdHm(item.loadEndStr)
            val loadEndDmStr = DateUtils.formatDm(loadEndDate)
            val loadEndHmStr = DateUtils.formatHm(loadEndDate)
            loadEndDm.text = loadEndDmStr
            loadEndHm.text = loadEndHmStr

            nettoPos.text = if (item.plan == null) "" else "%.3f".format(item.plan)
            nettoFact.text = if (item.fact == null) "" else "%.3f".format(item.fact)

            adapter.submitList(item.timesheet)
        }

    }


}