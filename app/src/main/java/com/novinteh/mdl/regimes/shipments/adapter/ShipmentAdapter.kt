package com.novinteh.mdl.regimes.shipments.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.novinteh.mdl.R
import com.novinteh.mdl.entity.data.shipments.ShipmentState
import com.novinteh.mdl.entity.util.DateUtils
import com.novinteh.mdl.regimes.shipments.model.ShipmentListItem
import com.novinteh.mdl.util.drawableRes
import com.novinteh.mdl.util.replaceAll
import com.novinteh.mdl.util.stringRes
import com.novinteh.mdl.view.appear
import com.novinteh.mdl.view.recycler.adapter.ClickAdapter
import com.novinteh.mdl.view.recycler.viewholder.ClickViewHolder
import kotlinx.android.synthetic.main.item_shipment.view.*
import kotlinx.android.synthetic.main.item_shipment_cargo.view.*
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.textResource


class ShipmentAdapter: ClickAdapter<ShipmentListItem, ClickViewHolder<ShipmentListItem>>(
    ShipmentListItem.DiffCallback
) {

    private val sectionPositions = mutableListOf<Int>()


    override fun getItemViewType(position: Int): Int {
        return if (position in sectionPositions) VIEW_TYPE_SECTION
        else VIEW_TYPE_ITEM
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ClickViewHolder<ShipmentListItem> {
        val inflater = LayoutInflater.from(parent.context)
        return if (viewType == VIEW_TYPE_SECTION) {
            val view = inflater.inflate(R.layout.item_shipment_cargo, parent, false)
            CargoViewHolder(view)
        }
        else {
            val view = inflater.inflate(R.layout.item_shipment, parent, false)
            ShipmentViewHolder(view).also { holder ->
                holder.init(clickListener)
            }
        }
    }

    override fun submitList(list: List<ShipmentListItem>?) {
        super.submitList(list?.addSections())
    }

    private fun List<ShipmentListItem>.addSections(): List<ShipmentListItem> {
        if (isEmpty()) return emptyList()

        val result = mutableListOf<ShipmentListItem>()
        val positions = mutableListOf<Int>()
        var prevSection: String? = null

        forEach { item ->
            if (item.cargo != prevSection) {
                prevSection = item.cargo
                result += item
                positions += result.lastIndex
            }
            result += item
        }

        sectionPositions.replaceAll(positions)
        return result
    }


    private inner class ShipmentViewHolder(view: View) : ClickViewHolder<ShipmentListItem>(view) {

        private val tanker = itemView.tanker_header
        private val lastOp = itemView.last_op
        private val posNo = itemView.pos_no
        private val loadBegin = itemView.load_begin
        private val loadEnd = itemView.load_end
        private val nettoPos = itemView.netto_pos
        private val nettoFact = itemView.netto_fact
        private val stateImage = itemView.state_image
        private val stateText = itemView.state_text


        override fun provideClickPosition(): Int {
            val adapterPos = adapterPosition
            return adapterPos - sectionPositions.filter { it < adapterPos }.size
        }

        override fun content(item: ShipmentListItem) {
            tanker.text = item.tankerName
            posNo.text = item.posNo

            val loadBeginDate = DateUtils.parseYmdHm(item.loadBeginStr)
            val loadBeginDhm = "${DateUtils.formatD(loadBeginDate)} ${DateUtils.formatHm(loadBeginDate)}"
            loadBegin.text = loadBeginDhm

            val loadEndDate = DateUtils.parseYmdHm(item.loadEndStr)
            val loadEndDhm = "${DateUtils.formatD(loadEndDate)} ${DateUtils.formatHm(loadEndDate)}"
            loadEnd.text = loadEndDhm

            nettoPos.text = if (item.plan == null) "" else "%.3f".format(item.plan)
            nettoFact.text = if (item.fact == null) "" else "%.3f".format(item.fact)

            stateText.textResource = item.state.stringRes()
            stateImage.imageResource = item.state.drawableRes()

            if (item.state != ShipmentState.DONE && item.timesheet.isNotEmpty()) {
                lastOp.appear(true)
                lastOp.text = item.timesheet.last().type
            }
        }

    }


    class CargoViewHolder(view: View) : ClickViewHolder<ShipmentListItem>(view) {

        private val cargo = itemView.cargo


        override fun content(item: ShipmentListItem) {
            cargo.text = item.cargo
        }

    }


    companion object {
        private const val VIEW_TYPE_ITEM = 0
        private const val VIEW_TYPE_SECTION = 1
    }

}