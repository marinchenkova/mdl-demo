package com.novinteh.mdl.regimes.shipments.model

import androidx.recyclerview.widget.DiffUtil
import androidx.room.ColumnInfo
import androidx.room.DatabaseView
import com.novinteh.mdl.entity.data.common.ProdType
import com.novinteh.mdl.entity.data.shipments.Operation
import com.novinteh.mdl.entity.data.shipments.ShipmentState


@DatabaseView("""
    SELECT rowN,
           date, 
           port_id, 
           tankerName,
           prodType,  
           cargo, 
           loadBeginStr, 
           loadEndStr, 
           position_no, 
           `plan`, 
           fact, 
           state, 
           timesheet
    from shipments
    ORDER BY rowN
""")
data class ShipmentListItem(
    val rowN: Int,
    val date: String,

    @ColumnInfo(name = "port_id")
    val portId: Int,
    val tankerName: String,

    val prodType: ProdType,
    val cargo: String,

    val loadBeginStr: String,
    val loadEndStr: String,

    @ColumnInfo(name = "position_no")
    val posNo: String?,

    val plan: Float?,
    val fact: Float?,

    val state: ShipmentState,
    val timesheet: List<Operation>
) {

    companion object {

        val DiffCallback = object : DiffUtil.ItemCallback<ShipmentListItem>() {
            override fun areItemsTheSame(
                oldItem: ShipmentListItem,
                newItem: ShipmentListItem
            ): Boolean {
                return oldItem.rowN == newItem.rowN
            }

            override fun areContentsTheSame(
                oldItem: ShipmentListItem,
                newItem: ShipmentListItem
            ): Boolean {
                return oldItem == newItem
            }
        }

    }

}