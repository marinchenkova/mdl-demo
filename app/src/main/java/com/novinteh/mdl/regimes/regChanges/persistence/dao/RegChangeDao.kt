package com.novinteh.mdl.regimes.regChanges.persistence.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.novinteh.mdl.regimes.regChanges.model.RegChangeItem
import com.novinteh.mdl.regimes.regChanges.model.RegChangeListItem
import com.novinteh.mdl.entity.data.common.ProdType


@Dao
interface RegChangeDao {

    @Query("""
        SELECT * from regchangelistitem 
        WHERE (date between :fromDate AND :toDate) 
          AND (prodType = :prodType) 
          AND (aomnId IN (:aomns)) 
        ORDER BY DATE
        """)
    fun listItems(fromDate: String, toDate: String, prodType: ProdType, aomns: List<Int>): LiveData<List<RegChangeListItem>>

    @Query("SELECT * from reg_changes WHERE date = :date AND prodType = :prodType AND aomnId IN (:aomns) ORDER BY rowN")
    fun items(date: String, prodType: ProdType, aomns: List<Int>): List<RegChangeItem>

    @Query("SELECT count(id) from reg_changes WHERE date = :date")
    fun count(date: String): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(list: List<RegChangeItem>)

    @Query("DELETE FROM reg_changes")
    suspend fun deleteAll()

}