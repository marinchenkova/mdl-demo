package com.novinteh.mdl.regimes.shipments.persistence.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.novinteh.mdl.regimes.regChanges.model.RegChangeItem
import com.novinteh.mdl.regimes.regChanges.model.RegChangeListItem
import com.novinteh.mdl.entity.data.common.ProdType
import com.novinteh.mdl.regimes.shipments.model.ShipmentItem
import com.novinteh.mdl.regimes.shipments.model.ShipmentListItem


@Dao
interface ShipmentDao {

    @Query("""
        SELECT * from shipmentlistitem 
        WHERE (date between :fromDate AND :toDate) 
          AND (prodType = :prodType) 
          AND (port_id = :portId) 
        ORDER BY DATE
        """)
    fun listItems(fromDate: String, toDate: String, prodType: ProdType, portId: Int): LiveData<List<ShipmentListItem>>

    @Query("SELECT * from shipments WHERE date = :date AND prodType = :prodType AND port_id = :portId ORDER BY rowN")
    fun items(date: String, prodType: ProdType, portId: Int): List<ShipmentItem>

    @Query("SELECT count(id) from shipments WHERE date = :date")
    fun count(date: String): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(list: List<ShipmentItem>)

    @Query("DELETE FROM shipments")
    suspend fun deleteAll()

}