package com.novinteh.mdl.regimes.dayPlans.persistence.repo

import androidx.lifecycle.LiveData
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.novinteh.mdl.data.repo.AsyncRepository
import com.novinteh.mdl.entity.data.common.ProdType
import com.novinteh.mdl.paged.filter.RangeSearch
import com.novinteh.mdl.rest.RequestService
import com.novinteh.mdl.data.Result
import com.novinteh.mdl.entity.data.tehsectPlans.TehsectPlan
import com.novinteh.mdl.regimes.common.persistence.dao.StateDao
import com.novinteh.mdl.regimes.dayPlans.model.DayPlanFilter
import com.novinteh.mdl.regimes.dayPlans.model.TehsectPlanItem
import com.novinteh.mdl.regimes.dayPlans.model.TehsectPlanListItem
import com.novinteh.mdl.regimes.dayPlans.model.toItem
import com.novinteh.mdl.regimes.dayPlans.persistence.dao.TehsectPlanDao

class TehsectPlanRepo(stateDao: StateDao, private val dao: TehsectPlanDao) : AsyncRepository(stateDao) {

    override val LOG = true
    override val LOG_TAG = "TehsectPlanRepo"

    override val stateId = "state.tehsect_plans"

    private val gson = GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()


    fun listItems(search: RangeSearch<DayPlanFilter>): LiveData<List<TehsectPlanListItem>> {
        return dao.listItems(
            search.fromDate,
            search.toDate,
            search.filter.prodType,
            search.filter.aomnId
        )
    }

    fun details(date: String, prodType: ProdType, aomnId: Int): List<TehsectPlanItem> {
        return dao.items(date, prodType, aomnId)
    }

    override suspend fun download(id: String): Result {
        return RequestService.getTehsectPlans(id)
    }

    override suspend fun upsert(id: String, data: Result.Success.Data) {
        val list = toList(id, data)
        dao.insert(list)
    }

    override suspend fun contains(id: String): Boolean {
        val count = dao.count(id)
        return count > 0
    }

    private fun toList(date: String, data: Result.Success.Data): List<TehsectPlanItem> = data.responseBody.charStream().use { reader -> gson
        .fromJson<List<TehsectPlan>>(reader, object : TypeToken<List<TehsectPlan>>() {}.type)
        ?.map { tehsectPlan -> tehsectPlan.toItem(date) }
        ?: emptyList()
    }

}