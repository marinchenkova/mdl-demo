package com.novinteh.mdl.regimes.common.persistence.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.novinteh.mdl.regimes.common.model.AomnItem


@Dao
interface AomnDao {

    @Query("SELECT * from aomns ORDER BY rowN")
    fun items(): LiveData<List<AomnItem>>

    @Query("SELECT count(id) from aomns")
    fun count(): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(list: List<AomnItem>)

    @Query("DELETE FROM aomns")
    suspend fun deleteAll()

}