package com.novinteh.mdl.regimes.regChanges.persistence.repo

import androidx.lifecycle.LiveData
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.novinteh.mdl.data.repo.AsyncRepository
import com.novinteh.mdl.regimes.regChanges.model.RegChangeItem
import com.novinteh.mdl.regimes.regChanges.model.RegChangeListItem
import com.novinteh.mdl.entity.data.common.ProdType
import com.novinteh.mdl.entity.data.regchanges.RegChange
import com.novinteh.mdl.paged.filter.RangeSearch
import com.novinteh.mdl.regimes.regChanges.model.RegChangeFilter
import com.novinteh.mdl.regimes.regChanges.model.toItem
import com.novinteh.mdl.regimes.regChanges.persistence.dao.RegChangeDao
import com.novinteh.mdl.rest.RequestService
import com.novinteh.mdl.data.Result
import com.novinteh.mdl.regimes.common.persistence.dao.StateDao

class RegChangeRepo(stateDao: StateDao, private val dao: RegChangeDao) : AsyncRepository(stateDao) {

    override val LOG = true
    override val LOG_TAG = "RegChangeRepo"

    override val stateId = "state.reg_changes"

    private val gson = GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()


    fun listItems(search: RangeSearch<RegChangeFilter>): LiveData<List<RegChangeListItem>> {
        return dao.listItems(
            search.fromDate,
            search.toDate,
            search.filter.prodType,
            search.filter.aomnIds
        )
    }

    fun details(date: String, prodType: ProdType, aomns: List<Int>): List<RegChangeItem> {
        return dao.items(date, prodType, aomns)
    }

    override suspend fun download(id: String): Result {
        return RequestService.getRegChanges(id)
    }

    override suspend fun upsert(id: String, data: Result.Success.Data) {
        val list = toList(id, data)
        dao.insert(list)
    }

    override suspend fun contains(id: String): Boolean {
        val count = dao.count(id)
        return count > 0
    }

    private fun toList(date: String, data: Result.Success.Data): List<RegChangeItem> = data.responseBody.charStream().use { reader -> gson
        .fromJson<List<RegChange>>(reader, object : TypeToken<List<RegChange>>() {}.type)
        ?.map { regChange -> regChange.toItem(date) }
        ?: emptyList()
    }

}