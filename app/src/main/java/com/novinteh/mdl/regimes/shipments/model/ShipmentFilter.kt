package com.novinteh.mdl.regimes.shipments.model

import com.novinteh.mdl.entity.data.common.ProdType

data class ShipmentFilter(var prodType: ProdType = ProdType.OIL, var portId: Int = -1) {

    val portIdAbsent get() = portId == -1


    fun setPortIdIfAbsent(id: Int?) {
        if (portIdAbsent && id != null) portId = id
    }

}