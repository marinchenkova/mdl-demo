package com.novinteh.mdl.regimes.dayPlans.model

import androidx.room.DatabaseView
import com.novinteh.mdl.entity.data.common.ProdType
import com.novinteh.mdl.entity.data.common.Regime
import com.novinteh.mdl.entity.util.DateUtils
import com.novinteh.mdl.regimes.dayPlans.model.mapping.TehsectPlanListStub
import com.novinteh.mdl.util.Hm

@DatabaseView("""
    SELECT rowN,
           date, 
           tehsectName,
           aomnId, 
           prodType, 
           beginStr,
           endStr,
           regimeQt,
           regime
    from tehsect_plans
    ORDER BY rowN
""")
data class TehsectPlanListItem(
    val rowN: Int,
    val date: String,
    val tehsectName: String,
    val aomnId: Int,
    val prodType: ProdType,
    val beginStr: String,
    val endStr: String,
    val regimeQt: Int,
    val regime: Regime
) {

    fun toStub(): TehsectPlanListStub {
        val beginDate = DateUtils.parseYmdHm(beginStr)
        val endDate = DateUtils.parseYmdHm(endStr)
        val nextDay = DateUtils.diffDays(beginDate, endDate) > 0

        return TehsectPlanListStub(
            Hm.fromDate(beginDate, false),
            Hm.fromDate(endDate, nextDay),
            regimeQt,
            regime,
            isStub = false
        )
    }

    override fun toString(): String {
        return "Plan(${beginStr} - ${endStr}: $regime ($regimeQt))"
    }

}

