package com.novinteh.mdl.regimes.dayPlans.model.mapping

import com.novinteh.mdl.entity.data.common.Regime
import com.novinteh.mdl.util.Hm
import kotlin.math.abs

interface ITehsectPlan<T> {

    val beginHm: Hm
    val endHm: Hm
    val regimeQt: Int
    val regime: Regime
    val isStub: Boolean


    fun clone(
        beginHm: Hm = this.beginHm,
        endHm: Hm = this.endHm,
        regimeQt: Int = this.regimeQt,
        regime: Regime = this.regime,
        isStub: Boolean = this.isStub
    ): T

}


fun <T: ITehsectPlan<T>> List<T>.fillVoids(onEmpty: T): List<T> {
    if (isEmpty()) return listOf(onEmpty)

    val result = mutableListOf<T>()
    val regimeZero = first().regime.buildZeroRegime()

    var hm = Hm.Hm0000
    forEach { plan ->
        if (plan.beginHm != hm) {
            result += plan.clone(
                beginHm = hm,
                endHm = plan.beginHm,
                regimeQt = 0,
                regime = regimeZero,
                isStub = true
            )
        }

        result += plan
        hm = plan.endHm
    }

    if (!hm.is2400()) {
        result += result.last().clone(
            beginHm = hm,
            endHm = Hm.Hm2400,
            regimeQt = 0,
            regime = regimeZero,
            isStub = true
        )
    }

    return result
}

fun <T: ITehsectPlan<T>> List<T>.filterSimilar(): List<T> {
    if (size <= 1) return this

    var prev = first()
    val result = mutableListOf<T>()

    for (i in 1 until size) {
        val curr = get(i)

        if (planChanged(curr, prev)) {
            prev = prev.clone(endHm = curr.beginHm)
            result += prev

            prev = curr

            if (i == lastIndex) result += curr
            continue
        }

        if (i == lastIndex) result += prev.clone(endHm = curr.endHm)
    }

    return result
}

private fun planChanged(curr: ITehsectPlan<*>, prev: ITehsectPlan<*>) =
    (curr.regime.name != prev.regime.name) || qChanged(
        curr.regimeQt,
        prev.regimeQt
    )

private fun qChanged(qCurr: Int, qPrev: Int): Boolean {
    if (qPrev == 0) return qCurr > 0
    return abs((qPrev - qCurr) / qPrev.toFloat()) > 0.1
}