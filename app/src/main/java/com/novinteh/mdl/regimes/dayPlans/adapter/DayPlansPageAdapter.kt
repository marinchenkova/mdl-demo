package com.novinteh.mdl.regimes.dayPlans.adapter

import android.view.View
import com.novinteh.mdl.paged.viewpager.adapter.PageAdapter
import com.novinteh.mdl.paged.viewpager.viewholder.PageViewHolder
import com.novinteh.mdl.regimes.dayPlans.model.DayPlanListItem


class DayPlansPageAdapter : PageAdapter<DayPlanListItem, DayPlansPageAdapter.ViewHolder>() {

    override val LOG = true
    override val LOG_TAG = "DayPlansPageAdapter"


    override fun onCreateViewHolder(view: View): ViewHolder {
        return ViewHolder(view)
    }


    class ViewHolder(view: View): PageViewHolder<DayPlanListItem>(view) {

        override val subAdapter = DayPlanAdapter()

    }

}