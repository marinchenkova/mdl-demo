package com.novinteh.mdl.regimes.regChanges.persistence.converters

import androidx.room.TypeConverter
import com.google.gson.reflect.TypeToken
import com.novinteh.mdl.entity.JsonParser
import com.novinteh.mdl.entity.data.common.ProdType
import com.novinteh.mdl.entity.data.common.Regime
import com.novinteh.mdl.entity.data.regchanges.QKind
import com.novinteh.mdl.entity.data.regchanges.RegChangeState
import com.novinteh.mdl.entity.data.regchanges.Switch


class RegChangeConverters {

    private val parser = JsonParser()


    @TypeConverter
    fun qKindToString(qKind: QKind): String = qKind.name

    @TypeConverter
    fun qKindFromString(str: String): QKind = QKind.fromString(str)

    @TypeConverter
    fun regChangeStateToString(state: RegChangeState): String = state.name

    @TypeConverter
    fun regChangeStateFromString(str: String): RegChangeState = RegChangeState.fromString(str)

    @TypeConverter
    fun switchesToString(switches: List<Switch>): String = parser.toJson(switches)

    @TypeConverter
    fun switchesFromString(str: String): List<Switch> = parser
        .listFromJson(str, object : TypeToken<List<Switch>>(){})

}