package com.novinteh.mdl.login

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.novinteh.mdl.R
import com.novinteh.mdl.data.Result
import com.novinteh.mdl.login.server.ServerActivity
import com.novinteh.mdl.main.MainActivity
import com.novinteh.mdl.main.ReceiverActivity
import com.novinteh.mdl.rest.*
import com.novinteh.mdl.util.*
import com.novinteh.mdl.view.afterTextChanged
import com.novinteh.mdl.view.show
import com.novinteh.mdl.view.transition.startActivityClearTask
import kotlinx.android.synthetic.main.activity_login.*
import okhttp3.Credentials
import org.jetbrains.anko.startActivity


/**
 * A login screen that offers login via login/password.
 * This activity starts if empty is not logged in.
 * After start of Main Activity back stack will be
 * cleared for proper back navigation.
 */
class LoginActivity : ReceiverActivity() {

    override val LOG: Boolean get() = false
    override val LOG_TAG: String get() = "LoginActivity"

    private val serverActivitySafeClick = SafeEvent { startActivity<ServerActivity>() }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initToolbar(getString(R.string.activity_login_title), up = false)
        initLoginForm()
    }

    /**
     * Set up the login form.
     */
    private fun initLoginForm() {
        activity_login_user.setText(getUser())
        activity_login_password.setText(getPass())

        checkCanSign()
        activity_login_user.afterTextChanged { checkCanSign() }
        activity_login_password.afterTextChanged { checkCanSign() }

        activity_login_button.setSafeClickListener {
            attemptLogin()
        }
    }

    /**
     * Toolbar menu initialization.
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.server, menu)
        return true
    }

    /**
     * Toolbar action check.
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.menu_server -> {
            serverActivitySafeClick.launch()
            true
        }

        else -> super.onOptionsItemSelected(item)
    }


    /**
     * Async login operation attempt. If error occurs, alert dialog will be shown,
     * if not, MainActivity launches.
     */
    private fun attemptLogin() {
        showProgress(true)

        val login = activity_login_user.text.toString()
        val password = activity_login_password.text.toString()
        val credentials = Credentials.basic(login, password)

        setUser(login)
        setPass(password)
        setCredentials(credentials)

        RequestService.login(credentials) { result ->
            when (result) {
                is Result.Success -> {
                    setLogged(true)
                    startActivityClearTask<MainActivity>()
                }

                is Result.Error -> {
                    showErrorDialog(pickErrorMessage(result))
                    showProgress(false)
                }
            }
        }
    }

    private fun showProgress(show: Boolean) {
        enableLoginForms(!show)
        progress_bar.show(show)
        if (!show) checkCanSign()
    }

    /**
     * Enable Sign in button if login and password are valid.
     */
    private fun checkCanSign() {
        activity_login_button.isEnabled = activity_login_user.text.isNotBlank()
                && activity_login_password.text.isNotBlank()
    }

    /**
     * Enable or disable login, password and sign in button views.
     */
    private fun enableLoginForms(enabled: Boolean) {
        activity_login_user.isEnabled = enabled
        activity_login_password.isEnabled = enabled
        activity_login_button.isEnabled = enabled
        activity_login_button.text =
            if (!enabled) ""
            else getString(R.string.activity_login_button_sign_in)
    }

}
