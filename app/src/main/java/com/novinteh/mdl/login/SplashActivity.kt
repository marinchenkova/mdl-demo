package com.novinteh.mdl.login

import android.os.Bundle
import com.novinteh.mdl.R
import com.novinteh.mdl.main.MainActivity
import com.novinteh.mdl.main.ToolbarActivity
import com.novinteh.mdl.util.isLogged
import com.novinteh.mdl.util.setAppRelaunched
import com.novinteh.mdl.view.transition.startActivityClearTask
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.jetbrains.anko.startActivity

/**
 * Splash Activity is launcher of app with No Display theme.
 * Activity checks if user is logged in.
 * If so, go to Main Activity, else go to Login Activity.
 * Back stack is clearing while starting new activity for
 * providing proper back navigation.
 */
class SplashActivity : ToolbarActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setAppRelaunched(true)
        checkLogged()
        finish()
    }

    /**
     * Check if user is logged in.
     */
    private fun checkLogged() {
        if (isLogged()) startActivity<MainActivity>()
        else startActivity<LoginActivity>()
    }

}
