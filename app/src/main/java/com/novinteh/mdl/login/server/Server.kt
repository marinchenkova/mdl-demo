package com.novinteh.mdl.login.server

import android.content.Context
import androidx.recyclerview.widget.DiffUtil
import com.novinteh.mdl.R
import com.novinteh.mdl.regimes.regChanges.model.DirWithAomns

data class Server(var url: String, val state: State) {

    var order = 0
    val data get() = "$order$url"


    fun checkUrl() = when {
        url.isBlank() -> "2$DefaultUrl"
        data.last() == '/' -> data.dropLast(1)
        else -> data
    }

    override fun toString(): String {
        return data
    }


    enum class State {
        OFFLINE,
        MDL,
        GIT,
        USER
    }


    companion object {

        val DiffCallback = object : DiffUtil.ItemCallback<Server>() {
            override fun areItemsTheSame(oldItem: Server, newItem: Server): Boolean {
                return oldItem.url == newItem.url
            }

            override fun areContentsTheSame(oldItem: Server, newItem: Server): Boolean {
                return oldItem == newItem
            }
        }


        fun mdl() = Server(DefaultUrl, State.MDL)

        fun empty() = Server("", State.USER)

        fun fromUrl(ctx: Context, data: String): Server {
            val url = data.substring(1)
            val order = data[0].toString().toInt()
            val server = Server(url, when (url) {
                DefaultUrl -> State.MDL
                ctx.getString(R.string.activity_server_config_offline) -> State.OFFLINE
                ctx.getString(R.string.activity_server_config_git) -> State.GIT
                else -> State.USER
            })
            server.order = order
            return server
        }

        fun fromUrls(ctx: Context, set: MutableSet<String>) =
            set.map { fromUrl(ctx, it) }.sortedBy { it.order }

        fun toUrls(servers: Iterable<Server>) =
            servers.map { it.checkUrl() }.toMutableSet()

        fun defaultSet(ctx: Context) = fromUrls(
            ctx,
            mutableSetOf(
                "0${ctx.getString(R.string.activity_server_config_offline)}",
                "1${ctx.getString(R.string.activity_server_config_git)}",
                "2$DefaultUrl"
            )
        )

    }

}