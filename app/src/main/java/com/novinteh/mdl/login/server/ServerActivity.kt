package com.novinteh.mdl.login.server

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.novinteh.mdl.R
import com.novinteh.mdl.main.ToolbarActivity
import com.novinteh.mdl.rest.RequestService
import com.novinteh.mdl.util.*
import com.novinteh.mdl.view.show
import kotlinx.android.synthetic.main.activity_server.*
import org.jetbrains.anko.sdk27.coroutines.onClick


class ServerActivity : ToolbarActivity() {

    private val adapter = ServerRadioAdapter()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_server)
        initToolbar(getString(R.string.activity_server_title))
        initRecycler()
    }

    private fun initRecycler() {
        val list = Server.defaultSet(this) + getServers()
        val selected = getSelectedServer()

        adapter.submitList(list)
        adapter.switch(selected)

        adapter.onSizeChanged { if (it >= 10) add.show(false) }

        add.onClick { adapter.add(Server.empty()) }

        recycler_data.itemAnimator = null
        recycler_data.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recycler_data.adapter = adapter
    }

    private fun save() {
        setServers(adapter.expose())
        adapter.checkedItem?.also { server ->
            setSelectedServer(server)
            RequestService.server = server
        }
        onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.apply, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.menu_apply -> {
            save()
            true
        }

        else -> super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}
