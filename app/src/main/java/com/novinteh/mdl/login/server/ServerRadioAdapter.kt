package com.novinteh.mdl.login.server

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.novinteh.mdl.R
import com.novinteh.mdl.view.afterTextChanged
import com.novinteh.mdl.view.recycler.adapter.RadioAdapter
import com.novinteh.mdl.view.recycler.viewholder.CheckViewHolder
import com.novinteh.mdl.view.recycler.viewholder.ClickViewHolder
import kotlinx.android.synthetic.main.item_radio_server_url.view.*
import kotlinx.android.synthetic.main.item_radio_server_url_editable.view.*
import org.jetbrains.anko.sdk27.coroutines.onFocusChange


class ServerRadioAdapter: RadioAdapter<Server, CheckViewHolder<Server>>(Server.DiffCallback) {

    private var onSizeChanged: (Int) -> Unit = {}


    fun onSizeChanged(block: (Int) -> Unit) {
        onSizeChanged = block
    }

    override fun submitList(list: List<Server>?) {
        super.submitList(list)
        onSizeChanged(itemCount)
    }

    fun add(item: Server) {
        item.order = itemCount
        submitList(currentList + item)
        switch(item.order)
        onSizeChanged(itemCount)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CheckViewHolder<Server> {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            VIEW_TYPE_HARD -> {
                val view = inflater.inflate(
                    R.layout.item_radio_server_url,
                    parent,
                    false
                )

                TextViewHolder(view)
            }
            else -> {
                val view = inflater.inflate(
                    R.layout.item_radio_server_url_editable,
                    parent,
                    false
                )

                EditableViewHolder(view) { text, position ->
                    getItem(position)?.url = text
                }
            }
        }.also { holder ->
            holder.init(clickListener)
        }
    }

    override fun getItemViewType(position: Int) = when (getItem(position)?.state) {
        Server.State.USER -> VIEW_TYPE_EDITABLE
        else -> VIEW_TYPE_HARD
    }

    fun expose() = currentList
        .distinct()
        .filter { it.state == Server.State.USER && it.url.isNotBlank() }


    class TextViewHolder(view: View): CheckViewHolder<Server>(view) {

        private val server: TextView = itemView.text
        private val button = itemView.radio


        override fun content(item: Server) {
            server.text = item.url
        }

        override fun check() {
            button.isChecked = true
        }

        override fun uncheck() {
            button.isChecked = false
        }

    }


    class EditableViewHolder(view: View, onEditText: (text: String, position: Int) -> Unit):
        CheckViewHolder<Server>(view) {

        private val server = itemView.url
        private val button = itemView.button


        init {
            server.afterTextChanged { onEditText(server.text.toString(), adapterPosition) }
            server.onFocusChange { _, hasFocus -> if (hasFocus) click() }
        }

        override fun content(item: Server) {
            server.setText(item.url)
        }

        override fun check() {
            button.isChecked = true
        }

        override fun uncheck() {
            button.isChecked = false
        }

    }


    companion object {
        private const val VIEW_TYPE_HARD = 0
        private const val VIEW_TYPE_EDITABLE = 1
    }

}