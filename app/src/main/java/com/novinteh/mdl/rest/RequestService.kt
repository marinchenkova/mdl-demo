package com.novinteh.mdl.rest

import android.content.Context
import com.novinteh.mdl.data.Result
import com.novinteh.mdl.login.server.Server
import com.novinteh.mdl.util.*
import kotlinx.coroutines.*
import kotlin.random.Random


object RequestService : MdlApi, Logging {

    override val LOG = false
    override val LOG_TAG = "RequestService"

    var server = Server.mdl()
    override val serverUrl get() = server.url


    fun init(ctx: Context) {
        with(ctx) {
            server = getSelectedServer()
            RestClient.credentials = getCredentials()
        }
    }

    fun login(credentials: String, onReceive : (Result) -> Unit) {
        CoroutineScope(Dispatchers.IO).launch {
            log("Start login...")

            RestClient.credentials = credentials

            val result = when (server.state) {
                Server.State.OFFLINE, Server.State.GIT -> Result.Success.Empty
                else -> RestClient.request(login())
            }

            if (result is Result.Success.Data) result.responseBody.close()

            withContext(Dispatchers.Main) { onReceive(result) }
        }
    }

    suspend fun getPorts() = request(FilePorts, portAll())

    suspend fun getAomns() = request(FileAomns, aomnAll())

    suspend fun getUserShifts(date: String) = request(FileUserShifts, userShifts(date))

    suspend fun getRegChanges(date: String) = request(FileRegChanges, regChanges(date))

    suspend fun getDirections(date: String) = request(FileDirections, directions(date))

    suspend fun getTehsectPlans(date: String) = request(FileTehsectPlans, tehsectPlans(date))

    suspend fun getShipments(date: String) = request(FileShipments, shipments(date))

    private suspend fun request(file: String, url: String): Result {
        log("Requesting $url")
        return when (server.state) {
            Server.State.OFFLINE -> Result.Error.Client("Offline not supported.")
            Server.State.GIT -> RestClient.request("$GitUrl$file")
            else -> RestClient.request(url)
        }
    }

    private suspend fun Result.mockDelay(from: Long = 1000, to: Long = 2000): Result {
        delay(Random.nextLong(from, to))
        return this
    }

    private fun Result.maybeError(possibility: Float): Result =
        if (Random.nextFloat() <= possibility) Result.Error.Server("Generated error")
        else this

}