package com.novinteh.mdl.rest

import com.novinteh.mdl.data.Result
import com.novinteh.mdl.util.Logging
import kotlinx.coroutines.CompletableDeferred
import okhttp3.*
import java.io.IOException
import java.util.concurrent.TimeUnit


object RestClient : Logging {

    private const val CONNECT_TIMEOUT_DEF = 10L
    private const val READ_TIMEOUT_DEF = 60L

    override val LOG = true
    override val LOG_TAG = "RestClient"

    private val okHttp = OkHttpClient.Builder()
        .connectTimeout(CONNECT_TIMEOUT_DEF, TimeUnit.SECONDS)
        .readTimeout(READ_TIMEOUT_DEF, TimeUnit.SECONDS)
        .build()

    var credentials: String? = null


    private fun Request.Builder.authorize(): Request.Builder {
        credentials?.also { header("Authorization", it) }
        return this
    }

    suspend fun request(url: String): Result {
        val request = Request.Builder()
            .url(url)
            .authorize()
            .build()

        val result = CompletableDeferred<Result>()
        okHttp.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                log("Request ${request.url()} failed with exception: $e")
                result.complete(Result.Error.Client())
            }
            override fun onResponse(call: Call, response: Response) {
                result.complete(makeRestResult(response))
            }
        })

        return result.await()
    }

    private fun makeRestResult(response: Response): Result {
        val body = response.body()
        return when {
            response.code() == 401 -> {
                log(response, "login failed")
                response.close()
                Result.Error.Login()
            }

            response.code() != 200 || body == null -> {
                log(response, "server error")
                val message: String = response.message().let { msg ->
                    val code = response.code()
                    if (msg.isBlank()) "$code"
                    else "$code $msg"
                }

                response.close()
                Result.Error.Server(message)
            }

            else -> {
                log(response, "data ok")
                Result.Success.Data(body)
            }
        }
    }

    private fun log(response: Response, msg: String) =
        log( "Response: $msg, ${response.code()} ${response.message()} for request ${response.request().url()}")

}