package com.novinteh.mdl.rest


interface MdlApi {

    val serverUrl: String


    fun login() = "${serverUrl}/"

    fun aomnAll() = "${serverUrl}/aomn/list/"

    fun portAll() = "${serverUrl}/port/list/"

    fun userShifts(date: String) = "${serverUrl}/usershifts/$date/"

    fun directions(date: String) = "${serverUrl}/directions/$date/"

    fun regChanges(date: String) = "${serverUrl}/regchanges/$date/"

    fun tehsectPlans(date: String) = "${serverUrl}/tehsectplans/$date/"

    fun shipments(date: String) = "${serverUrl}/shipments/$date/"

}