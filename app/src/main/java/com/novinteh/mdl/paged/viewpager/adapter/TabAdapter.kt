package com.novinteh.mdl.paged.viewpager.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.NO_POSITION
import com.novinteh.mdl.R
import com.novinteh.mdl.entity.util.DateUtils
import com.novinteh.mdl.paged.model.StringId
import com.novinteh.mdl.paged.model.Tab
import com.novinteh.mdl.paged.viewpager.util.OnTabClickListener
import com.novinteh.mdl.paged.viewpager.viewholder.TabViewHolder
import com.novinteh.mdl.util.Logging
import com.novinteh.mdl.view.recycler.adapter.ItemAdapter
import com.novinteh.mdl.view.recycler.util.OnItemClickListener
import com.novinteh.mdl.view.recycler.util.Payload
import com.novinteh.mdl.view.recycler.util.Ref

/**
 * [TabAdapter] provides on item view click event callback and highlighting.
 */
abstract class TabAdapter<I: Tab, VH: TabViewHolder<I>>(diffCallback: DiffUtil.ItemCallback<I>) :
    ItemAdapter<I, VH>(diffCallback),
    Logging {

    /**
     * Position that must be highlighted when item binds to view holder.
     */
    private var selectedPosition = NO_POSITION
    private var tabOffset = 0

    private var tabClickListener: OnTabClickListener? = null
    private var clickListener = Ref<OnItemClickListener>(object : OnItemClickListener {
        override fun onItemClick(position: Int) {
            tabClickListener?.onTabClick(selectedPosition, position)
        }
    })


    final override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_viewpager_tab, parent, false)

        return createViewHolder(view).also { holder ->
            holder.init(clickListener)
        }
    }

    protected abstract fun createViewHolder(view: View): VH

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.checkHighlight(position)
        super.onBindViewHolder(holder, position)
    }

    override fun onBindViewHolder(holder: VH, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) return onBindViewHolder(holder, position)
        if (payloads[0] as? Payload == Payload.CHECK) holder.checkHighlight(position)
        else super.onBindViewHolder(holder, position, payloads)
    }

    private fun VH.checkHighlight(position: Int) =
        if (position == selectedPosition) highlight()
        else unhighlight()

    override fun getItemCount(): Int = super.getItemCount() + tabOffset * 2

    override fun getItem(position: Int): I? =
        if (inOffset(position)) null
        else super.getItem(position - tabOffset)

    override fun getItemId(position: Int): Long {
        return if (inOffset(position)) getOffsetPositionId(position)
        else getItem(position)?.id?.hashCode()?.toLong() ?: 0
    }

    private fun inOffset(position: Int) =
        position < tabOffset || position >= itemCount - tabOffset

    private fun getOffsetPositionId(position: Int): Long = when {
        position < tabOffset -> (-position - 1).toLong()
        else -> (position - itemCount - tabOffset).toLong()
    }

    fun setTabOffset(value: Int) {
        tabOffset = value
    }

    fun setOnTabClickListener(listener: OnTabClickListener) {
        tabClickListener = listener
    }

    fun removeOnTabClickListener() {
        tabClickListener = null
    }

    fun setIndicatorPosition(position: Int, notifyFrom: Int, notifySize: Int) {
        selectedPosition = position
        notifyItemRangeChanged(notifyFrom, notifySize, Payload.CHECK)
    }

}