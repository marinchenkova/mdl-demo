package com.novinteh.mdl.paged.viewpager.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.novinteh.mdl.R
import com.novinteh.mdl.paged.model.Page
import com.novinteh.mdl.paged.viewpager.viewholder.PageViewHolder
import com.novinteh.mdl.util.Logging
import com.novinteh.mdl.view.recycler.adapter.ItemAdapter
import com.novinteh.mdl.view.recycler.util.OnItemClickListener
import com.novinteh.mdl.view.recycler.util.Ref


abstract class PageAdapter<I, VH: PageViewHolder<I>> :
    ItemAdapter<Page<I>, VH>(Page.getDiffCallback()),
    Logging {

    private val viewPool = RecyclerView.RecycledViewPool()
    private val subItemClickListener = Ref<OnItemClickListener>()


    final override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_viewpager_page, parent, false)

        return onCreateViewHolder(view).also { holder ->
            holder.init(subItemClickListener, viewPool)
        }
    }

    protected abstract fun onCreateViewHolder(view: View): VH

    override fun getItemId(position: Int): Long {
        return getItem(position)?.id?.hashCode()?.toLong() ?: 0
    }

    fun getPageId(position: Int): String? = getItem(position)?.id

    fun setOnSubItemClickListener(listener: OnItemClickListener) {
        subItemClickListener.set(listener)
    }

    fun removeOnSubItemClickListener() {
        subItemClickListener.clear()
    }

}