package com.novinteh.mdl.paged.filter

import com.novinteh.mdl.util.replaceAll

class RangeSearch<F>(
    initialDates: List<String>,
    var filter: F,
    private val maxRange: Int
) {

    private val list = initialDates.toMutableList()
    val currentDates: List<String> get() = list

    val fromDate get() = list.firstOrNull() ?: ""
    val toDate get() = list.lastOrNull() ?: ""


    init {
        dropEndIfSizeExceedsMaxRange()
    }

    fun addDatesBefore(dates: List<String>) {
        list.addAll(0, dates)
        dropEndIfSizeExceedsMaxRange()
    }

    fun addDatesAfter(dates: List<String>) {
        list.addAll(dates)
        dropStartIfSizeExceedsMaxRange()
    }

    private fun dropStartIfSizeExceedsMaxRange() {
        dropIfSizeExceedsMaxRange(List<String>::drop)
    }

    private fun dropEndIfSizeExceedsMaxRange() {
        dropIfSizeExceedsMaxRange(List<String>::dropLast)
    }

    private inline fun dropIfSizeExceedsMaxRange(dropWhat: List<String>.(n: Int) -> List<String>) {
        if (list.size > maxRange) {
            val resized = list.dropWhat(list.size - maxRange)
            list.replaceAll(resized)
        }
    }

    override fun toString(): String {
        return "RangeSearch(from $fromDate to $toDate, filter $filter)"
    }

}