package com.novinteh.mdl.paged.viewmodel

import android.app.Application
import androidx.lifecycle.*
import com.novinteh.mdl.util.Logging
import kotlinx.coroutines.*


abstract class ScopeViewModel(app: Application) : AndroidViewModel(app),
    CoroutineScope,
    Logging {

    override val coroutineContext = Dispatchers.IO


    protected inline fun <T> MutableLiveData<T>.setAsync(crossinline make: suspend () -> T) =
        launch { postValue(make()) }

    protected inline fun <T, K, R> LiveData<T>.combineWith(
        other: LiveData<K>,
        crossinline map: (T, K) -> R
    ): LiveData<R> {
        val mediator = MediatorLiveData<R>()

        val merge = {
            val thisValue = this.value
            val otherValue = other.value

            if (thisValue != null && otherValue != null) {
                mediator.postValue(map(thisValue, otherValue))
            }
        }

        mediator.addSource(this) { merge() }
        mediator.addSource(other) { merge() }

        return mediator
    }

    protected inline fun <T, R> LiveData<T>.mapAsync(
        crossinline map: suspend (T) -> R
    ): LiveData<R> {
        val mediator = MediatorLiveData<R>()

        mediator.addSource(this) {
            value?.also { value ->
                mediator.setAsync { map(value) }
            }
        }

        return mediator
    }

}