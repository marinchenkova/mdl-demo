package com.novinteh.mdl.paged.filter

interface Filterable<F> {

    var filter: F


    fun onFilterChanged(value: F)

}

fun <V> Filterable<V>.setFilter(value: V) {
    filter = value
    onFilterChanged(value)
}

fun <F> Filterable<F>.changeFilter(change: F.() -> Unit) {
    val oldHashCode = filter.hashCode()
    if (filter.apply(change).hashCode() != oldHashCode) onFilterChanged(filter)
}