package com.novinteh.mdl.paged.viewpager

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.novinteh.mdl.R
import com.novinteh.mdl.paged.viewpager.RecyclerTabLayout.TabScrollSync
import com.novinteh.mdl.paged.viewpager.adapter.TabAdapter
import com.novinteh.mdl.view.isLandscape
import kotlin.math.roundToInt


/**
 * Amount of tabs that are visible on screen by default if attribute was not set.
 */
const val DEF_TABS_ON_SCREEN = 1

/**
 * Draw indicator if attribute was not set.
 */
const val DEF_DRAW_INDICATOR = false


/**
 * This is a widget for displaying tabs with parent [RecyclerViewPager].
 * It is based on [RecyclerViewPager] and provides scroll sync by [TabScrollSync] object
 * when parent is attached by [setUpWithRecyclerViewPager].
 */
class RecyclerTabLayout @JvmOverloads constructor(
    ctx: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : RecyclerViewPager(ctx, attrs, defStyle) {

    /**
     * How many tabs should be visible on screen.
     */
    val tabsOnScreen: Int

    /**
     * Half of a [tabsOnScreen] value.
     */
    val tabOffset: Int

    /**
     * Indicator properties holder.
     */
    private val indicator = Indicator()

    /**
     * Helps [RecyclerTabLayout] to resize views according to [tabsOnScreen] value.
     */
    private val tabDecoration = TabDecoration()

    /**
     * Syncs parent and tab scroll.
     */
    private var tabScrollSync: TabScrollSync? = null


    /**
     * Setting attributes.
     */
    init {
        val arr = ctx.obtainStyledAttributes(attrs, R.styleable.RecyclerTabLayout)

        tabsOnScreen = if (resources.configuration.isLandscape()) {
            arr.getInt(R.styleable.RecyclerTabLayout_tabsOnScreenLandscape, DEF_TABS_ON_SCREEN)
        }
        else {
            arr.getInt(R.styleable.RecyclerTabLayout_tabsOnScreenPortrait, DEF_TABS_ON_SCREEN)
        }

        check(tabsOnScreen % 2 != 0) { "tabsOnScreen attribute value must be odd!" }

        tabOffset = tabsOnScreen / 2

        val defColor = ContextCompat.getColor(ctx, R.color.colorPrimary)
        indicator.paint.color = arr.getColor(R.styleable.RecyclerTabLayout_indicatorColor, defColor)
        indicator.enable = arr.getBoolean(R.styleable.RecyclerTabLayout_drawIndicator, DEF_DRAW_INDICATOR)
        indicator.height = arr.getDimension(R.styleable.RecyclerTabLayout_indicatorHeight, 0f)

        arr.recycle()

        initItemWidth()
        initIndicatorSize()
    }

    /**
     * Calculate and pass item width after width of [RecyclerTabLayout] has been set.
     */
    private fun initItemWidth() = post {
        val itemWidth = width / tabsOnScreen
        layoutManager.itemWidth = itemWidth
        tabDecoration.itemWidth = itemWidth
        addItemDecoration(tabDecoration)
    }

    override fun getAdapter(): TabAdapter<*, *> {
        return super.getAdapter() as TabAdapter<*, *>
    }

    override fun setAdapter(adapter: Adapter<*>?) {
        super.setAdapter(adapter)
        this.adapter.setTabOffset(tabOffset)
    }

    /**
     * Pass [LoadingRecyclerViewPager] to sync its scroll with [RecyclerTabLayout].
     * Should be called after [setAdapter].
     */
    fun setUpWithRecyclerViewPager(parent: RecyclerViewPager) {
        check(tabScrollSync == null) { "Already set up." }
        tabScrollSync = TabScrollSync(parent)
    }

    /**
     * Removes scroll sync between [RecyclerTabLayout] and parent [RecyclerViewPager].
     */
    fun clearSetUp() {
        tabScrollSync?.removeParentListeners()
        tabScrollSync = null
    }

    /**
     * Called with visible changes.
     */
    override fun onDraw(c: Canvas?) {
        super.onDraw(c)
        //drawIndicator(c)
    }

    /**
     * Calculate and pass dimensions after width of [RecyclerTabLayout] has been set.
     */
    private fun initIndicatorSize() = post {
        indicator.left = ((tabsOnScreen / 2) * layoutManager.itemWidth).toFloat()
        indicator.right = ((tabsOnScreen / 2 + 1) * layoutManager.itemWidth).toFloat()
        indicator.bottom = bottom.toFloat()
    }

    /**
     * Draw indicator with properties of [indicator] under central child view.
     */
    private fun drawIndicator(c: Canvas?) {
        if (indicator.enable) c?.drawRect(
            indicator.left,
            height - indicator.height,
            indicator.right,
            indicator.bottom,
            indicator.paint
        )
    }

    /**
     * Disables tab scroll without disabling click events.
     */
    override fun onInterceptTouchEvent(e: MotionEvent?) = false


    /**
     * This class syncs parent [LoadingRecyclerViewPager] scroll with [RecyclerTabLayout],
     * also it is listening for on tab click events, that calls scroll to clicked page
     * of parent [LoadingRecyclerViewPager] and centering of clicked tab.
     */
    private inner class TabScrollSync(private val parent: RecyclerViewPager) :
        OnScrollListener(),
        PageListener {

        /**
         * Scroll of [RecyclerTabLayout] from idle to idle stateLiveData in pixels.
         */
        private var scrolledTabX = 0

        /**
         * Scroll of parent [LoadingRecyclerViewPager] from idle to idle stateLiveData in pixels.
         */
        private var scrolledParentX = 0


        init {
            parent.addOnScrollListener(this)
            parent.addPageListener(this)
        }

        fun removeParentListeners() {
            parent.removeOnScrollListener(this)
            parent.removePageListener(this)
        }

        override fun onPageChanged(position: Int) {
            adapter.setIndicatorPosition(position + tabOffset, position, tabsOnScreen)
        }

        /**
         * Parent scroll causes tab scroll.
         */
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            scrolledParentX += dx
            val tabDx = tabScroll(dx)
            scrollBy(tabDx, 0)
            scrolledTabX += tabDx
        }

        /**
         * When parent scroll state is idle and page changed, calculate how much
         * tab scroll is left to properly finish scroll and apply it.
         */
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            if (newState == SCROLL_STATE_IDLE) pagesChanged()?.also { pages ->
                scrollBy(leftScrollOfTabs(pages), 0)
                scrolledParentX = 0
                scrolledTabX = 0
            }
        }

        /**
         * Returns number of pages that was changed by parent scroll,
         * null if parent scroll does not multiply parent item width.
         */
        private fun pagesChanged(): Int? {
            val parentWidth = parent.layoutManager.itemWidth
            val pages = scrolledParentX / parentWidth
            val mod = scrolledParentX % parentWidth
            return if (parentWidth == 0 || mod != 0) null
            else pages
        }

        /**
         * How much tab scroll left to finish page changes.
         */
        private fun leftScrollOfTabs(pages: Int) =
            pages * layoutManager.itemWidth - scrolledTabX

        /**
         * Calculate tab scroll depending on parent scroll.
         */
        private fun tabScroll(parentScroll: Int): Int {
            val parentWidth = parent.layoutManager.itemWidth
            if (parentWidth == 0) return 0
            return (parentScroll.toDouble() / parentWidth * layoutManager.itemWidth).roundToInt()
        }

    }


    /**
     * Indicator properties holder.
     */
    private class Indicator {
        var bottom = 0f
        var left = 0f
        var right = 0f
        var height = 0f
        var enable = false
        val paint = Paint()
    }


    /**
     * Resizing child views of [RecyclerTabLayout].
     */
    private class TabDecoration : RecyclerView.ItemDecoration() {

        /**
         * Width of item view in pixels.
         */
        var itemWidth = 0

        /**
         * Set view width to [itemWidth] value.
         */
        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: State) {
            view.layoutParams.width = itemWidth
        }

    }

}