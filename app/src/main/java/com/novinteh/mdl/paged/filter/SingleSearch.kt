package com.novinteh.mdl.paged.filter

data class SingleSearch<F>(
    var date: String,
    var filter: F
)