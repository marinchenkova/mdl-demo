package com.novinteh.mdl.paged.viewpager.viewholder

import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.novinteh.mdl.R
import com.novinteh.mdl.paged.model.Page
import com.novinteh.mdl.entity.util.DateUtils
import com.novinteh.mdl.paged.model.StringId
import com.novinteh.mdl.paged.model.Tab
import com.novinteh.mdl.view.recycler.util.OnItemClickListener
import com.novinteh.mdl.view.recycler.util.Ref
import com.novinteh.mdl.view.recycler.viewholder.ClickViewHolder
import kotlinx.android.synthetic.main.item_viewpager_tab.view.*
import org.jetbrains.anko.backgroundColorResource
import org.jetbrains.anko.textColor
import org.jetbrains.anko.textColorResource


open class TabViewHolder<I: Tab>(view: View) : ClickViewHolder<I>(view) {

    protected val date: TextView = itemView.data


    override fun placeHolder() {
        date.text = ""
    }

    override fun content(item: I) {
        date.text = DateUtils.ymdToDm(item.id)
    }

    open fun highlight() {
        date.textColorResource = R.color.white95
    }

    open fun unhighlight() {
        date.textColorResource = R.color.white60
    }

}