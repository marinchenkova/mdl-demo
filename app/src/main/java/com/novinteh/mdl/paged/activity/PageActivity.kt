package com.novinteh.mdl.paged.activity

import android.app.Activity
import android.app.ActivityOptions
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.graphics.Paint
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.gson.Gson
import com.novinteh.mdl.R
import com.novinteh.mdl.data.Result
import com.novinteh.mdl.entity.util.DateUtils
import com.novinteh.mdl.main.WorkActivity
import com.novinteh.mdl.paged.model.Page
import com.novinteh.mdl.paged.activity.FilterActivity.Companion.FILTER_JSON
import com.novinteh.mdl.paged.activity.FilterActivity.Companion.REQUEST_FILTER
import com.novinteh.mdl.paged.filter.Filterable
import com.novinteh.mdl.paged.filter.setFilter
import com.novinteh.mdl.paged.model.Tab
import com.novinteh.mdl.paged.viewmodel.PageViewModel
import com.novinteh.mdl.paged.viewpager.RecyclerViewPager
import com.novinteh.mdl.paged.viewpager.adapter.PageAdapter
import com.novinteh.mdl.paged.viewpager.adapter.TabAdapter
import com.novinteh.mdl.paged.viewpager.util.OnTabClickListener
import com.novinteh.mdl.util.*
import com.novinteh.mdl.view.appear
import com.novinteh.mdl.view.recycler.util.OnItemClickListener
import com.novinteh.mdl.view.setContrast
import kotlinx.android.synthetic.main.activity_tabbed_viewpager.*
import kotlinx.coroutines.*
import org.jetbrains.anko.support.v4.onRefresh
import kotlin.math.abs


abstract class PageActivity<I, T: Tab, F, VM: PageViewModel<I, T, F>> : WorkActivity(),
    Filterable<F>,
    RecyclerViewPager.PageListener,
    OnTabClickListener,
    OnItemClickListener {


    private var pageSize = 1
    private var prefetchDistance = 0
    private var tabOffset = 0

    protected abstract val refreshRate: RefreshRate

    protected lateinit var viewModel: VM
    protected abstract val viewModelClass: Class<out VM>

    protected abstract val titleRes: Int

    protected abstract val pageAdapter: PageAdapter<I, *>
    protected abstract val tabAdapter: TabAdapter<T, *>

    protected abstract val filterActivityClass: Class<out FilterActivity<F, *>>

    private val refreshJob = RefreshJob(
        CoroutineScope(Dispatchers.Default),
        safeDelayMillis = 1000
    )


    override fun onCreate(savedInstanceState: Bundle?) {
        // Transitions
        //setEnterSharedElementCallback(MaterialContainerTransformSharedElementCallback())
        //setExitSharedElementCallback(MaterialContainerTransformSharedElementCallback())
        //window.sharedElementsUseOverlay = false

        super.onCreate(savedInstanceState)
        log("onCreate")
        setContentView(R.layout.activity_tabbed_viewpager)

        val initDate = getMainDate()

        initToolbar(getString(titleRes))
        initViewModel(initDate)
        initFilter()
        initTabs()
        initPager()
        initState()
        initData(initDate)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_FILTER -> if (resultCode == Activity.RESULT_OK) {
                getFilterFromJson(data?.getStringExtra(FILTER_JSON))?.also { setFilter(it) }
            }

            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    protected abstract fun getFilterFromJson(json: String?): F?

    private fun convertFilterToJson(value: F) = Gson().toJson(value)

    override fun onStart() {
        super.onStart()
        log("onStart")
        setUpListeners()
    }

    override fun onResume() {
        super.onResume()
        log("onResume")
    }

    override fun onPause() {
        log("onPause")
        super.onPause()
    }

    override fun onStop() {
        super.onStop()
        log("onStop")
        removeListeners()
    }

    override fun onDestroy() {
        super.onDestroy()
        log("onDestroy")
    }

    private fun setUpListeners() {
        recycler_tabs.setUpWithRecyclerViewPager(recycler_pager)
        recycler_pager.addPageListener(this)
        pageAdapter.setOnSubItemClickListener(this)
        tabAdapter.setOnTabClickListener(this)
    }

    private fun removeListeners() {
        recycler_tabs.clearSetUp()
        recycler_pager.removePageListener(this)
        pageAdapter.removeOnSubItemClickListener()
        tabAdapter.removeOnTabClickListener()
    }

    private fun initViewModel(date: String) {
        viewModel = ViewModelProvider(this).get(viewModelClass)
        onInitViewModel(date)
    }

    protected open fun onInitViewModel(date: String) {}

    private fun initFilter() {
        fab.setSafeClickListener { launchFilterActivity() }
        onInitFilter()
    }

    private fun launchFilterActivity() {
        val intent = Intent(this, filterActivityClass).apply {
            putExtra(FILTER_JSON, convertFilterToJson(filter))
            putExtra(DATE, getMainDate())
        }
/*
        val options = ActivityOptions.makeSceneTransitionAnimation(
            this,
            fab,
            "filter"
        )
        startActivityForResult(intent, REQUEST_FILTER, options.toBundle())
*/
        startActivityForResult(intent, REQUEST_FILTER)
    }

    protected open fun onInitFilter() {}

    override fun onFilterChanged(value: F) {
        //log("onFilterChanged to $value")
        viewModel.search { filter = value }
    }

    private fun initTabs() {
        tabAdapter.setHasStableIds(true)

        pageSize = recycler_tabs.tabsOnScreen
        tabOffset = recycler_tabs.tabsOnScreen / 2

        recycler_tabs.setHasFixedSize(true)
        recycler_tabs.itemAnimator = null
        recycler_tabs.adapter = tabAdapter
    }

    protected open fun initPager() {
        pageAdapter.setHasStableIds(true)

        prefetchDistance = pageSize - 1

        recycler_pager.setHasFixedSize(true)
        recycler_pager.itemAnimator = null
        recycler_pager.adapter = pageAdapter
    }

    private fun initState() {
        lifecycle.addObserver(refreshJob)

        swipe_refresh_layout.apply {
            setColorSchemeResources(R.color.colorPrimary)

            setOnChildScrollUpCallback { _, _ -> recycler_pager.canScrollUp(R.id.recycler_data) }

            onRefresh(refreshJob::launch)
        }

        refreshJob.apply {
            setRefreshRate(refreshRate)

            onRefresh { viewModel.refresh() }

            onUpdateState { result -> when (result) {
                is Result.Success -> showSuccess()
                is Result.Loading -> showLoading()
                is Result.Error -> showError(pickErrorMessage(result))
            }}
        }

        viewModel.state.observe(this, Observer { state ->
            refreshJob.setLastState(state)
        })

        viewModel.connect.observe(this, Observer { networkIsStable ->
            refreshJob.setNetworkIsStable(networkIsStable)
        })
    }

    private fun showSuccess() {
        val defaultPaint = Paint().apply {
            colorFilter = ColorMatrixColorFilter(ColorMatrix())
        }
        recycler_pager.setLayerType(View.LAYER_TYPE_HARDWARE, defaultPaint)
        text_error.text = ""
        showLoaded()
        text_error.appear(false)
    }

    private fun showLoading() {
        swipe_refresh_layout.isRefreshing = true
    }

    private fun showLoaded() {
        swipe_refresh_layout.isRefreshing = false
    }

    private fun showError(msg: String) {
        val grayPaint = Paint().apply {
            colorFilter = ColorMatrixColorFilter(ColorMatrix().apply {
                setContrast(0.5f)
                setSaturation(0f)
            })
        }
        recycler_pager.setLayerType(View.LAYER_TYPE_HARDWARE, grayPaint)
        text_error.text = msg
        showLoaded()
        text_error.appear(true)
    }

    private fun initData(date: String) {
        onLoadInit(date)

        viewModel.pages.observe(this, Observer { list ->
            //log("submitData pages $list")
            pageAdapter.submitList(list)
        })

        viewModel.tabs.observe(this, Observer { list ->
            //log("submitData tabs $list")
            tabAdapter.submitList(list)
        })
    }

    private fun onLoadInit(date: String) {
        val dates = DateUtils.daysFrom(date, pageSize)
        val tabs = dates.map { buildEmptyTab(it) }
        val pages = dates.map { Page<I>(it) }

        log("onLoadInit on $date, initDates $dates")

        if (tabAdapter.getCurrentList().isNotEmpty() || pageAdapter.getCurrentList().isNotEmpty()) {
            tabAdapter.submitList(null) { tabAdapter.submitList(tabs) }
            pageAdapter.submitList(null) { pageAdapter.submitList(pages) }
        }
        else {
            tabAdapter.submitList(tabs)
            pageAdapter.submitList(pages)
        }

        viewModel.initSearch(dates, filter, maxRange = pageSize * 3)
        viewModel.loadIfAbsent(dates)
    }

    protected abstract fun buildEmptyTab(id: String): T

    private fun onLoadBefore(date: String) {
        val dates = DateUtils.daysFrom(date, pageSize, reversed = true)

        log("loadBefore $date, dates $dates")

        viewModel.search { addDatesBefore(dates) }
        viewModel.loadIfAbsent(dates)
    }

    private fun onLoadAfter(date: String) {
        val dates = DateUtils.daysFrom(date, pageSize, 1)

        log("loadAfter $date, dates $dates")

        viewModel.search { addDatesAfter(dates) }
        viewModel.loadIfAbsent(dates)
    }

    /**
     * User clicks on entity in the current date list to view getDetails.
     */
    override fun onItemClick(position: Int) {
        viewDetails(position, getMainDate())
    }

    protected abstract fun viewDetails(initialPosition: Int, date: String)

    /**
     * User clicks on tab to switch to some date.
     */
    override fun onTabClick(fromPosition: Int, toPosition: Int) {
        if (fromPosition == toPosition) showDatePicker()
        else scrollBy(toPosition - fromPosition)
    }

    override fun onPageChanged(position: Int) {
        val date = pageAdapter.getPageId(position) ?: return
        checkLoadMore(position)
        onDateChanged(date)
    }

    protected open fun onDateChanged(date: String) {
        setMainDate(date)
        if (fab.visibility != View.VISIBLE) fab.show()
    }

    private fun checkLoadMore(position: Int) {
        val lastPosition = pageAdapter.getItemCount() - 1
        if (lastPosition - position < prefetchDistance) {
            pageAdapter.getPageId(lastPosition)?.also { date ->
                onLoadAfter(date)
            }
        }

        if (position < prefetchDistance) {
            pageAdapter.getPageId(0)?.also { date ->
                onLoadBefore(date)
            }
        }
    }

    private fun scrollBy(positions: Int) {
        recycler_pager.post {
            recycler_pager.scrollBy(
                positions,
                smooth = abs(positions) < recycler_tabs.tabsOnScreen
            )
        }
    }

    private fun showDatePicker() {
        val currentDate = getMainDate()
        val datePick = DateUtils.DatePick(currentDate)
        DatePickerDialog(
            this,
            { _, year, month, day ->
                val newDate = DateUtils.DatePick(year, month, day).ymd
                if (newDate != currentDate) reload(currentDate, newDate)
            },
            datePick.year,
            datePick.month,
            datePick.day
        ).show()
    }

    private fun reload(fromDate: String, toDate: String) {
        if (viewModel.searchDates.containsItemInBounds(toDate, tabOffset)) {
            val diff = DateUtils.diffDaysYmd(fromDate, toDate)
            scrollBy(diff)
            return
        }

        onLoadInit(toDate)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.prod_type, menu)
        menuInflater.inflate(R.menu.refresh, menu)
        menuInflater.inflate(R.menu.set_date, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.menu_set_date -> {
            showDatePicker()
            true
        }

        R.id.menu_refresh -> {
            refreshJob.launch()
            true
        }

        else -> super.onOptionsItemSelected(item)
    }


    companion object {
        const val DETAILS_LIST_INIT_POSITION = "details_list_init_position"
        const val DETAILS_LIST = "details_list"
        const val DATE = "date"
    }

}