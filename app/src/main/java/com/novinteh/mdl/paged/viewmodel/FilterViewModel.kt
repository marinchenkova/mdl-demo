package com.novinteh.mdl.paged.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import com.novinteh.mdl.regimes.common.model.LoadStateInfo
import com.novinteh.mdl.rest.ConnectionLiveData


abstract class FilterViewModel(app: Application) : ScopeViewModel(app) {

    val connect = ConnectionLiveData.from(app)
    abstract val state: LiveData<LoadStateInfo>


    abstract fun refresh()

}