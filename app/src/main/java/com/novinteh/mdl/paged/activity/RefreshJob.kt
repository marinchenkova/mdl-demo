package com.novinteh.mdl.paged.activity

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.novinteh.mdl.data.Result
import com.novinteh.mdl.regimes.common.model.LoadStateInfo
import com.novinteh.mdl.util.RefreshRate
import com.novinteh.mdl.util.TimerJob
import kotlinx.coroutines.CoroutineScope

class RefreshJob(
    scope: CoroutineScope,
    safeDelayMillis: Long
) : LifecycleObserver {

    private var onUpdateState: (result: Result) -> Unit = {}

    private val timerJob = TimerJob(scope, safeDelayMillis)
    private val refreshRate = RefreshRate(0, 0, 30)

    private var networkIsStable: Boolean? = null
    private var lastState: LoadStateInfo? = null

    private var started = false
    private var startLaunchDone = false


    fun launch() {
        timerJob.launch()
    }

    fun onRefresh(block: () -> Unit) {
        timerJob.action = block
    }

    fun onUpdateState(block: (Result) -> Unit) {
        onUpdateState = block
    }

    fun setRefreshRate(value: RefreshRate) {
        refreshRate.setFrom(value)
    }

    fun setNetworkIsStable(value: Boolean?) {
        if (networkIsStable != value) {
            networkIsStable = value
            onNetworkStateChanged(lastState?.lastResult, value)
        }
    }

    fun setLastState(value: LoadStateInfo?) {
        if (lastState != value) {
            lastState = value
            updateState(value)
        }
    }

    private fun onNetworkStateChanged(lastResult: Result?, networkIsStable: Boolean?) {
        if (networkIsStable == null) return

        checkStartLaunch(networkIsStable)

        when {
            lastResult is Result.Loading && !networkIsStable -> {
                timerJob.cancel()
                onUpdateState(Result.Error.Client())
            }

            lastResult is Result.Error.Client && networkIsStable -> {
                timerJob.launch()
            }
        }
    }

    private fun updateState(state: LoadStateInfo?) {
        when (val result = state?.lastResult) {
            is Result.Loading,
            is Result.Error -> onUpdateState(result)

            is Result.Success -> {
                onUpdateState(result)
                val refreshIn = state.timestamp.millisTillNextInvokeOf(refreshRate)
                timerJob.trySchedule(refreshIn)
            }

        }
    }

    private fun checkStartLaunch(networkIsStable: Boolean) {
        if (started && networkIsStable && !startLaunchDone) {
            timerJob.trySchedule()
            startLaunchDone = true
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private fun start() {
        started = true
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private fun stop() {
        started = false
        startLaunchDone = false
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private fun cancel() {
        timerJob.cancel()
    }

}