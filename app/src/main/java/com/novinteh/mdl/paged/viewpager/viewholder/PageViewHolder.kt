package com.novinteh.mdl.paged.viewpager.viewholder

import android.view.MotionEvent
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.novinteh.mdl.paged.model.Page
import com.novinteh.mdl.view.recycler.adapter.ClickAdapter
import com.novinteh.mdl.view.recycler.util.OnItemClickListener
import com.novinteh.mdl.view.recycler.util.Ref
import com.novinteh.mdl.view.recycler.viewholder.BindViewHolder
import com.novinteh.mdl.view.show
import kotlinx.android.synthetic.main.item_viewpager_page.view.*


abstract class PageViewHolder<I>(view: View) : BindViewHolder<Page<I>>(view) {

    private val noDataText = itemView.no_data_text
    private val recycler: RecyclerView = itemView.recycler_data

    protected abstract val subAdapter: ClickAdapter<I, *>


    init {
        val manager = LinearLayoutManager(itemView.context, LinearLayoutManager.VERTICAL, false)
        manager.initialPrefetchItemCount = 10
        recycler.layoutManager = manager
        recycler.itemAnimator = null
        recycler.setHasFixedSize(true)
        //recycler.setItemViewCacheSize(10)
    }

    fun init(
        onSubItemClickListener: Ref<OnItemClickListener>,
        recycledViewPool: RecyclerView.RecycledViewPool
    ) {
        subAdapter.setHasStableIds(true)
        recycler.adapter = subAdapter
        recycler.setRecycledViewPool(recycledViewPool)
        subAdapter.setOnItemClickListener(object : OnItemClickListener {
            override fun onItemClick(position: Int) {
                onSubItemClickListener.get()?.onItemClick(position)
            }
        })
    }

    override fun content(item: Page<I>) {
        subAdapter.submitList(item.list)
        noDataText.show(item.list.isEmpty())
    }

}