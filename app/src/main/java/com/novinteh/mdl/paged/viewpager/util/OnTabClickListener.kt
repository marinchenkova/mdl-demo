package com.novinteh.mdl.paged.viewpager.util

interface OnTabClickListener {

    fun onTabClick(fromPosition: Int, toPosition: Int)

}