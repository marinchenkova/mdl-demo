package com.novinteh.mdl.paged.activity

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.novinteh.mdl.R
import com.novinteh.mdl.entity.util.DateUtils
import com.novinteh.mdl.main.ToolbarActivity
import com.novinteh.mdl.paged.viewmodel.DetailsViewModel
import com.novinteh.mdl.paged.viewpager.RecyclerViewPager
import com.novinteh.mdl.view.recycler.adapter.ItemAdapter
import kotlinx.android.synthetic.main.activity_details.*


abstract class DetailsActivity<I> : ToolbarActivity(), RecyclerViewPager.PageListener {

    private lateinit var viewModel: DetailsViewModel
    protected abstract val adapter: ItemAdapter<I, *>
    protected abstract val titleRes: Int


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        val date = intent.getStringExtra(PageActivity.DATE)
        initToolbar(getString(titleRes), DateUtils.ymdToDmy(date))
        initViewModel()

        val intentPosition = intent.getIntExtra(PageActivity.DETAILS_LIST_INIT_POSITION, 0)
        val initialPosition = viewModel.recyclerPosition ?: intentPosition
        val list = getDetailsFromJson(intent.getStringExtra(PageActivity.DETAILS_LIST)) ?: return

        initData(initialPosition, list)
    }

    protected abstract fun getDetailsFromJson(json: String?): List<I>?

    private fun initViewModel() {
        viewModel = ViewModelProvider(this).get(DetailsViewModel::class.java)
    }

    private fun initData(initialPosition: Int, list: List<I>) {
        recycler_data.itemAnimator = null
        recycler_data.adapter = adapter
        adapter.submitList(list)
        recycler_data.post { recycler_data.scrollToPosition(initialPosition) }
    }

    override fun onPageChanged(position: Int) {
        viewModel.recyclerPosition = position
    }

}
