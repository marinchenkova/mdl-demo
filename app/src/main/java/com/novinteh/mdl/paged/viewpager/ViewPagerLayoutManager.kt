package com.novinteh.mdl.paged.viewpager

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


/**
 * Layout manager for [RecyclerView] with horizontal orientation
 * that determines how many getDetails should be visible on screen.
 */
class ViewPagerLayoutManager(ctx: Context) : LinearLayoutManager(ctx, HORIZONTAL, false) {

    /**
     * Width of item view in pixels.
     */
    var itemWidth = 0

    /**
     * Listens to layout completion.
     */
    var onLayoutCompletedListener: OnLayoutCompletedListener? = null


    /**
     * Here we are listening to layout completion to insert check page
     * of [LoadingRecyclerViewPager] in ymd.
     */
    override fun onLayoutCompleted(state: RecyclerView.State?) {
        super.onLayoutCompleted(state)
        onLayoutCompletedListener?.onLayoutCompleted()
    }

    /**
     * Disable predictive animations. There is a bug in [RecyclerView] which causes views that
     * are being reloaded to pull invalid ViewHolders from the internal recycler stack if the
     * subAdapter size has decreased since the ShipmentViewHolder was recycled.
     */
    override fun supportsPredictiveItemAnimations(): Boolean {
        return false
    }

    /**
     * Layout completion listener.
     */
    interface OnLayoutCompletedListener {
        fun onLayoutCompleted()
    }

}