package com.novinteh.mdl.paged.viewpager

import android.content.Context
import android.util.AttributeSet
import androidx.annotation.IdRes
import androidx.viewpager.widget.ViewPager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.novinteh.mdl.paged.viewpager.ViewPagerLayoutManager.OnLayoutCompletedListener


/**
 * Horizontal oriented [RecyclerView] that is acting like a [ViewPager].
 */
open class RecyclerViewPager @JvmOverloads constructor(
    ctx: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : RecyclerView(ctx, attrs, defStyleAttr) {

    private val pageChangesNotifier = PageChangesNotifier()
    private val pageListeners = mutableListOf<PageListener>()
    private val pagerSnapHelper = PagerSnapHelperListenable()


    init {
        initLayoutManager()
        initPager()
    }

    private fun initPager() {
        pagerSnapHelper.attachToRecyclerView(this)
        addOnScrollListener(pageChangesNotifier)
    }

    private fun initLayoutManager() {
        layoutManager = ViewPagerLayoutManager(context)
        post { layoutManager.itemWidth = width }
        layoutManager.onLayoutCompletedListener = pageChangesNotifier
    }

    override fun getLayoutManager(): ViewPagerLayoutManager {
        return super.getLayoutManager() as ViewPagerLayoutManager
    }

    /**
     * Listen to this [RecyclerViewPager] selected item position changed.
     */
    fun addPageListener(listener: PageListener) {
        pageListeners.add(listener)
    }

    fun removePageListener(listener: PageListener) {
        pageListeners.remove(listener)
    }

    /**
     * Scroll for [positions] starting at current.
     */
    fun scrollBy(positions: Int, smooth: Boolean = false) {
        if (smooth) smoothScrollBy(positions * layoutManager.itemWidth, 0)
        else scrollBy(positions * layoutManager.itemWidth, 0)
    }

    fun canScrollUp(@IdRes childRecyclerViewId: Int): Boolean {
        val view =  layoutManager.findViewByPosition(pageChangesNotifier.selectedPosition)
        val scrollable = view?.findViewById<RecyclerView>(childRecyclerViewId)
        return scrollable?.canScrollVertically(-1) ?: false
    }


    interface PageListener {
        fun onPageChanged(position: Int)
    }


    /**
     * This class listens to scroll and notifies [pageListeners] when selected page changes, also
     * it notifies adapter if [RecyclerViewPager] needs more data.
     */
    private inner class PageChangesNotifier : OnScrollListener(), OnLayoutCompletedListener {

        var selectedPosition = NO_POSITION


        /**
         * Notify if selected page changes on scroll and check if [RecyclerViewPager] needs
         * more pages.
         */
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            notifyMaybePageChanged()
        }

        /**
         * Notify if selected page changes after [ViewPagerLayoutManager.onLayoutCompleted] and
         * check if total item count has been changed.
         */
        override fun onLayoutCompleted() {
            notifyMaybePageChanged()
        }

        private fun notifyMaybePageChanged() {
            val current = pagerSnapHelper.getSnapPosition()
            if (current != selectedPosition) {
                selectedPosition = current
                notifyPageChanged(current)
            }
        }

        private fun notifyPageChanged(position: Int) {
            pageListeners.forEach { listener -> listener.onPageChanged(position) }
        }

    }


    /**
     * Addon to [PagerSnapHelper] for retrieving snap position as selected item position.
     */
    private inner class PagerSnapHelperListenable : PagerSnapHelper() {

        /**
         * Snap view is view that [pagerSnapHelper] is targeting to when scrolls.
         * It's position can be interpret as selected item position.
         * Returns NO_POSITION == -1 if position can not be found.
         */
        fun getSnapPosition(): Int {
            val snapView = findSnapView(layoutManager) ?: return NO_POSITION
            return layoutManager.getPosition(snapView)
        }


    }

}