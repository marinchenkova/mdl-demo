package com.novinteh.mdl.paged.model

import androidx.recyclerview.widget.DiffUtil

data class Page<out I>(val id: String, val list: List<I> = emptyList()) {

    companion object {

        fun <I> getDiffCallback() = object : DiffUtil.ItemCallback<Page<I>>() {
            override fun areItemsTheSame(oldItem: Page<I>, newItem: Page<I>): Boolean {
                return oldItem.id == newItem.id
            }
            override fun areContentsTheSame(oldItem: Page<I>, newItem: Page<I>): Boolean {
                return oldItem == newItem
            }
        }

    }

}