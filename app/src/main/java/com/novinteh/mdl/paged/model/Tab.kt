package com.novinteh.mdl.paged.model

import androidx.recyclerview.widget.DiffUtil

open class Tab(override val id: String) : StringId(id) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Tab) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    override fun toString(): String {
        return "Tab($id)"
    }

    companion object {

        val DiffCallback = object : DiffUtil.ItemCallback<Tab>() {
            override fun areItemsTheSame(oldItem: Tab, newItem: Tab): Boolean {
                return oldItem.id == newItem.id
            }
            override fun areContentsTheSame(oldItem: Tab, newItem: Tab): Boolean {
                return oldItem == newItem
            }
        }

    }

}