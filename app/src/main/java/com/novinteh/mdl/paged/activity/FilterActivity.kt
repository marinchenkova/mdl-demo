package com.novinteh.mdl.paged.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.Window
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.transition.platform.MaterialContainerTransform
import com.google.android.material.transition.platform.MaterialContainerTransformSharedElementCallback
import com.google.gson.Gson
import com.novinteh.mdl.R
import com.novinteh.mdl.data.Result
import com.novinteh.mdl.entity.util.DateUtils
import com.novinteh.mdl.main.ReceiverActivity
import com.novinteh.mdl.paged.filter.Filterable
import com.novinteh.mdl.paged.filter.setFilter
import com.novinteh.mdl.paged.viewmodel.FilterViewModel
import com.novinteh.mdl.paged.viewmodel.ScopeViewModel
import com.novinteh.mdl.regimes.regChanges.filter.RegChangesFilterViewModel
import com.novinteh.mdl.util.pickErrorMessage
import com.novinteh.mdl.view.show
import kotlinx.android.synthetic.main.activity_filter.*

abstract class FilterActivity<F, VM: FilterViewModel> : ReceiverActivity(), Filterable<F> {

    protected lateinit var viewModel: VM
    protected abstract val viewModelClass: Class<out VM>


    override fun onCreate(savedInstanceState: Bundle?) {
        // Transitions
/*
        setEnterSharedElementCallback(MaterialContainerTransformSharedElementCallback())
        setExitSharedElementCallback(MaterialContainerTransformSharedElementCallback())

        window.sharedElementEnterTransition = MaterialContainerTransform().apply {
            addTarget(R.id.activity_filter_root)
            duration = 300L
        }

        window.sharedElementReturnTransition = MaterialContainerTransform().apply {
            addTarget(R.id.activity_filter_root)
            duration = 250L
        }
*/
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter)
        
        val date = intent.getStringExtra(PageActivity.DATE)
        initToolbar(getString(R.string.shift_filter_title), DateUtils.ymdToDmy(date))
        initViewModel()
        initState()
        getFilterFromJson(intent.getStringExtra(FILTER_JSON))?.also { setFilter(it) }
        onDateSet(date)
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(this).get(viewModelClass)
    }

    protected abstract fun onDateSet(date: String)

    protected abstract fun getFilterFromJson(json: String?): F?

    private fun filterJson(filter: F): String = Gson().toJson(filter)

    protected abstract fun saveFilter(): F?

    private fun returnResultOk() {
        val saved = saveFilter()
        if (saved != null) {
            log("Return result ok: $filter")
            setResult(Activity.RESULT_OK, Intent().apply { putExtra(FILTER_JSON, filterJson(saved)) })
            //supportFinishAfterTransition()
            finish()
        }
        else returnResultCancelled()
    }

    private fun returnResultCancelled() {
        log("Return result cancelled")
        setResult(Activity.RESULT_CANCELED)

        //supportFinishAfterTransition()
        finish()
    }

    protected abstract fun isContentLoaded(): Boolean

    private fun initState() {
        var lastResult: Result? = null
        viewModel.state.observe(this, Observer { state ->
            lastResult = state?.lastResult
            updateState(lastResult)
        })

        viewModel.connect.observe(this, Observer { networkIsStable ->
            val canRefresh = !isContentLoaded()
                    && networkIsStable
                    && (lastResult is Result.Error || lastResult == null)

            if (canRefresh) viewModel.refresh()
        })
    }

    private fun updateState(result: Result?) {
        if (isContentLoaded()) return
        when (result) {
            is Result.Success -> showSuccess()
            is Result.Loading -> if (!isContentLoaded()) showLoading()
            is Result.Error -> if (!isContentLoaded()) showError(pickErrorMessage(result))
        }
    }

    private fun showSuccess() {
        text_error.text = ""
        progress_bar.show(false)
    }

    private fun showLoading() {
        text_error.text = ""
        progress_bar.show(true)
    }

    private fun showError(msg: String) {
        text_error.text = msg
        progress_bar.show(false)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.apply, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.menu_apply -> {
            returnResultOk()
            true
        }

        else -> super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        returnResultCancelled()
    }


    companion object {
        const val FILTER_JSON = "filter_json"
        const val REQUEST_FILTER = 0
    }

}
