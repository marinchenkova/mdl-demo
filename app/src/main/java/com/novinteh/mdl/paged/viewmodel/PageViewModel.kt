package com.novinteh.mdl.paged.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.novinteh.mdl.data.repo.AsyncRepository
import com.novinteh.mdl.paged.model.Page
import com.novinteh.mdl.paged.filter.RangeSearch
import com.novinteh.mdl.paged.model.Tab
import com.novinteh.mdl.regimes.common.model.LoadStateInfo
import com.novinteh.mdl.util.Timestamp
import com.novinteh.mdl.rest.ConnectionLiveData

abstract class PageViewModel<I, T: Tab, F>(app: Application) : ScopeViewModel(app) {

    protected val search = MutableLiveData<RangeSearch<F>>()
    val searchDates get() = search.value?.currentDates ?: emptyList()

    protected abstract val repo: AsyncRepository

    abstract val tabs: LiveData<List<T>>
    abstract val pages: LiveData<List<Page<I>>>
    abstract val state: LiveData<LoadStateInfo>

    val connect = ConnectionLiveData.from(app)


    fun initSearch(dates: List<String>, filter: F, maxRange: Int) {
        search.value = RangeSearch(dates, filter, maxRange)
    }

    fun search(changes: RangeSearch<F>.() -> Unit) {
        search.value = search.value?.apply(changes)
    }

    abstract fun refresh(dates: List<String> = searchDates, timestamp: Timestamp = Timestamp.now())

    abstract fun loadIfAbsent(dates: List<String>, timestamp: Timestamp = Timestamp.now())

}