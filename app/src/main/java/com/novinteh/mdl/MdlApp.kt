package com.novinteh.mdl

import android.app.Application
import android.content.Context
import com.novinteh.mdl.rest.RequestService

class MdlApp : Application() {

    override fun onCreate() {
        super.onCreate()
        RequestService.init(this)
    }

}
