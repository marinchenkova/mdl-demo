package com.novinteh.mdl.util

data class RefreshRate(
    var hours: Long,
    var minutes: Long,
    var seconds: Long
) {

    fun millis(): Long = (seconds + (minutes + hours * 60) * 60) * 1000

    fun setFrom(other: RefreshRate) {
        hours = other.hours
        minutes = other.minutes
        seconds = other.seconds
    }

}