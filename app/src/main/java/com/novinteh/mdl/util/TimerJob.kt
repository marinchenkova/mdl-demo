package com.novinteh.mdl.util

import kotlinx.coroutines.*

open class TimerJob(
    private val scope: CoroutineScope,
    private val safeDelayMillis: Long
) {

    var action: () -> Unit = {}

    private var lastJobStartMillis: Long = 0
    private var nextActionInMillis: Long = 0
    private var job: Job? = null


    fun trySchedule(delayMillis: Long = safeDelayMillis) {
        if (job?.isActive == true && delayMillis < millisTillNextAction()) return
        schedule(if (delayMillis < safeDelayMillis) safeDelayMillis else delayMillis)
    }

    fun launch() {
        schedule(0)
    }

    private fun schedule(delayMillis: Long) {
        nextActionInMillis = delayMillis
        job?.cancel()

        lastJobStartMillis = System.currentTimeMillis()
        job = scope.launch {
            delay(nextActionInMillis)
            action()
        }
    }

    fun cancel() {
        job?.cancel()
        job = null
    }

    private fun millisTillNextAction(): Long =
        nextActionInMillis - (System.currentTimeMillis() - lastJobStartMillis)

    override fun toString(): String {
        val isActive = job?.isActive == true
        return "TimerJob(isActive=${isActive}${if (isActive) ", millisTillNextAction=${millisTillNextAction()}" else ""})"
    }

}