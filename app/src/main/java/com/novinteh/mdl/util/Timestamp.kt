package com.novinteh.mdl.util

import com.novinteh.mdl.entity.util.DateUtils
import java.text.SimpleDateFormat
import java.util.*

data class Timestamp(val ymdHms: String) {

    fun millisTillNextInvokeOf(refreshRate: RefreshRate): Long {
        val last = format.parse(ymdHms)
        val now = DateUtils.today()

        val passed = now.time - last.time
        val left = refreshRate.millis() - passed

        return if (left > 0) left else 0
    }

    override fun toString(): String {
        return ymdHms
    }


    companion object {

        private val format = SimpleDateFormat("yyyy.MM.dd HH:mm:ss")

        fun now(): Timestamp = fromMillis(DateUtils.today().time)

        private fun fromMillis(millis: Long): Timestamp {
            val cal = Calendar.getInstance().apply { time.time = millis }
            val ymdHms = format.format(cal.time)
            return Timestamp(ymdHms)
        }

    }

}