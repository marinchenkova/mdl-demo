package com.novinteh.mdl.util

import android.content.Context
import android.content.SharedPreferences
import com.novinteh.mdl.entity.data.common.ProdType
import com.novinteh.mdl.entity.util.DateUtils
import com.novinteh.mdl.login.server.Server
import com.novinteh.mdl.util.SharedPrefs.PREFS_AOMN_ID
import com.novinteh.mdl.util.SharedPrefs.PREFS_LOGGED
import com.novinteh.mdl.util.SharedPrefs.PREFS_LOGIN_CREDS
import com.novinteh.mdl.util.SharedPrefs.PREFS_LOGIN_PASS
import com.novinteh.mdl.util.SharedPrefs.PREFS_LOGIN_USER
import com.novinteh.mdl.util.SharedPrefs.PREFS_MAIN_DATE
import com.novinteh.mdl.util.SharedPrefs.PREFS_PORT_ID
import com.novinteh.mdl.util.SharedPrefs.PREFS_PROD_TYPE
import com.novinteh.mdl.util.SharedPrefs.PREFS_RELAUNCHED
import com.novinteh.mdl.util.SharedPrefs.PREFS_SERVERS
import com.novinteh.mdl.util.SharedPrefs.PREFS_SELECTED_SERVER
import com.novinteh.mdl.util.SharedPrefs.PREFS_TIMESTAMP
import com.novinteh.mdl.util.SharedPrefs.PREFS_TODAY
import com.novinteh.mdl.util.SharedPrefs.fromContext


fun Context.setAppRelaunched(relaunched: Boolean) = fromContext(this)
    .edit()
    .putBoolean(PREFS_RELAUNCHED, relaunched)
    .apply()

fun Context.isAppRelaunched(): Boolean = fromContext(this)
    .getBoolean(PREFS_RELAUNCHED, false)



fun Context.setLogged(logged: Boolean) = fromContext(this)
    .edit()
    .putBoolean(PREFS_LOGGED, logged)
    .apply()

fun Context.isLogged(): Boolean = fromContext(this)
    .getBoolean(PREFS_LOGGED, false)


fun Context.setPass(pass: String) = fromContext(this)
    .edit()
    .putString(PREFS_LOGIN_PASS, pass)
    .apply()

fun Context.getPass(): String? = fromContext(this)
    .getString(PREFS_LOGIN_PASS, null)


fun Context.setUser(user: String) = fromContext(this)
    .edit()
    .putString(PREFS_LOGIN_USER, user)
    .apply()

fun Context.getUser(): String? = fromContext(this)
    .getString(PREFS_LOGIN_USER, null)


fun Context.setCredentials(basic: String) = fromContext(this)
    .edit()
    .putString(PREFS_LOGIN_CREDS, basic)
    .apply()

fun Context.getCredentials(): String? = fromContext(this)
    .getString(PREFS_LOGIN_CREDS, null)



fun Context.getSelectedServer(): Server {
    val data = fromContext(this).getString(PREFS_SELECTED_SERVER, "1${Server.DefaultUrl}")
        ?: "1${Server.DefaultUrl}"

    val result = if (data.isBlank()) "1${Server.DefaultUrl}" else data
    return Server.fromUrl(this, result)
}

fun Context.setSelectedServer(server: Server) = fromContext(this)
    .edit()
    .putString(PREFS_SELECTED_SERVER, server.checkUrl())
    .apply()

fun Context.getServers(): List<Server> {
    val urls = fromContext(this).getStringSet(PREFS_SERVERS, emptySet()) ?: emptySet()
    return Server.fromUrls(this, urls)
}

fun Context.setServers(servers: Iterable<Server>) = fromContext(this)
    .edit()
    .putStringSet(PREFS_SERVERS, Server.toUrls(servers))
    .apply()



fun Context.getProdType(): ProdType = ProdType.fromString(
    fromContext(this)
        .getString(PREFS_PROD_TYPE, ProdType.OIL.name)
        ?: ProdType.OIL.name
)

fun Context.setProdType(prodType: ProdType) = fromContext(this)
    .edit()
    .putString(PREFS_PROD_TYPE, prodType.name)
    .apply()


fun Context.getMainDate(): String = fromContext(this)
    .getString(PREFS_MAIN_DATE, DateUtils.todayFormatYmd())
    ?: DateUtils.todayFormatYmd()

fun Context.setMainDate(date: String) = fromContext(this)
    .edit()
    .putString(PREFS_MAIN_DATE, date)
    .apply()


fun Context.getToday(): String = fromContext(this)
    .getString(PREFS_TODAY, DateUtils.todayFormatYmd())
    ?: DateUtils.todayFormatYmd()

fun Context.setToday(date: String) = fromContext(this)
    .edit()
    .putString(PREFS_TODAY, date)
    .apply()



fun Context.setAomnId(id: Int) = fromContext(this)
    .edit()
    .putInt(PREFS_AOMN_ID, id)
    .apply()

fun Context.getAomnId(): Int = fromContext(this)
    .getInt(PREFS_AOMN_ID, -1)


fun Context.setPortId(id: Int) = fromContext(this)
    .edit()
    .putInt(PREFS_PORT_ID, id)
    .apply()

fun Context.getPortId(): Int = fromContext(this)
    .getInt(PREFS_PORT_ID, -1)


fun Context.getNextTimestamp(): String {
    val prefs = fromContext(this)
    val next = prefs.getInt(PREFS_TIMESTAMP, 0)
    val update = if (next < 100) next + 1 else 0
    prefs.edit().putInt(PREFS_TIMESTAMP, update).apply()
    return next.toString()
}


object SharedPrefs {

    private const val PREFS_FILENAME = "com.novinteh.mdl.sharedPrefs"

    const val PREFS_LOGGED = "prefs_logged"
    const val PREFS_RELAUNCHED = "prefs_relaunched"
    const val PREFS_LOGIN_CREDS = "prefs_login_creds"
    const val PREFS_LOGIN_USER = "prefs_login_user"
    const val PREFS_LOGIN_PASS = "prefs_login_pwd"
    const val PREFS_MAIN_DATE = "prefs_main_date"
    const val PREFS_TODAY = "prefs_today"
    const val PREFS_PROD_TYPE = "prefs_prod_type"

    const val PREFS_SELECTED_SERVER = "prefs_selected_server"
    const val PREFS_SERVERS = "prefs_servers"

    const val PREFS_TIMESTAMP = "prefs_timestamp"
    const val PREFS_PORT_ID = "prefs_port_id"
    const val PREFS_AOMN_ID = "prefs_aomn_id"


    fun fromContext(ctx: Context): SharedPreferences = ctx.getSharedPreferences(PREFS_FILENAME, 0)

}
