package com.novinteh.mdl.util

import android.os.SystemClock


class SafeEvent(
    private val safeMillis: Long = 1000,
    private val onSafeEvent: () -> Unit
) {

    private var lastTimeClicked: Long = 0


    fun launch() {
        if (SystemClock.elapsedRealtime() - lastTimeClicked < safeMillis) return
        lastTimeClicked = SystemClock.elapsedRealtime()
        onSafeEvent()
    }

}