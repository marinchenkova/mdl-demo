package com.novinteh.mdl.util

import android.util.Log


interface Logging {

    val LOG: Boolean
    val LOG_TAG: String

    fun log(msg: String) {
        if (LOG) Log.d(LOG_TAG, msg)
    }

}