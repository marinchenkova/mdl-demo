package com.novinteh.mdl.util

import android.view.View

class SafeClickListener(
    safeMillis: Long,
    onSafeCLick: () -> Unit
) : View.OnClickListener {

    private val safeEvent = SafeEvent(safeMillis, onSafeCLick)


    override fun onClick(v: View) {
        safeEvent.launch()
    }

}

fun View.setSafeClickListener(safeMillis: Long = 1000, onSafeClick: () -> Unit) {
    val safeClickListener = SafeClickListener(safeMillis, onSafeClick)
    setOnClickListener(safeClickListener)
}