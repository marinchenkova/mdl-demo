package com.novinteh.mdl.util

import com.novinteh.mdl.entity.util.DateUtils
import java.util.*

data class Hm(val hStr: String, val mStr: String) {

    val h: Int = hStr.toInt()
    val m: Int = mStr.toInt()


    fun formatHm(): String = "$hStr:$mStr"

    fun percentOfDay(): Float = (h * 60 + m) / 1440f

    fun is0000(): Boolean = h == 0 && m == 0

    fun is2400(): Boolean = h == 24 && m == 0

    override fun toString(): String = formatHm()


    companion object {

        val Hm0000 = Hm("00", "00")
        val Hm2400 = Hm("24", "00")


        fun fromDate(date: Date, isNextDay: Boolean): Hm {
            val hm = DateUtils.formatHm(date).split(":")
            val h = if (isNextDay) "24" else hm[0]
            val m = hm[1]
            return Hm(h, m)
        }

    }

}