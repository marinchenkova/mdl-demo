package com.novinteh.mdl.util

import android.content.Context
import com.novinteh.mdl.R
import com.novinteh.mdl.data.Result
import com.novinteh.mdl.entity.data.common.ProdType
import com.novinteh.mdl.entity.data.regchanges.*
import com.novinteh.mdl.entity.data.shipments.ShipmentState
import com.novinteh.mdl.regimes.regChanges.model.RegChangeListItem

fun Context.pickErrorMessage(result: Result.Error): String = when (result) {
    is Result.Error.Login -> getString(R.string.error_login_failed)
    is Result.Error.Client -> getString(R.string.error_client_exception)
    is Result.Error.Server -> {
        val serverError = getString(R.string.error_server_exception)
        if (result.msg.isBlank()) serverError
        else "$serverError: ${result.msg}"
    }
}

fun ProdType.stringRes() = when (this) {
    ProdType.OIL -> R.string.shift_filter_prod_type_oil
    ProdType.NP -> R.string.shift_filter_prod_type_np
    ProdType.ALL -> R.string.shift_filter_prod_type_all
    ProdType.UNKNOWN -> R.string.not_set
}

fun ProdType.iconRes() = when (this) {
    ProdType.OIL -> R.drawable.ic_drop_fill
    ProdType.NP -> R.drawable.ic_drop_empty
    ProdType.ALL -> R.drawable.ic_drop_half
    ProdType.UNKNOWN -> R.drawable.ic_unknown_24dp
}

fun RegChangeState.imageRes() = when (this) {
    RegChangeState.AWAIT -> R.drawable.ic_await
    RegChangeState.DONE -> R.drawable.ic_done
    RegChangeState.LATE -> R.drawable.ic_late
    RegChangeState.FAILED -> R.drawable.ic_fail
    RegChangeState.CANCELLED -> R.drawable.ic_cancel
    RegChangeState.NOT_SCHEDULED -> R.drawable.ic_not_scheduled
    RegChangeState.UNKNOWN -> R.drawable.ic_unknown_24dp
}

fun RegChangeState.backgroundRes() = when (this) {
    RegChangeState.AWAIT -> R.color.yellow
    RegChangeState.DONE -> R.color.green
    RegChangeState.LATE -> R.color.yellow
    RegChangeState.FAILED -> R.color.red
    RegChangeState.CANCELLED -> R.color.grey
    RegChangeState.NOT_SCHEDULED -> R.color.pink
    RegChangeState.UNKNOWN -> R.color.colorPrimary
}

fun RegChangeState.sideBarRes() = when (this) {
    RegChangeState.AWAIT -> R.drawable.side_bar_yellow
    RegChangeState.DONE -> R.drawable.side_bar_green
    RegChangeState.LATE -> R.drawable.side_bar_yellow
    RegChangeState.FAILED -> R.drawable.side_bar_red
    RegChangeState.CANCELLED -> R.drawable.side_bar_grey
    RegChangeState.NOT_SCHEDULED -> R.drawable.side_bar_pink
    RegChangeState.UNKNOWN -> R.drawable.side_bar_grey
}

fun RegChangeState.stringRes() = when (this) {
    RegChangeState.AWAIT -> R.string.reg_change_state_await
    RegChangeState.DONE -> R.string.reg_change_state_done
    RegChangeState.LATE -> R.string.reg_change_state_late
    RegChangeState.FAILED -> R.string.reg_change_state_failed
    RegChangeState.CANCELLED -> R.string.reg_change_state_cancelled
    RegChangeState.NOT_SCHEDULED -> R.string.reg_change_state_not_scheduled
    RegChangeState.UNKNOWN -> R.string.reg_change_state_unknown
}

fun RegChangeListItem.qKindImageRes(): Int {
    return when (state) {
        RegChangeState.CANCELLED, RegChangeState.AWAIT -> when (qKind) {
            QKind.STABLE -> R.drawable.ic_square_empty
            QKind.GROWTH -> R.drawable.ic_up_empty
            QKind.FALL -> R.drawable.ic_down_empty
            else -> R.drawable.ic_unknown_24dp
        }

        RegChangeState.DONE -> when (qKind) {
            QKind.STABLE -> R.drawable.ic_square_green
            QKind.GROWTH -> R.drawable.ic_up_green
            QKind.FALL -> R.drawable.ic_down_green
            else -> R.drawable.ic_unknown_24dp
        }

        RegChangeState.FAILED -> when (qKind) {
            QKind.STABLE -> R.drawable.ic_square_red
            QKind.GROWTH -> R.drawable.ic_up_red
            QKind.FALL -> R.drawable.ic_down_red
            else -> R.drawable.ic_unknown_24dp
        }

        RegChangeState.NOT_SCHEDULED -> when (qKind) {
            QKind.STABLE -> R.drawable.ic_square_pink
            QKind.GROWTH -> R.drawable.ic_up_pink
            QKind.FALL -> R.drawable.ic_down_pink
            else -> R.drawable.ic_unknown_24dp
        }

        RegChangeState.LATE -> when (qKind) {
            QKind.STABLE -> R.drawable.ic_square_yellow
            QKind.GROWTH -> R.drawable.ic_up_yellow
            QKind.FALL -> R.drawable.ic_down_yellow
            else -> R.drawable.ic_unknown_24dp
        }

        else -> R.drawable.ic_unknown_24dp
    }
}

fun SwitchType.drawableRes() = when (this) {
    SwitchType.OFF -> R.drawable.ic_circle_red
    SwitchType.ON -> R.drawable.ic_circle_green
    SwitchType.UNKNOWN -> R.drawable.ic_unknown_24dp
}

fun SwitchType.stringRes() = when (this) {
    SwitchType.OFF -> R.string.switch_type_off
    SwitchType.ON -> R.string.switch_type_on
    SwitchType.UNKNOWN -> R.string.not_set
}

fun AgrType.stringRes() = when (this) {
    AgrType.MNA -> R.string.switch_agr_type_mna
    AgrType.PNA -> R.string.switch_agr_type_pns
    AgrType.UNKNOWN -> R.string.not_set
}

fun ShipmentState.stringRes() = when (this) {
    ShipmentState.WAIT -> R.string.shipment_state_wait
    ShipmentState.RAID -> R.string.shipment_state_raid
    ShipmentState.MOOR -> R.string.shipment_state_moor
    ShipmentState.LOAD -> R.string.shipment_state_load
    ShipmentState.DONE -> R.string.shipment_state_done
    ShipmentState.UNKNOWN -> R.string.shipment_state_unknown
}

fun ShipmentState.drawableRes() = when (this) {
    ShipmentState.WAIT -> R.drawable.ic_wait
    ShipmentState.RAID -> R.drawable.ic_anchor
    ShipmentState.MOOR -> R.drawable.ic_moor
    ShipmentState.LOAD -> R.drawable.ic_load
    ShipmentState.DONE -> R.drawable.ic_done
    ShipmentState.UNKNOWN -> R.drawable.ic_unknown_24dp
}