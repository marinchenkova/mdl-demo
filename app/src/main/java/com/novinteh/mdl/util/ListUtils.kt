package com.novinteh.mdl.util


fun <T> MutableCollection<T>.replaceAll(new: Collection<T>) {
    clear()
    addAll(new)
}

fun <T> List<T>.containsItemInBounds(item: T, boundOffset: Int): Boolean = when {
    size > boundOffset * 2 -> item in subList(boundOffset, size - boundOffset)
    else -> false
}