package com.novinteh.mdl.main

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.recyclerview.widget.DiffUtil
import com.novinteh.mdl.regimes.regChanges.model.DirWithAomns

data class MainItem(
    val order: Int,
    val activity: Class<out WorkActivity>,
    @StringRes val titleRes: Int,
    @StringRes val textRes: Int,
    @DrawableRes val iconRes: Int
) {

    companion object {

        val DiffCallback = object : DiffUtil.ItemCallback<MainItem>() {
            override fun areItemsTheSame(oldItem: MainItem, newItem: MainItem): Boolean {
                return oldItem.order == newItem.order
            }

            override fun areContentsTheSame(oldItem: MainItem, newItem: MainItem): Boolean {
                return oldItem == newItem
            }
        }

    }

}