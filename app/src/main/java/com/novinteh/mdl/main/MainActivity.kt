package com.novinteh.mdl.main

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.novinteh.mdl.R
import com.novinteh.mdl.entity.data.common.ProdType
import com.novinteh.mdl.regimes.dayPlans.DayPlansActivity
import com.novinteh.mdl.regimes.regChanges.RegChangesActivity
import com.novinteh.mdl.regimes.shipments.ShipmentActivity
import com.novinteh.mdl.util.*
import com.novinteh.mdl.view.recycler.util.OnItemClickListener
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : WorkActivity(), OnItemClickListener {

    override val LOG: Boolean get() = false
    override val LOG_TAG: String get() = "MainActivity"

    private val adapter = MainItemAdapter()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        prepareData()
        initToolbar(getString(R.string.app_name), up = false)
        initRegimes()
    }

    override fun onStart() {
        super.onStart()
        adapter.setOnItemClickListener(this)
    }

    override fun onStop() {
        super.onStop()
        adapter.removeOnItemClickListener()
    }

    private fun prepareData() {
        if (!isAppRelaunched()) return

        setProdType(ProdType.OIL)
        setMainDate("2019.10.10")
        setToday("2019.10.10")

        //setPortId(-1)
        //setAomnId(-1)

        //clearCache()

        setAppRelaunched(false)
    }

    private fun initRegimes() {
        adapter.submitList(listOf(
            MainItem(
                0,
                RegChangesActivity::class.java,
                R.string.activity_main_reg_change_item_title,
                R.string.activity_main_reg_change_item_text,
                R.drawable.ic_reg_change
            ),

            MainItem(
                1,
                DayPlansActivity::class.java,
                R.string.activity_main_plan_diagram_item_title,
                R.string.activity_main_plan_diagram_item_text,
                R.drawable.ic_plan_diagram
            ),
            MainItem(
                2,
                ShipmentActivity::class.java,
                R.string.activity_main_shipment_item_title,
                R.string.activity_main_shipment_item_text,
                R.drawable.ic_shipment
            )
        ))

        recycler_main.itemAnimator = null
        recycler_main.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recycler_main.adapter = adapter
    }

    override fun onItemClick(position: Int) {
        adapter.getItemOrNull(position)?.also { regime ->
            startActivity(Intent(this, regime.activity))
        }
    }

}
