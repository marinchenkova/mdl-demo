package com.novinteh.mdl.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.novinteh.mdl.R
import com.novinteh.mdl.view.recycler.adapter.ClickAdapter
import com.novinteh.mdl.view.recycler.adapter.ItemAdapter
import com.novinteh.mdl.view.recycler.util.OnItemClickListener
import com.novinteh.mdl.view.recycler.util.Ref
import com.novinteh.mdl.view.recycler.viewholder.BindViewHolder
import com.novinteh.mdl.view.recycler.viewholder.ClickViewHolder
import kotlinx.android.synthetic.main.item_main_view.view.*
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.textResource


class MainItemAdapter : ClickAdapter<MainItem, MainItemAdapter.ViewHolder>(MainItem.DiffCallback) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_main_view, parent, false)

        return ViewHolder(view).also { holder ->
            holder.init(clickListener)
        }
    }


    class ViewHolder(view: View) : ClickViewHolder<MainItem>(view) {

        private val icon = itemView.icon
        private val title = itemView.title
        private val text = itemView.text


        override fun content(item: MainItem) {
            icon.imageResource = item.iconRes
            title.textResource = item.titleRes
            text.textResource = item.textRes
        }

    }

}