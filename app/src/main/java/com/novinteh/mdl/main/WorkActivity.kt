package com.novinteh.mdl.main

import android.view.Menu
import android.view.MenuItem
import com.novinteh.mdl.R
import com.novinteh.mdl.data.MdlDatabase
import com.novinteh.mdl.debug.DebugActivity
import com.novinteh.mdl.login.LoginActivity
import com.novinteh.mdl.regimes.dayPlans.DayPlansActivity
import com.novinteh.mdl.regimes.regChanges.RegChangesActivity
import com.novinteh.mdl.regimes.shipments.ShipmentActivity
import com.novinteh.mdl.util.setLogged
import com.novinteh.mdl.view.transition.startActivityClearTask
import kotlinx.coroutines.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

/**
 * Base class for toolbar activities with common transitions between work activities.
 */
abstract class WorkActivity: ReceiverActivity() {

    /**
     * Update toolbar menu on activity resume, when some item
     * was changed in the other activity.
     */
    override fun onResume() {
        super.onResume()
        invalidateOptionsMenu()
    }

    protected fun clearCache() {
        val db = MdlDatabase.getInstance(this)
        CoroutineScope(Dispatchers.IO).launch {
            db.stateDao().deleteAll()

            db.aomnDao().deleteAll()

            db.userShiftDao().deleteAll()
            db.directionDao().deleteAll()
            db.regChangeDao().deleteAll()

            db.tehsectPlanDao().deleteAll()

            db.portDao().deleteAll()
            db.shipmentDao().deleteAll()

            withContext(Dispatchers.Main) { toast("Кэш очищен") }
        }
    }

    /**
     * Toolbar menu initialization.
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.work_toolbar, menu)
        return true
    }

    /**
     * Toolbar action check.
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.toolbar_popup_reg_changes -> {
            if (this !is RegChangesActivity) startActivity<RegChangesActivity>()
            true
        }
        R.id.toolbar_popup_plan_diagram -> {
            if (this !is DayPlansActivity) startActivity<DayPlansActivity>()
            true
        }
        R.id.toolbar_popup_shipments -> {
            if (this !is ShipmentActivity) startActivity<ShipmentActivity>()
            true
        }
        R.id.toolbar_popup_quit -> {
            setLogged(false)
            startActivityClearTask<LoginActivity>()
            true
        }
        R.id.toolbar_clear_cache -> {
            clearCache()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

}