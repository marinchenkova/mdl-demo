package com.novinteh.mdl.main

import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import com.novinteh.mdl.R
import com.novinteh.mdl.util.Logging

/**
 * Base class for activities with toolbar.
 */
abstract class ToolbarActivity : AppCompatActivity() {

    /**
     * Initialize toolbar with [title] and [subtitle], up navigation button ([up] is true),
     * toolbar layout resource [toolbarId]. By default up navigation is on.
     */
    open fun initToolbar(title: String? = null,
                         subtitle: String? = null,
                         up: Boolean = true,
                         @IdRes toolbarId: Int = R.id.toolbar) {
        setSupportActionBar(findViewById(toolbarId))
        supportActionBar?.apply {
            this.title = title
            this.subtitle = subtitle
            setDisplayHomeAsUpEnabled(up)
            setDisplayShowHomeEnabled(up)
        }
    }

    fun setTitle(title: String?) {
        supportActionBar?.title = title

    }

    fun setSubtitle(subtitle: String?) {
        supportActionBar?.subtitle = subtitle
    }

    /**
     * Up navigation handler.
     */
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}
