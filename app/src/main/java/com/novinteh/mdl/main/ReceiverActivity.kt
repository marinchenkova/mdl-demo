package com.novinteh.mdl.main

import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.novinteh.mdl.R
import com.novinteh.mdl.data.Result
import com.novinteh.mdl.login.LoginActivity
import com.novinteh.mdl.util.Logging
import com.novinteh.mdl.util.setLogged
import com.novinteh.mdl.view.transition.startActivityClearTask


abstract class ReceiverActivity : ToolbarActivity(), Observer<Result>, Logging {

    private var dialog: AlertDialog? = null

    /**
     * Alert dialog is shown when error occurs.
     */
    protected open fun showErrorDialog(
        msg: String,
        okText: String = getString(R.string.try_again),
        onPositive: () -> Unit = {}
    ) {
        dialog = AlertDialog.Builder(this)
            .setTitle(getString(R.string.error))
            .setMessage(msg)
            .setPositiveButton(okText) { d, _ ->
                d.dismiss()
                onPositive()
                dialog = null
            }
            .create()
        dialog?.show()
    }

    override fun onChanged(result: Result?) {
        if (result is Result.Error) processError(result)
    }

    protected open fun processError(error: Result.Error) {
        if (dialog?.isShowing == true) return

        if (error is Result.Error.Login) showErrorDialog(error.msg, getString(R.string.relogin)) {
            setLogged(false)
            startActivityClearTask<LoginActivity>()
        }
    }

}
