package com.novinteh.mdl.view


import android.content.Context
import android.content.res.Configuration
import android.graphics.Typeface
import android.text.Editable
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.transition.Transition
import androidx.transition.TransitionManager
import com.novinteh.mdl.entity.data.regchanges.RegChange
import android.graphics.ColorMatrix
import androidx.annotation.IdRes
import com.novinteh.mdl.entity.data.common.Regime
import com.novinteh.mdl.regimes.regChanges.model.RegChangeListItem


const val DEF_ANIM_DURATION = 200L


fun ColorMatrix.setContrast(contrast: Float) {
    val scale = contrast + 1f
    val translate = (-.5f * scale + .5f) * 255f
    set(ColorMatrix(floatArrayOf(
        scale, 0f, 0f, 0f, translate,
        0f, scale, 0f, 0f, translate,
        0f, 0f, scale, 0f, translate,
        0f, 0f, 0f, 1f, 0f
    )))
}

inline fun ViewGroup.animateTransition(transition: Transition, @IdRes vararg exclude: Int, block: () -> Unit) {
    exclude.forEach { transition.excludeChildren(it ,true) }
    TransitionManager.beginDelayedTransition(this, transition)
    block()
}

fun View.show(enable: Boolean) {
    visibility = if (enable) View.VISIBLE else View.INVISIBLE
}

fun View.appear(enable: Boolean) {
    visibility = if (enable) View.VISIBLE else View.GONE
}

fun Context.dpToPixels(dp: Int) = TypedValue.applyDimension(
    TypedValue.COMPLEX_UNIT_DIP,
    dp.toFloat(),
    resources.displayMetrics
)

fun Configuration.isLandscape() = orientation == Configuration.ORIENTATION_LANDSCAPE

inline fun EditText.afterTextChanged(crossinline action: () -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun afterTextChanged(p0: Editable?) = action()
    })
}

fun String.highlightedOn(positions: List<Int>, color: Int): SpannableStringBuilder {
    val builder = SpannableStringBuilder(this)
    positions.forEach {
        builder.setSpan(StyleSpan(Typeface.BOLD), it, it + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        builder.setSpan(ForegroundColorSpan(color), it, it + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    }
    return builder
}

fun RegChangeListItem.regimeArray(limit: Int, color: Int): Array<SpannableStringBuilder> {
    val builder = SpannableStringBuilder("$fromReg->")
    val spanned = toReg.span(fromReg, color)

    if ("$fromReg->$toReg".length > limit) return arrayOf(builder, spanned)

    builder.append(spanned)
    return arrayOf(builder, SpannableStringBuilder())
}

fun Regime.span(from: Regime, color: Int) = name
    .highlightedOn(changedPositions(from), color)


