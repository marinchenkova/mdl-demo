package com.novinteh.mdl.view.recycler.adapter

import androidx.recyclerview.widget.DiffUtil
import com.novinteh.mdl.view.recycler.util.Payload
import com.novinteh.mdl.view.recycler.viewholder.CheckViewHolder
import com.novinteh.mdl.view.recycler.viewholder.ClickViewHolder


abstract class CheckBoxAdapter<I, VH: CheckViewHolder<I>>(diffCallback: DiffUtil.ItemCallback<I>):
    CheckAdapter<I, VH>(diffCallback) {

    private val checkSet = mutableSetOf<Int>()
    val checkedItems: List<I> get() = currentList.filterIndexed { index, _ -> index in checkSet }


    override fun isChecked(position: Int) = position in checkSet

    override fun switch(position: Int) {
        if (isChecked(position)) uncheck(position)
        else check(position)
    }

    fun check(position: Int) {
        if (isChecked(position)) return
        checkSet.add(position)
        notifyItemChanged(position, Payload.CHECK)
        checkListener.get()?.onChecksUpdated(checkedItems)
    }

    fun uncheck(position: Int) {
        if (!isChecked(position)) return
        checkSet.remove(position)
        notifyItemChanged(position, Payload.CHECK)
        checkListener.get()?.onChecksUpdated(checkedItems)
    }

    fun checkAll() {
        checkSet.addAll(currentList.indices)
        notifyItemRangeChanged(0, itemCount, Payload.CHECK)
        checkListener.get()?.onChecksUpdated(checkedItems)
    }

    fun uncheckAll() {
        checkSet.clear()
        notifyItemRangeChanged(0, itemCount, Payload.CHECK)
        checkListener.get()?.onChecksUpdated(checkedItems)
    }

}