package com.novinteh.mdl.view.recycler.util

enum class Payload {
    CHECK,
    FILTER,
    FREEZE,
    LOADSTATE
}