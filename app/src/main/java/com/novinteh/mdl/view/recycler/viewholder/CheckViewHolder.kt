package com.novinteh.mdl.view.recycler.viewholder

import android.view.View

abstract class CheckViewHolder<I>(view: View) : ClickViewHolder<I>(view) {

    abstract fun check()

    abstract fun uncheck()

}