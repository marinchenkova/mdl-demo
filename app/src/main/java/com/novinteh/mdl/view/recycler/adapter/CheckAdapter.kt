package com.novinteh.mdl.view.recycler.adapter

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.novinteh.mdl.view.recycler.util.*
import com.novinteh.mdl.view.recycler.viewholder.CheckViewHolder
import com.novinteh.mdl.view.recycler.viewholder.ClickViewHolder


abstract class CheckAdapter<I, VH: CheckViewHolder<I>>(diffCallback: DiffUtil.ItemCallback<I>):
    ClickAdapter<I, VH>(diffCallback) {

    protected val checkListener = Ref<OnCheckListener<I>>()


    init {
        setOnItemClickListener(object : OnItemClickListener {
            override fun onItemClick(position: Int) {
                switch(position)
            }
        })
    }

    fun setOnCheckListener(listener: OnCheckListener<I>) {
        checkListener.set(listener)
    }

    fun removeOnCheckListener() {
        checkListener.clear()
    }

    protected abstract fun isChecked(position: Int): Boolean

    abstract fun switch(position: Int)

    fun switch(item: I) {
        positionOf(item).apply { if (this != RecyclerView.NO_POSITION) switch(this) }
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.checkCheck(position)
        super.onBindViewHolder(holder, position)
    }

    override fun onBindViewHolder(holder: VH, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) return onBindViewHolder(holder, position)
        if (payloads[0] as? Payload == Payload.CHECK) holder.checkCheck(position)
        else super.onBindViewHolder(holder, position, payloads)
    }

    private fun VH.checkCheck(position: Int) =
        if (isChecked(position)) check() else uncheck()

}