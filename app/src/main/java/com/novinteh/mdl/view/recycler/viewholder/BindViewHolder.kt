package com.novinteh.mdl.view.recycler.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * View holder is class for binding item data to item view.
 */
abstract class BindViewHolder<Item>(view: View) : RecyclerView.ViewHolder(view) {

    /**
     * If passed [item] is null, content should be set via [placeHolder],
     * else use [content].
     */
    fun bind(item: Item?) {
        if (item == null) placeHolder()
        else content(item)
    }

    /**
     * Called when bound item is null.
     */
    open fun placeHolder() {}

    /**
     * Set view contents here.
     */
    abstract fun content(item: Item)

}