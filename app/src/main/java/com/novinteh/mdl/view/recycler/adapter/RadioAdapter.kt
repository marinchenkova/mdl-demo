package com.novinteh.mdl.view.recycler.adapter

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.novinteh.mdl.view.recycler.util.Payload
import com.novinteh.mdl.view.recycler.viewholder.CheckViewHolder
import com.novinteh.mdl.view.recycler.viewholder.ClickViewHolder

abstract class RadioAdapter<I, VH: CheckViewHolder<I>>(diffCallback: DiffUtil.ItemCallback<I>):
    CheckAdapter<I, VH>(diffCallback) {

    val checkedItem: I? get() = getItemOrNull(checkedPosition)
    private var checkedPosition = RecyclerView.NO_POSITION


    override fun isChecked(position: Int) = position == checkedPosition

    override fun switch(position: Int) {
        if (isChecked(position)) return
        checkedPosition = position
        notifyItemRangeChanged(0, itemCount, Payload.CHECK)
        checkListener.get()?.onChecksUpdated(checkedItem)
    }

}