package com.novinteh.mdl.view.recycler.viewholder

import android.view.View
import com.novinteh.mdl.util.setSafeClickListener
import com.novinteh.mdl.view.recycler.util.OnItemClickListener
import com.novinteh.mdl.view.recycler.util.Ref
import kotlin.reflect.jvm.internal.impl.incremental.components.Position


/**
 * Process item view click events.
 */
abstract class ClickViewHolder<I>(view: View) : BindViewHolder<I>(view) {

    private lateinit var onItemClickListener: Ref<OnItemClickListener>


    fun init(clickListener: Ref<OnItemClickListener>) {
        onItemClickListener = clickListener
        itemView.setSafeClickListener { click() }
    }

    protected fun click() {
        onItemClickListener.get()?.onItemClick(provideClickPosition())
    }

    protected open fun provideClickPosition(): Int = adapterPosition

}