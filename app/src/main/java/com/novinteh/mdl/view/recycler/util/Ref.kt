package com.novinteh.mdl.view.recycler.util

data class Ref<I>(private var obj: I? = null) {

    fun get() = obj

    fun set(value: I) {
        obj = value
    }

    fun clear() {
        obj = null
    }

}