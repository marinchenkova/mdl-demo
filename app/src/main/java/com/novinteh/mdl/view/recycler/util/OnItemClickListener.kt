package com.novinteh.mdl.view.recycler.util

interface OnItemClickListener {

    fun onItemClick(position: Int)

}