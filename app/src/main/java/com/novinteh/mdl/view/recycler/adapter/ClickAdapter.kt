package com.novinteh.mdl.view.recycler.adapter

import androidx.recyclerview.widget.DiffUtil
import com.novinteh.mdl.view.recycler.util.OnItemClickListener
import com.novinteh.mdl.view.recycler.util.Ref
import com.novinteh.mdl.view.recycler.viewholder.ClickViewHolder


abstract class ClickAdapter<I, VH: ClickViewHolder<I>>(diffCallback: DiffUtil.ItemCallback<I>) :
    ItemAdapter<I, VH>(diffCallback) {

    protected val clickListener = Ref<OnItemClickListener>()


    fun getItemOrNull(position: Int): I? =
        if (position in 0 until itemCount) getItem(position)
        else null

    fun setOnItemClickListener(listener: OnItemClickListener) {
        clickListener.set(listener)
    }

    fun removeOnItemClickListener() {
        clickListener.clear()
    }

}