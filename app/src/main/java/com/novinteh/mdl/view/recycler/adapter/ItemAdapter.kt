package com.novinteh.mdl.view.recycler.adapter

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.novinteh.mdl.view.recycler.viewholder.BindViewHolder

/**
 * Build up over [ListAdapter] that uses [BindViewHolder].
 */
abstract class ItemAdapter<I, VH: BindViewHolder<I>>(diffCallback: DiffUtil.ItemCallback<I>) :
    ListAdapter<I, VH>(diffCallback) {

    protected fun positionOf(item: I): Int = currentList.indexOf(item)

    override fun getItem(position: Int): I? =
        if (position in 0 until itemCount) super.getItem(position)
        else null

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(getItem(position))
    }

}