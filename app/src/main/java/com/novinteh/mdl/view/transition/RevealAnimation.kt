package com.novinteh.mdl.view.transition

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.view.ViewAnimationUtils
import android.view.animation.AccelerateInterpolator
import android.view.ViewTreeObserver
import android.app.Activity
import android.content.Intent
import android.view.View
import com.novinteh.mdl.view.show


class RevealAnimation(private val view: View, intent: Intent, private val activity: Activity) {

    private var revealX: Int = 0
    private var revealY: Int = 0


    init {
        if (intent.hasExtra(EXTRA_CIRCULAR_REVEAL_X) && intent.hasExtra(
                EXTRA_CIRCULAR_REVEAL_Y
            )) {
            view.show(false)

            revealX = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_X, 0)
            revealY = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_Y, 0)

            val viewTreeObserver = view.viewTreeObserver
            if (viewTreeObserver.isAlive) {
                viewTreeObserver.addOnGlobalLayoutListener(object :
                    ViewTreeObserver.OnGlobalLayoutListener {
                    override fun onGlobalLayout() {
                        revealActivity(revealX, revealY)
                        view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                    }
                })
            }
        }
        else view.show(true)
    }

    fun revealActivity(x: Int, y: Int) {
        val finalRadius = (view.width.coerceAtLeast(view.height) * 1.1).toFloat()

        val circularReveal =
            ViewAnimationUtils.createCircularReveal(view, x, y, 0f, finalRadius)
        circularReveal.duration = 300
        circularReveal.interpolator = AccelerateInterpolator()

        view.show(true)
        circularReveal.start()
    }

    fun unRevealActivity() {
        val finalRadius = (view.width.coerceAtLeast(view.height) * 1.1).toFloat()
        val circularReveal = ViewAnimationUtils.createCircularReveal(
            view, revealX, revealY, finalRadius, 0f
        )

        circularReveal.duration = 300
        circularReveal.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                view.show(false)
                activity.finish()
                activity.overridePendingTransition(0, 0)
            }
        })

        circularReveal.start()
    }


    companion object {
        const val EXTRA_CIRCULAR_REVEAL_X = "EXTRA_CIRCULAR_REVEAL_X"
        const val EXTRA_CIRCULAR_REVEAL_Y = "EXTRA_CIRCULAR_REVEAL_Y"
    }

}