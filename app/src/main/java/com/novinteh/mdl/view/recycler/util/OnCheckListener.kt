package com.novinteh.mdl.view.recycler.util

interface OnCheckListener<I> {

    fun onChecksUpdated(item: I?) {}

    fun onChecksUpdated(checked: List<I>) {}

    fun onChecksUpdated(position: Int, checked: List<I>) {}

}