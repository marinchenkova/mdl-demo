package com.novinteh.mdl.view.transition

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask


inline fun <reified A: AppCompatActivity> AppCompatActivity.startRevealActivity(
    v: View,
    requestCode: Int,
    vararg params: Pair<String, Any?>
) {
    val x = (v.x + v.width / 2).toInt()
    val y = (v.y + v.height / 2).toInt()

    startActivityForResult(intentFor<A>(
        RevealAnimation.EXTRA_CIRCULAR_REVEAL_X to x,
        RevealAnimation.EXTRA_CIRCULAR_REVEAL_Y to y,
        *params
    ), requestCode)

    overridePendingTransition(0, 0)
}


/**
 * Start activity with clearing back stack.
 */
inline fun <reified T : AppCompatActivity> AppCompatActivity.startActivityClearTask() =
    startActivity(intentFor<T>().newTask().clearTask())