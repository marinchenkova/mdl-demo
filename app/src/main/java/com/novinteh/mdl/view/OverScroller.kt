package com.novinteh.mdl.view

import android.animation.ValueAnimator
import android.view.MotionEvent
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import java.lang.ref.WeakReference
import kotlin.math.abs

class OverScroller(
    view: View,
    private val decelerateFactor: Float = 3f,
    private val yxRatio: Float = 2.5f,
    private val canScrollUp: () -> Boolean = { true }
) : RecyclerView.OnScrollListener() {

    private val ref = WeakReference(view)
    private var currentAnimation: ValueAnimator? = null

    private var overScrollAllowed = false
    private var isOverScrolling = false

    private var initialX = 0f
    private var initialY = 0f

    private var prevX = 0f
    private var prevY = 0f

    private var translationY = 0f


    fun onTouchEvent(event: MotionEvent?): Boolean = when (event?.actionMasked) {
        MotionEvent.ACTION_DOWN -> down(event.rawX, event.rawY)
        MotionEvent.ACTION_MOVE -> scroll(event.rawX, event.rawY)
        MotionEvent.ACTION_UP -> finishOverScroll()
        else -> false
    }

    private fun down(x: Float, y: Float): Boolean {
        currentAnimation?.cancel()
        initialX = x
        initialY = y
        prevX = x
        prevY = y
        return false
    }

    private fun finishOverScroll(): Boolean {
        if (isOverScrolling) {
            currentAnimation = ValueAnimator.ofFloat(translationY, 0f).also { animator ->
                animator.addUpdateListener { valueAnimator ->
                    translationY = valueAnimator.animatedValue as Float
                    setTranslationY(translationY)
                    if (translationY == 0f) {
                        isOverScrolling = false
                        prevX = 0f
                        prevY = 0f
                    }
                }
                animator.start()
            }
            return true
        }

        return false
    }

    private fun scroll(x: Float, y: Float): Boolean {
        if (canScrollUp()) {
            initialX = x
            initialY = y
            prevX = x
            prevY = y
            return false
        }

        isOverScrolling = true

        if (y > prevY) translationY = (y - initialY) / decelerateFactor
        else translationY += y - prevY

        prevX = x
        prevY = y

        if (translationY <= 0) {
            isOverScrolling = false
            return false
        }

        setTranslationY(translationY)

        return true
    }

    private fun setTranslationY(translation: Float) {
        ref.get()?.translationY = translation
    }

    private fun satisfiesRatio(distanceX: Float, distanceY: Float): Boolean {
        if (distanceX == 0f) return true
        return abs(distanceY / distanceX) >= yxRatio
    }

}