package com.novinteh.mdl.data.repo

import com.novinteh.mdl.util.Timestamp
import com.novinteh.mdl.regimes.common.persistence.dao.StateDao


abstract class SingleIdRepository(stateDao: StateDao): Repository(stateDao) {

    protected abstract val singleId: String
    private val singleTimestamp = Timestamp.now()


    suspend fun load() = load(singleId, singleTimestamp)

    suspend fun loadIfAbsent() = loadIfAbsent(singleId, singleTimestamp)

}