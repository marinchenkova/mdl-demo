package com.novinteh.mdl.data.repo

import com.novinteh.mdl.data.Result
import com.novinteh.mdl.util.Timestamp
import com.novinteh.mdl.regimes.common.persistence.dao.StateDao
import kotlinx.coroutines.*


abstract class AsyncRepository(stateDao: StateDao): Repository(stateDao) {

    /**
     * Load data for each id from [ids] asynchronously, then wait for all data loaded
     * and notify about successful and failed results. The id from [ids] may not be enqueued
     * for load if it is in process.
     */
    suspend fun load(ids: List<String>, timestamp: Timestamp) {
        val idsForLoad = ids.filterNot { isLoading(it) }
        if (idsForLoad.isEmpty()) return

        idsForLoad.forEach { id -> setIdIsLoading(id) }
        tryNotifyLoading(timestamp)

        val results = download(idsForLoad)
        processResults(idsForLoad, timestamp, results)
    }

    suspend fun loadIfAbsent(ids: List<String>, timestamp: Timestamp) {
        val absentIds = ids.filterNot { contains(it) }

        //log("loadIfAbsent, ids $ids, absent ids $absentIds")

        if (absentIds.isEmpty()) return

        load(absentIds, timestamp)
    }

    private suspend fun download(ids: List<String>): List<Result> {
        return coroutineScope {
            ids.map { id -> async { download(id) } }.awaitAll()
        }
    }

    private suspend fun processResults(ids: List<String>, timestamp: Timestamp, results: List<Result>) {
        ids.zip(results).toMap().forEach { entry ->
            val id = entry.key
            when (val result = entry.value) {
                is Result.Success.Data -> {
                    setIdIsSuccess(id)
                    upsert(id, result)
                }
                is Result.Error -> setIdIsError(id)
            }
        }

        tryNotifySuccess(timestamp)

        results.filterIsInstance<Result.Error>().lastOrNull()?.also { lastError ->
            tryNotifyError(timestamp, lastError)
        }
    }

}