package com.novinteh.mdl.data.repo

import com.novinteh.mdl.data.Result
import com.novinteh.mdl.regimes.common.model.LoadStateInfo
import com.novinteh.mdl.util.Timestamp
import com.novinteh.mdl.regimes.common.persistence.dao.StateDao
import com.novinteh.mdl.util.Logging

abstract class Repository(private val stateDao: StateDao) : Logging {

    protected abstract val stateId: String

    val state get() = stateDao.getState(stateId)

    private var lastState: LoadStateInfo? = null
    private val loading = mutableSetOf<String>()
    private val errors = mutableSetOf<String>()


    suspend fun load(id: String, timestamp: Timestamp) {
        if (isLoading(id)) return
        setIdIsLoading(id)
        tryNotifyLoading(timestamp)
        val result = download(id)
        processResult(id, timestamp, result)
    }

    suspend fun loadIfAbsent(id: String, timestamp: Timestamp) {
        if (!contains(id)) load(id, timestamp)
    }

    private suspend fun processResult(id: String, timestamp: Timestamp, result: Result) {
        when (result) {
            is Result.Success.Data -> {
                setIdIsSuccess(id)
                tryNotifySuccess(timestamp)
                upsert(id, result)
            }
            is Result.Error -> {
                setIdIsError(id)
                tryNotifyError(timestamp, result)
            }
        }
    }

    protected fun setIdIsLoading(id: String) {
        loading.add(id)
        errors.remove(id)
    }

    protected fun setIdIsSuccess(id: String) {
        loading.remove(id)
        errors.remove(id)
    }

    protected fun setIdIsError(id: String) {
        errors.add(id)
        loading.remove(id)
    }

    protected suspend fun tryNotifyLoading(timestamp: Timestamp) {
        updateState(timestamp, Result.Loading)
    }

    protected suspend fun tryNotifySuccess(timestamp: Timestamp) {
        if (loading.isEmpty() && errors.isEmpty()) {
            updateState(timestamp, Result.Success.Empty)
        }
    }

    protected suspend fun tryNotifyError(timestamp: Timestamp, error: Result.Error) {
        if (loading.isEmpty() && errors.isNotEmpty()) {
            updateState(timestamp, error)
        }
    }

    private suspend fun updateState(timestamp: Timestamp, result: Result) {
        val state = LoadStateInfo(stateId, timestamp, result)
        if (state != lastState) stateDao.insert(state)
        lastState = state
    }

    protected fun isLoading(id: String): Boolean = id in loading

    protected abstract suspend fun contains(id: String): Boolean

    protected abstract suspend fun upsert(id: String, data: Result.Success.Data)

    protected abstract suspend fun download(id: String): Result

}