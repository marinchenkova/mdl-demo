package com.novinteh.mdl.data

import okhttp3.ResponseBody

sealed class Result {

    object Loading : Result()


    sealed class Success : Result() {

        object Empty : Success()

        data class Data(val responseBody: ResponseBody) : Success() {

            override fun toString(): String {
                return "Result.Success.Data"
            }
        }

    }


    sealed class Error (open val msg: String) : Result() {

        data class Client(override val msg: String = "") : Error(msg)

        data class Login(override val msg: String = "") : Error(msg)

        data class Server(override val msg: String = "") : Error(msg)

    }

}
