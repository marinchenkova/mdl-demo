package com.novinteh.mdl.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.novinteh.mdl.regimes.common.converters.CommonConverters
import com.novinteh.mdl.regimes.common.model.AomnItem
import com.novinteh.mdl.regimes.common.model.LoadStateInfo
import com.novinteh.mdl.regimes.common.persistence.dao.AomnDao
import com.novinteh.mdl.regimes.common.persistence.dao.StateDao
import com.novinteh.mdl.regimes.dayPlans.model.TehsectPlanItem
import com.novinteh.mdl.regimes.dayPlans.model.TehsectPlanListItem
import com.novinteh.mdl.regimes.dayPlans.persistence.converters.DayPlanConverters
import com.novinteh.mdl.regimes.dayPlans.persistence.dao.TehsectPlanDao
import com.novinteh.mdl.regimes.regChanges.model.*
import com.novinteh.mdl.regimes.regChanges.persistence.converters.DirectionConverters
import com.novinteh.mdl.regimes.regChanges.persistence.converters.RegChangeConverters
import com.novinteh.mdl.regimes.regChanges.persistence.dao.DirectionDao
import com.novinteh.mdl.regimes.regChanges.persistence.dao.RegChangeDao
import com.novinteh.mdl.regimes.regChanges.persistence.dao.UserShiftDao
import com.novinteh.mdl.regimes.shipments.model.PortItem
import com.novinteh.mdl.regimes.shipments.model.ShipmentItem
import com.novinteh.mdl.regimes.shipments.model.ShipmentListItem
import com.novinteh.mdl.regimes.shipments.persistence.converters.ShipmentConverters
import com.novinteh.mdl.regimes.shipments.persistence.dao.PortDao
import com.novinteh.mdl.regimes.shipments.persistence.dao.ShipmentDao


@Database(
    entities = [
        LoadStateInfo::class,

        AomnItem::class,

        RegChangeItem::class,
        DirectionItem::class,
        UserShiftItem::class,

        TehsectPlanItem::class,

        PortItem::class,
        ShipmentItem::class
    ],
    views = [
        RegChangeListItem::class,
        TehsectPlanListItem::class,
        ShipmentListItem::class
    ],
    version = 101
)
@TypeConverters(
    CommonConverters::class,
    RegChangeConverters::class,
    DirectionConverters::class,
    DayPlanConverters::class,
    ShipmentConverters::class
)
abstract class MdlDatabase : RoomDatabase() {

    abstract fun stateDao(): StateDao

    abstract fun aomnDao(): AomnDao

    abstract fun regChangeDao(): RegChangeDao
    abstract fun directionDao(): DirectionDao
    abstract fun userShiftDao(): UserShiftDao

    abstract fun tehsectPlanDao(): TehsectPlanDao

    abstract fun portDao(): PortDao
    abstract fun shipmentDao(): ShipmentDao


    companion object {

        @Volatile
        private var INSTANCE: MdlDatabase? = null

        fun getInstance(ctx: Context): MdlDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) return tempInstance

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    ctx.applicationContext,
                    MdlDatabase::class.java,
                    "cached_database"
                )
                    .fallbackToDestructiveMigration()
                    .build()

                INSTANCE = instance
                return instance
            }
        }

    }

}


